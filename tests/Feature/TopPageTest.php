<?php

namespace Tests\Feature;

use App\User;

class TopPageTest extends TestCaseFeature
{
    // --- Logined Test
    public function testLogined()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/');
        $this->assertEquals(200, $response->status());
        $this->assertContains(__('top.description'), $response->getContent());
    }

    // --- terms of service
    public function testTermsOfService()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/terms_of_service');
        $this->assertEquals(200, $response->status());
    }
}
