<?php

namespace Tests\Feature;

use Tests\TestCase;

class TestCaseFeature extends TestCase
{
    protected $uri = '/';

    public function testNotLogin()
    {
        $response = $this->get($this->uri);
        $this->assertEquals(302, $response->status());
        $this->assertContains("/login", $response->getContent());
    }
}
