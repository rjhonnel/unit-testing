<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;

use App\User;
use App\Models\Profile;
use App\Models\Club;
use App\Models\Post;
use App\Models\ClubJoinUser;
use App\Models\Comment;

class CommunicationPageTest extends TestCaseFeature
{
  public function setUp()
  {
    parent::setUp();
    Artisan::call('db:seed', [ '--class' => 'DatabaseSeeder', '--force' => true ]);
  }

  // --- Redirect Login page, if not login
  public function testNotLogin()
  {
    $response = $this->get('/communications/1');
    $this->assertEquals(302, $response->status());
    $this->assertContains("/login", $response->getContent());
  }

  // --- Logined Test

  // --- Post List
  public function testPostList()
  {
    $user = factory(User::class)->create();
    $profile = factory(Profile::class)->create(['user_id' => $user->id]);
    ClubJoinUser::create([
        'club_id' => 1,
        'user_id' => $user->id,
        'start_date' => '2017-11-10'
    ]);
    $response = $this->actingAs($user)->get('/communications/1');
    $this->assertEquals(200, $response->status());
  }
  // --- Post List - Not Found
  public function testPostListNotFound()
  {
    Club::destroy(99999);
    $user = factory(User::class)->create();
    $response = $this->actingAs($user)->get('/communications/99999');
    $this->assertEquals(404, $response->status());
  }

}
