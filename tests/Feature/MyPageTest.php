<?php

namespace Tests\Feature;

use App\User;
use App\Models\Profile;

class MyPageTest extends TestCaseFeature
{
    protected $uri = '/mypage';

    // --- MyPage
    public function testMyPage()
    {
        $user = factory(User::class)->create();
        $profile = factory(Profile::class)->create(['user_id' => $user->id]);
        $response = $this->actingAs($user)->get($this->uri);
        $this->assertEquals(200, $response->status());
        $this->assertContains("マイページ", $response->getContent());
    }
}
