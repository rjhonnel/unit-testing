<?php

namespace Tests\Feature;

use App\User;

class TutorialPageTest extends TestCaseFeature
{
    protected $uri = '/tutorial';

    // --- Logined and Agreed Test
    public function testLoginedAndAgreement()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/tutorial');
        $this->assertEquals(200, $response->status());
    }

    // --- Redirect Top page, if not agreement.
    public function testLoginedAndNotAgreement()
    {
        $user = factory(User::class)->create();
        $user->agreement_flag = 0;
        $user->save();
        $response = $this->actingAs($user)->get('/tutorial');
        $this->assertEquals(302, $response->status());
    }
}
