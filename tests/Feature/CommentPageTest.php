<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use App\Models\Post;
use App\Models\Supporter;
use App\Models\Program;
use App\Models\Club;
use App\Models\ClubJoinUser;
use App\Models\Comment;
use App\Models\Profile;

class CommentPageTest extends TestCaseFeature
{

    public function testCommentList()
    {
        $user = factory(User::class)->create();
        $profile = factory(Profile::class)->create(['user_id' => $user->id]);
        $supporter = factory(Supporter::class)->create(['user_id' => $user->id]);
        $program = factory(Program::class)->create([
            'supporter_id' => $supporter->id
        ]);

        $club = factory(Club::class)->create([
            'program_id' => $program->id
        ]);
       
        $post = factory(Post::class)->create([
            'user_id' => $supporter->id,
            'club_id' => $club->id
        ]);

        $cju = ClubJoinUser::createUserJoinClub($user->id, $program);
        
        $comment = factory(Comment::class)->create([
            'post_id' => $post->id,
            'user_id' => $user->id,
            'text' => config('post.automaticly.text')
        ]);

        $response = $this->actingAs($user)->get('/comments/'.$post->id);
        $this->assertEquals(200, $response->status());
    	$this->assertContains('/comments', $response->getContent());
    }
}
