<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;

use App\User;
use App\Models\Supporter;

class SupporterPageTest extends TestCaseFeature
{
    protected $uri = '/supporters';

    public function setUp()
    {
        parent::setUp();
        Artisan::call('db:seed', [ '--class' => 'UserSeeder', '--force' => true ]);
    }

    // --- Supporter List
    public function testSupporterList()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/supporters');
        $this->assertEquals(200, $response->status());
    }

    // --- Supporter Detail
    public function testSupporterDetail()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/supporters/1');
        $this->assertEquals(200, $response->status());
    }

    // --- Program Detail - Not Found
    public function testSupporterDetailNotFound()
    {
        $user = factory(User::class)->create();
        Supporter::destroy(99999);
        $response = $this->actingAs($user)->get('/supporters/99999');
        $this->assertEquals(404, $response->status());
    }

    // --- Recommend
    public function testRecommend()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/recommend_programs');
        $this->assertEquals(200, $response->status());
        $this->assertContains("あなたにオススメ", $response->getContent());
    }
}
