<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;

use App\User;
use App\Models\Program;

class ProgramPageTest extends TestCaseFeature
{
    protected $uri = '/programs';

    public function setUp()
    {
        parent::setUp();
        Artisan::call('db:seed', [ '--class' => 'UserSeeder', '--force' => true ]);
        Artisan::call('db:seed', [ '--class' => 'ProgramSeeder', '--force' => true ]);
    }

    // --- Program List
    public function testProgramList()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/programs');
        $this->assertEquals(200, $response->status());
        $this->assertSame(200, $response->status());
        $this->assertContains('/programs', $response->getContent());

    }

    // --- Program Detail
    public function testProgramDetail()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/programs/1');
        $this->assertEquals(200, $response->status());
    }

    // --- Program Detail - Not Found
    public function testProgramDetailNotFound()
    {
        $user = factory(User::class)->create();
        Program::destroy(99999);
        $response = $this->actingAs($user)->get('/programs/99999');
        $this->assertEquals(404, $response->status());
    }

    // --- Recommend
    public function testRecommend()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->get('/recommend_programs');
        $this->assertEquals(200, $response->status());
        $this->assertSame(200, $response->status());
        $this->assertContains("あなたにオススメ", $response->getContent());
    }
}
