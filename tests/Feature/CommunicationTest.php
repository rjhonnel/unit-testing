<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\Models\Post;
use App\Models\Club;
use App\Models\Comment;
use App\Models\LikePost;
use App\Models\LikeComment;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommunicationTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->fixtures();
    }

    public function testExistsClub()
    {
        $club = Club::find(1);
        $this->assertEquals(1, $club->count());
    }

    public function testGetPostByClubId()
    {
        $post = Post::getPostByClubId(1);
        $this->assertGreaterThanOrEqual(1, $post->count());
    }

    public function testLikePost()
    {
        $like = LikePost::create([
            'user_id' => 1,
            'post_id' => 1
        ]);
        $this->assertEquals(1, $like->count());
    }

    public function testDeleteLike()
    {
        $like = LikePost::create([
            'user_id' => 1,
            'post_id' => 1
        ])->delete();
        $this->assertTrue($like);
    }

    public function testAddComment()
    {
        $comment = Comment::create([
            'post_id' => 1,
            'user_id' => 1,
            'text' => 'test text'
        ]);
        $this->assertGreaterThanOrEqual(1, $comment->count());
    }

    public function testDeleteComment()
    {
        $comment = Comment::create([
            'post_id' => 1,
            'user_id' => 1,
            'text' => 'test text'
        ])->delete();
        $this->assertTrue($comment);
    }

    public function testLikeComment()
    {
        $comment = LikeComment::create([
            'user_id' => 1,
            'comment_id' => 1
        ]);
        $this->assertEquals(1, $comment->count());
    }

    public function testDeleteLikeComment()
    {
        $comment = LikeComment::create([
            'user_id' => 1,
            'comment_id' => 1
        ])->delete();
        $this->assertTrue($comment);
    }

    private function fixtures()
    {
        Artisan::call('db:seed');
    }
}
