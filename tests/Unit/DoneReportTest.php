<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;

use App\Models\Program;
use App\Models\Supporter;
use App\Models\DoneReport;
use App\Models\Club;
use App\Models\ClubJoinUser;

class DoneReportTest extends TestCase
{

	protected $u;
    protected $s;
    protected $p;
    protected $c;
    protected $cju;

	public function setUp()
    {
        parent::setUp();
        $this->u = factory(User::class)->create();
        $this->s = factory(Supporter::class)->create([ 'user_id' => $this->u->id ]);
        $this->p = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        $this->c = factory(Club::class)->create(['program_id' => $this->p->id ]);
        $this->cju = ClubJoinUser::createUserJoinClub($this->u->id, $this->p);
    }

    public function testCreateDoneReportIfNotExist(){
        
        $this->assertEquals(1,DoneReport::createDoneReportIfNotExist($this->u->id,$this->p->id));
        
    }
}

?>