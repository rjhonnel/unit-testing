<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\UserWeight;
use App\User;

class WeightManageTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function testUpdateWeight()
    {
        $weight = factory(UserWeight::class)->create(['user_id' => $this->user->id]);
        $this->assertTrue(UserWeight::createData($this->user->id, 65, date('Y-m-d')));
    }

    public function testInsertWeight()
    {
        $this->assertTrue(UserWeight::createData($this->user->id, 65));
    }
}
