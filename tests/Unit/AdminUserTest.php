<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Models\Profile;

class AdminUserTest extends TestCase
{
    private $user;
    private $profile;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->profile = factory(Profile::class)->create(['user_id' => $this->user->id]);
    }

    public function testSearchUserById()
    {
        $user = User::seachUserByIdOrNickname($this->user->id);
        $this->assertEquals(1, $user->count());
    }

    public function testSearchUserByNickname()
    {
        $user = User::seachUserByIdOrNickname($this->profile->nickname);
        $this->assertEquals(1, $user->count());
    }
}
