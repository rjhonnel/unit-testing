<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Models\Club;
use App\Models\Program;
use App\Models\Supporter;
use App\Models\ClubJoinUser;
use App\Models\Post;
use App\Models\DoneReport;
use Carbon\Carbon;

class ClubJoinUserTest extends TestCase
{
    private $user;
    private $supporter;
    private $program;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->supporter = factory(Supporter::class)->create([ 'user_id' => $this->user->id ]);
        $this->program = factory(Program::class)->create([
            'supporter_id' => $this->supporter->id,
            'capacity' => 2
        ]);
    }

    public function testCreateUserJoinClubIfNoClubAvailable()
    {
        // assert
        $res = ClubJoinUser::createUserJoinClub($this->user->id, $this->program);
        $this->assertTrue($res);
    }

    public function testCreateUserJoinClubIfHasClubAvailable()
    {
        // club
        factory(Club::class)->create([ 'program_id' => $this->program->id ]);
        factory(Club::class)->create([ 'program_id' => $this->program->id ]);
        factory(Club::class)->create([ 'program_id' => $this->program->id ]);

        // assert
        $res = ClubJoinUser::createUserJoinClub($this->user->id, $this->program);
        $this->assertTrue($res);
    }

    public function testCreateUserJoinClubIfHasClubAndOverCapacity()
    {
        // clubs
        $c1 = factory(Club::class)->create([ 'program_id' => $this->program->id ]);

        // users
        $u2 = factory(User::class)->create();
        $u3 = factory(User::class)->create();

        // club_join_users
        $cju2 = factory(ClubJoinUser::class)->create([
            'club_id' => $c1->id,
            'user_id' => $u2->id
        ]);
        $cju3 = factory(ClubJoinUser::class)->create([
            'club_id' => $c1->id,
            'user_id' => $u3->id
        ]);

        // assert
        $res = ClubJoinUser::createUserJoinClub($this->user->id, $this->program);
        $userJoinedClub = ClubJoinUser::where('user_id', '=', $this->user->id)->first();
        $this->assertEquals($c1->id + 1, $userJoinedClub->club_id);
    }

    public function testIsUserJoinedProgram()
    {
        // club
        $c1 = factory(Club::class)->create([ 'program_id' => $this->program->id ]);

        // club_join_users
        $cju1 = factory(ClubJoinUser::class)->create([
            'club_id' => $c1->id,
            'user_id' => $this->user->id
        ]);
        // assert
        $res = ClubJoinUser::isUserJoinedProgram($this->user->id, $this->program->id);
        $this->assertTrue($res);
    }

    public function testIsUserNotJoinedProgram()
    {
        // club
        $c1 = factory(Club::class)->create([ 'program_id' => $this->program->id ]);

        // assert
        $res = ClubJoinUser::isUserJoinedProgram($this->user->id, $this->program->id);
        $this->assertFalse($res);
    }

    public function testCreatePostBySupporter()
    {
        // club
        $club = factory(Club::class)->create([ 'program_id' => $this->program->id ]);
        //assert
        $res = Post::createPostBySupporter($club->id);
        $this->assertDatabaseHas('posts', ['club_id' => $club->id, 'user_id' => $this->user->id, 'text' => config('post.automaticly.text')]);
    }

    public function testCreateDoneReportIfNotExist()
    {
        // user logged
        $userLogged = factory(User::class)->create();
        // club
        $club = factory(Club::class)->create([ 'program_id' => $this->program->id ]);
        // club_join_user
        factory(ClubJoinUser::class)->create([
            'user_id' => $userLogged->id,
            'club_id' => $club->id
        ]);
        //assert
        DoneReport::createDoneReportIfNotExist($userLogged->id, $this->program->id);
        $this->assertDatabaseHas('done_reports', ['date' => Carbon::today()]);
    }

    public function testGetDoneReportTodayIfExist()
    {
        // user logged
        $userLogged = factory(User::class)->create();
        $club = factory(Club::class)->create([ 'program_id' => $this->program->id ]);
        factory(ClubJoinUser::class)->create([
            'user_id' => $userLogged->id,
            'club_id' => $club->id
        ]);
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'club_id' => $club->id
        ]);
        factory(DoneReport::class)->create([
            'post_id' => $post->id,
            'date' => Carbon::today()
        ]);
        //assert
        $doneReportToday = Program::getDoneReportToday($userLogged->id, $this->program->id);
        $this->assertTrue(!is_null($doneReportToday));
    }

    public function testGetDoneReportTodayIfNotExist()
    {
        // user logged
        $userLogged = factory(User::class)->create();
        $club = factory(Club::class)->create([ 'program_id' => $this->program->id ]);
        factory(ClubJoinUser::class)->create([
            'user_id' => $userLogged->id,
            'club_id' => $club->id
        ]);
        $post = factory(Post::class)->create([
            'user_id' => $this->user->id,
            'club_id' => 10
        ]);
        //assert
        $doneReportToday = Program::getDoneReportToday($userLogged->id, $this->program->id);
        $this->assertTrue(is_null($doneReportToday));
    }
}

