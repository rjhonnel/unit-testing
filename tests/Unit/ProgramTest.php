<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Program;
use App\Models\RecommendProgram;
use App\User;
use App\Models\Supporter;

class ProgramTest extends TestCase
{
    private $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function testGetRecommendProgramListIsExist()
    {
        $u2 = factory(User::class)->create();
        $sp = factory(Supporter::class)->create(['user_id' => $u2->id]);
        $p1 = factory(\App\Models\Program::class)->create(['supporter_id' => $sp->id]);
        $p2 = factory(\App\Models\Program::class)->create(['supporter_id' => $sp->id]);
        factory(RecommendProgram::class)->create([
            'user_id' => $this->user->id,
            'program_id' => $p1->id
        ]);
        factory(RecommendProgram::class)->create([
            'user_id' => $this->user->id,
            'program_id' => $p2->id
        ]);
        $programs = Program::getRecommendProgramList($this->user->id);
        $this->assertEquals(2, $programs->count());
    }

    public function testGetRecommendProgramListIsNotExist()
    {
        $programs = Program::getRecommendProgramList($this->user->id);
        $this->assertEquals(0, $programs->count());
    }
}
