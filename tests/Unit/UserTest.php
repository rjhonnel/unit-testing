<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\User;
use App\Models\Profile;
use App\Models\Club;
use App\Models\Program;
use App\Models\Supporter;
use Illuminate\Foundation\Testing\RefreshDatabase;


class UserTest extends TestCase
{
    private $user;
    private $profile;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->profile = factory(Profile::class)->create(['user_id' => $this->user->id]);
        $this->supporter = factory(Supporter::class)->create([ 'user_id' => $this->user->id ]);
        $this->program = factory(Program::class)->create([ 'supporter_id' => $this->supporter->id ]);
        $this->club = factory(Club::class)->create([ 'program_id' => $this->program->id ]);
    }

    public function testCheckDuplicateEmail()
    {        
        $this->assertTrue(User::checkDuplicateEmail($this->user->email));
    }

    public function testSeachUserByIdOrNickname()
    {
        $user = User::seachUserByIdOrNickname($this->profile->nickname);
        $this->assertEquals(1, $user->count());

        $user = User::seachUserByIdOrNickname($this->user->id);
        $this->assertEquals(1, $user->count());
    }

    public function testIsOwnClub(){
        $this->assertTrue($this->user::isOwnClub($this->user->id,$this->club->id));
    }

    
}
