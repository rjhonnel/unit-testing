<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Auth;

class LoginPageTest extends TestCase
{
    private $user;
    private $password = '12345678';

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create([
            'password' => bcrypt($this->password)
        ]);
    }

    public function testUserLoginIfWrongEmail()
    {
        $data = [
            'email' => 'prefix' . $this->user->email,
            'password' => $this->password
        ];
        $this->assertFalse(Auth::attempt($data));
    }

    public function testUserLoginIfWrongPassword()
    {
        $data = [
            'email' => $this->user->email,
            'password' => 'prefix' . $this->password
        ];
        $this->assertFalse(Auth::attempt($data));
    }

    public function testUserLoginIfValid()
    {
        $data = [
            'email' => $this->user->email,
            'password' => $this->password
        ];
        $this->assertTrue(Auth::attempt($data));
    }
}
