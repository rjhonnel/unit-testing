<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Models\Club;
use App\Models\Program;
use App\Models\Supporter;
use App\Models\ClubJoinUser;
use Carbon\Carbon;

class ClubTest extends TestCase
{
    protected $p;
    protected $u;
    protected $s;
    protected $c;
    protected $cju;

    public function setUp() {
        parent::setUp();
        $this->u = factory(User::class)->create();
        $this->s = factory(Supporter::class)->create([ 'user_id' => $this->u->id ]);
        $this->p = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        $this->c = factory(Club::class)->create([ 'program_id' => $this->p->id ]);
        $this->cju = ClubJoinUser::createUserJoinClub($this->u->id, $this->p);
    }

    // --- getLatestClubOfProgram
    public function testGetLatestClubOfProgram() {

        // --- data
        $c1 = factory(Club::class)->create([ 'program_id' => $this->p->id ]);
        $c2 = factory(Club::class)->create([ 'program_id' => $this->p->id ]);
        $c3 = factory(Club::class)->create([ 'program_id' => $this->p->id ]);

        // --- assert
        $ret = Club::getLatestClubOfProgram($this->p->id);
        $this->assertEquals($c3->id, $ret->id);
    }

    public function testGetLatestClubOfProgram_NotClubs() {

        // --- assert
        $ret = Club::getLatestClubOfProgram($this->p->id);
        $this->assertNull($ret);
    }

    // --- isJoin
    public function testIsJoin_Joined() {
        // --- data
        $c1 = factory(Club::class)->create([ 'program_id' => $this->p->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $c1->id ]);

        // --- assert
        $this->assertTrue($c1->isJoin($this->u->id));
    }

    public function testIsJoin_NotJoin() {
        // --- data
        $c1 = factory(Club::class)->create([ 'program_id' => $this->p->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $c1->id ]);

        // --- assert
        $this->assertFalse($c1->isJoin(99999));
    }
    
    public function testGetClubNotFinishOfProgram() {
        /* -- case 1: end_date = null -- */
        // -- data
        $c1 = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        $cl1 = factory(Club::class)->create([ 'program_id' => $c1->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $cl1->id ]);
        
        // -- assert 1
        $ret1 = Club::getClubNotFinishOfProgram($c1->id);
        $this->assertEquals($c1->id, $ret1->first()->program_id);
        
        
        /* -- case 2: end_date = today -- */
        // -- data
        $c2 = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        $cl2 = factory(Club::class)->create([ 'program_id' => $c2->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $cl2->id, 'end_date' => Carbon::now() ]);
        
        // -- assert 2
        $ret2 = Club::getClubNotFinishOfProgram($c2->id);
        $this->assertEquals(null, $ret2->first());
        
        /* -- case 3: end_date > today -- */
        // -- data
        $c3 = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        $cl3 = factory(Club::class)->create([ 'program_id' => $c3->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $cl3->id, 'end_date' => Carbon::tomorrow() ]);
        
        // -- assert 3
        $ret3 = Club::getClubNotFinishOfProgram($c3->id);
        $this->assertEquals($c3->id, $ret3->first()->program_id);
        
        /* -- case 4: end_date < today -- */
        // -- data
        $c4 = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        $cl4 = factory(Club::class)->create([ 'program_id' => $c4->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $cl4->id, 'end_date' => Carbon::yesterday() ]);
       
        // -- assert 4
        $ret4 = Club::getClubNotFinishOfProgram($c4->id);
        $this->assertEquals(null, $ret4->first());
        
        /* -- case 5: program is no exist -- */
        // -- data
        $c5 = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        $cl5 = factory(Club::class)->create([ 'program_id' => $c5->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $cl5->id ]);

        // -- assert 5
        $ret5 = Club::getClubNotFinishOfProgram(($c5->id + 1));
        $this->assertEquals(null, $ret5->first());
        
        /* -- case 6: dont't have a club -- */
        // -- data
        $c6 = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        
        // -- assert 6
        $ret6 = Club::getClubNotFinishOfProgram($c6->id);
        $this->assertEquals(null, $ret6->first());
        
        /* -- case 7: have a club -- */
        // -- data
        $c7 = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        $cl7 = factory(Club::class)->create([ 'program_id' => $c7->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $cl7->id ]);
       
        // -- assert 7
        $ret7 = Club::getClubNotFinishOfProgram($c7->id);
        $this->assertEquals($c7->id, $ret7->first()->program_id);
        
        /* -- case 8: have some club -- */
        // -- data
        $c8 = factory(Program::class)->create([ 'supporter_id' => $this->s->id ]);
        $cl8_1 = factory(Club::class)->create([ 'program_id' => $c8->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $cl8_1->id ]);
        $cl8_2 = factory(Club::class)->create([ 'program_id' => $c8->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $cl8_2->id ]);
        $cl8_3 = factory(Club::class)->create([ 'program_id' => $c8->id ]);
        factory(ClubJoinUser::class)->create([ 'user_id' => $this->u->id, 'club_id' => $cl8_3->id ]);
        
        // -- assert 8
        $ret8 = Club::getClubNotFinishOfProgram($c8->id);
        $this->assertEquals( $cl8_1->id , $ret8->first()->club_id);
    }

    public function testGetClubNotFinishIfNotFound()
    {
        $c1 = factory(Club::class)->create([ 'program_id' => $this->p->id ]);
        $c2 = factory(Club::class)->create([ 'program_id' => $this->p->id ]);
        $u1 = factory(User::class)->create();
        factory(ClubJoinUser::class)->create([
            'user_id' => $this->u->id,
            'club_id' => $c1->id,
            'end_date' => '2016-10-10'
        ]);
        factory(ClubJoinUser::class)->create([
            'user_id' => $this->u->id,
            'club_id' => $c2->id,
            'end_date' => '2016-10-10'
        ]);
        factory(ClubJoinUser::class)->create([
            'user_id' => $u1->id,
            'club_id' => $c2->id,
            'end_date' => '2016-10-10'
        ]);
        $this->assertEquals(0, count(Club::getClubNotFinish()));
    }

    public function testGetClubNotFinishIfEndDateNull()
    {
        $c1 = factory(Club::class)->create([ 'program_id' => $this->p->id ]);
        $c2 = factory(Club::class)->create([ 'program_id' => $this->p->id ]);
        factory(ClubJoinUser::class)->create([
            'user_id' => $this->u->id,
            'club_id' => $c1->id,
            'end_date' => null
        ]);
        factory(ClubJoinUser::class)->create([
            'user_id' => $this->u->id,
            'club_id' => $c2->id,
            'end_date' => null
        ]);
        $this->assertEquals(2, Club::getClubNotFinish()->count());
    }

    public function testJoinUsers(){

        $this->assertEquals(1, $this->c->joinUsers()->count());
    
    }
}

