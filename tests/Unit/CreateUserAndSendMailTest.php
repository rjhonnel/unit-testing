<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;


class CreateUserAndSendMailTest extends TestCase
{
    private $rules = [
        'name' => 'string|max:255',
        'email' => 'required|string|email|max:255|unique:users'
    ];

    public function setUp()
    {
        parent::setUp();
    }

    public function testValidateCsvIfEmailDuplicate()
    {
        $user = factory(User::class)->create();
        $this->assertTrue(User::checkDuplicateEmail($user->email));
    }

    public function testValidateCsvIfEmailIsNotDuplicate()
    {
        $email = 'example@example.com';
        $this->assertFalse(User::checkDuplicateEmail($email));
    }

    public function testValidateCsvIfEmailIsNotExist()
    {
        $data = [
            'name' => 'Jinky',
            'email' => null
        ];
        $v = $this->app['validator']->make($data, $this->rules);
        $this->assertFalse($v->passes());
    }

    public function testValidateCsvIfEmailIsNotValidFormat()
    {
        $data = [
            'name' => 'Jinky',
            'email' => 'example.com'
        ];
        $v = $this->app['validator']->make($data, $this->rules);
        $this->assertFalse($v->passes());
    }
}
