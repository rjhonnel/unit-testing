<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\LikePost;
use App\Models\LikeComment;
use App\Models\LikeSupporter;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Supporter;
use App\Models\Profile;
use App\User;

class VoteTest extends TestCase
{
    private $user;
    private $post;
    private $comment;
    private $sp;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        factory(Profile::class)->create(['user_id' => $this->user->id]);
        $this->post = factory(Post::class)->create();
        $this->comment = factory(Comment::class)->create();
        $this->sp = factory(Supporter::class)->create(['user_id' => $this->user->id]);
    }

    public function testGetUserLikedPostOrderById()
    {
        factory(LikePost::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $this->post->id
        ]);
        $users = LikePost::getUserLikedPost($this->post->id);
        $this->assertEquals($users->pluck('id'), $users->sortBy('id')->pluck('id'),
        'The users liked post are not being ordered by id');
    }

    public function testGetUserLikedPostIfUserIsExist()
    {
        factory(LikePost::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $this->post->id
        ]);
        $users = LikePost::getUserLikedPost($this->post->id);
        $this->assertEquals(1, $users->count());

    }

    public function testGetUserLikedPostIfUserIsNotExist()
    {
        $users = LikePost::getUserLikedPost($this->post->id);
        $this->assertEquals(0, $users->count());
    }

    #test like comment
    public function testGetUserLikedCommentOrderById() {
        factory(LikeComment::class)->create([
            'user_id' => $this->user->id,
            'comment_id' => $this->comment->id
        ]);
        $users = LikeComment::getUserLikeCommentOfPost($this->comment->id);
        $this->assertEquals($users->pluck('id'), $users->sortBy('id')->pluck('id'),
        'The users liked comment are not being ordered by id');
    }

    public function testGetUserLikeCommentIfUserIsExist()
    {
         factory(LikeComment::class)->create([
            'user_id' => $this->user->id,
            'comment_id' => $this->comment->id
        ]);
        $users = LikeComment::getUserLikeCommentOfPost($this->comment->id);
        $this->assertEquals(1, $users->count());
    }

    public function testGetUserLikeCommentIfUserIsNotExist()
    {
        $users = LikeComment::getUserLikeCommentOfPost($this->comment->id);
        $this->assertEquals(0, $users->count());
    }

    #test like supporter
    public function testGetUserLikedSupporterOrderById() {
        factory(LikeSupporter::class)->create([
            'user_id' => $this->user->id,
            'supporter_id' => $this->sp->id
        ]);
        $users = LikeSupporter::getUserLikedSupporter($this->sp->id);
        $this->assertEquals($users->pluck('id'), $users->sortBy('id')->pluck('id'),
        'The users liked supporter are not being ordered by id');
    }

    public function testGetUserLikeSupporterIfUserIsExist()
    {
        $sp = factory(Supporter::class)->create(['user_id' => $this->user->id]);
        factory(LikeSupporter::class)->create([
            'user_id' => $this->user->id,
            'supporter_id' => $this->sp->id
        ]);
        $users = LikeSupporter::getUserLikedSupporter($this->sp->id);
        $this->assertEquals(1, $users->count());
    }

    public function testGetUserLikeSupporterIfUserIsNotExist()
    {
        $users = LikeSupporter::getUserLikedSupporter($this->sp->id);
        $this->assertEquals(0, $users->count());
    }

}
