<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use App\Models\Post;
use App\Models\Profile;
use App\Models\Comment;
use App\Models\LikeComment;
class LikeCommentTest extends TestCase
{

	private $user;
	private $post;
	private $comment;
	private $lcomment;

	public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->profile = factory(Profile::class)->create(['user_id' => $this->user->id]);

        $this->post = factory(Post::class)->create([
            'club_id' => 1,
            'user_id' => $this->user->id,
            'text' => config('post.automaticly.text')
        ]);

        $this->comment = factory(Comment::class)->create([
            'post_id' => $this->post->id,
            'user_id' => $this->user->id,
            'text' => config('post.automaticly.text')
        ]);

        $this->lcomment = factory(LikeComment::class)->create([
            'comment_id' => $this->comment->id,
            'user_id' => $this->user->id,
        ]);
    }

    public function testCreatePostBySupporter(){
    	$newPost= Post::create();
    	
        $this->assertEquals(1, $newPost->count());
    }

    public function testGetUserLikeCommentOfPost(){
    	
    	$this->assertEquals(1,LikeComment::getUserLikeCommentOfPost($this->comment->id)->count());

    }
}

?>