<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Models\Profile;

class TutorialTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testNotExistProfile()
    {
        $this->assertFalse(Profile::existProfile(1));
    }

    public function testExistProfile()
    {
        $user = factory(User::class)->create();
        $profile = factory(Profile::class)->create(['user_id' => $user->id]);
        $this->assertTrue(Profile::existProfile($user->id));
    }

    public function testGetProfileIfNotExist()
    {
        $user = factory(User::class)->create();
        $profile = factory(Profile::class)->create(['user_id' => $user->id]);
        $this->assertEquals('/images/no-photo.png', $profile->getPhotoImage());
    }

    public function testGetProfileIfExist()
    {
        $user = factory(User::class)->create();
        $profile = factory(Profile::class)->create([
            'user_id' => $user->id,
            'photo_image_url' => '/images/profile_1.jpg'
        ]);
        $this->assertEquals('/images/profile_1.jpg', $profile->getPhotoImage());
    }
}
