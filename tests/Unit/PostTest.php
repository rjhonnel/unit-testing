<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use App\Models\Post;
use App\Models\Supporter;
use App\Models\Program;
use App\Models\Club;

class ClubJoinUserTest extends TestCase
{

    private $user;
    private $supporter;
    private $program;
    private $club;
    private $post;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->supporter = factory(Supporter::class)->create(['user_id' => $this->user->id]);
        $this->program = factory(Program::class)->create([
            'supporter_id' => $this->supporter->id
        ]);

        $this->club = factory(Club::class)->create([
            'program_id' => $this->program->id
        ]);
       
        //$this->post = factory(Post::class)->create([
        //    'user_id' => $this->supporter->id,
        //    'club_id' => $this->club->id
        //]);
        $this->post = Post::createPostBySupporter($this->club->id);
    }

    public function testDeleteRecordPost(){
        $this->assertEquals(1,Post::deleteRecord($this->user->id,1));
    }

    public function testCreatePostBySupporter(){
        $this->assertEquals(1,$this->post);
    }
}

?>