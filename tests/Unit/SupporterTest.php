<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\LikeSupporter;
use App\User;
use App\Models\Profile;
use Supporter;

class SupporterTest extends TestCase
{
    private $user;
    private $profile;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->profile = factory(Profile::class)->create(['user_id' => $this->user->id]);
    }

    public function testGetSupporterListIfSupporterIsExist()
    {
        $u1 = factory(User::class)->create();
        factory(Profile::class)->create(['user_id' => $u1->id]);
        factory(\App\Models\Supporter::class)->create(['user_id' => $this->user->id]);
        factory(\App\Models\Supporter::class)->create(['user_id' => $u1->id]);
        $supporters = Supporter::getSupporterList();
        $this->assertEquals(2, $supporters->count());
    }

    public function testGetSupporterListIfSupporterIsNotExist()
    {
        $supporters = Supporter::getSupporterList();
        $this->assertEquals(0, $supporters->count());
    }

    public function testGetSupporterDetailByIdIfExist()
    {
        $sp = factory(\App\Models\Supporter::class)->create(['user_id' => $this->user->id]);
        $supporter = Supporter::getSupporterDetailById($sp->id);
        $this->assertEquals($this->profile->nickname, $supporter->nickname);
    }

    public function testNumberOfLikeSupporterIfLiked()
    {
        $u1 = factory(User::class)->create();
        $sp = factory(\App\Models\Supporter::class)->create(['user_id' => $u1->id]);
        factory(LikeSupporter::class)->create([
            'user_id' => $this->user->id,
            'supporter_id' => $sp->id
        ]);
        $likeData = LikeSupporter::supporterUserId($sp->id, $this->user->id)->get();
        $this->assertEquals(1, $likeData->count());
    }

    public function testNumberOfLikeSupporterIfNotLike()
    {
        $u1 = factory(User::class)->create();
        $sp = factory(\App\Models\Supporter::class)->create(['user_id' => $u1->id]);
        $likeData = LikeSupporter::supporterUserId($sp->id, $this->user->id)->get();
        $this->assertEquals(0, $likeData->count());
    }
}
