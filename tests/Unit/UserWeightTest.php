<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use App\Models\UserWeight;

class UserWeightTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->fixtures();
    }

    public function testAddReportUserWeight()
    {
        $userWeight = UserWeight::create([
            'user_id' => 1,
            'date' => date('Y-m-d'),
            'weight' => 65
        ]);
        $this->assertEquals(1, $userWeight->count());
    }

    public function testDuplicateUserWeightData()
    {
        UserWeight::create([
            'user_id' => 1,
            'date' => date('Y-m-d'),
            'weight' => 65
        ]);
        try {
            UserWeight::create([
                'user_id' => 1,
                'date' => date('Y-m-d'),
                'weight' => 60
            ]);
        } catch (\Exception $e) {}
        $this->assertEquals(1, UserWeight::count());
    }

    private function fixtures()
    {
        Artisan::call('db:seed', [
            '--class' => 'UserSeeder',
            '--force' => true
        ]);
    }
}
