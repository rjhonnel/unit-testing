<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\ClubJoinUser;
use App\Models\DoneReport;
use App\Models\DoneReportUser;
use App\Models\Program;
use App\Models\Club;
use App\Models\Supporter;
use App\Models\Post;
use App\User;
use Report;
use Carbon\Carbon;

class MyPageTest extends TestCase
{
    private $user;
    private $club;
    private $program;
    private $clubJoinUser;
    private $supporter;
    private $post;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $u = factory(User::class)->create();
        $this->supporter = factory(Supporter::class)->create(['user_id' => $u->id]);
        $this->program = factory(Program::class)->create([
            'supporter_id' => $this->supporter->id
        ]);
        $this->club = factory(Club::class)->create([
            'program_id' => $this->program->id
        ]);
        $this->clubJoinUser = factory(ClubJoinUser::class)->create([
            'user_id' => $this->user->id,
            'club_id' => $this->club->id
        ]);
        $this->post = factory(Post::class)->create([
            'user_id' => $this->supporter->id,
            'club_id' => $this->club->id
        ]);
    }

    public function testGetEndProgramDataTodayIfExist()
    {
        $program = Report::getEndProgramDataByDate($this->user->id, date('Y-m-d', strtotime(' +2 day')));
        $this->assertEquals($this->program->title, $program['programName']);
    }

    public function testGetEndProgramDataTodayIfProgramHasClosed()
    {
        // Update program_end_checked_at
        $this->clubJoinUser->program_end_checked_at = date('Y-m-d');
        $this->clubJoinUser->save();
        $program = Report::getEndProgramDataByDate($this->user->id, date('Y-m-d'));
        $this->assertEmpty($program);
    }

    public function testGetEndProgramDataTodayIfEndDateIsNull()
    {
        // Update end_date
        $this->clubJoinUser->end_date = null;
        $this->clubJoinUser->save();
        $program = Report::getEndProgramDataByDate($this->user->id, date('Y-m-d'));
        $this->assertEmpty($program);
    }

    public function testGetUserJoinedPeriodArray()
    {
        $startDate = '2017-11-13';
        $endDate = '2017-11-17';
        $joinedDateArray = ['2017-11-13', '2017-11-15'];
        list($dayOfWeeks, $days, $joins) =
            Report::getUserJoinedPeriodArray($startDate, $endDate, $joinedDateArray);

        //$this->assertEquals(5, count($days));
        $this->assertEquals(2,count(array_filter($joins)));
    }

    public function testGetClubUserDoneReportDateArrayIfNotCompleted()
    {
        $clubId = $userId = 1;
        // Make done_reports and done_report_users data
        $startDate = '2017-11-13';
        $endDate = '2017-11-17';
        $this->makeDoneReportData($this->user->id, $startDate, $endDate);

        $joinedDateArray = Report::getClubUserDoneReportDateArray($this->club->id, $this->user->id);
        $this->assertEquals('2017-11-13', $joinedDateArray[0]);
    }

    public function testGetClubUserDoneReportDateArrayIfHasCompleted()
    {
        // Make done_reports and done_report_users data
        $startDate = '2017-11-13';
        $endDate = '2017-11-17';
        $this->makeDoneReportData($this->user->id, $startDate, $endDate, true);

        $joinedDateArray = Report::getClubUserDoneReportDateArray($this->club->id, $this->user->id);
        $this->assertEquals(5, count($joinedDateArray));
    }

    private function makeDoneReportData($userId, $startDate, $endDate, $isCompleted = false)
    {
        $startDateObj = new Carbon($startDate);
        $endDateObj = new Carbon($endDate);
        $endDateObj->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $dateRange = new \DatePeriod($startDateObj, $interval, $endDateObj);
        $firstDayDone = false;
        foreach ($dateRange as $date) {
            $post = factory(Post::class)->create([
                'club_id' => $this->club->id,
                'user_id' => $this->supporter->user->id
            ]);
            $done = factory(DoneReport::class)->create([
                'post_id' => $post->id,
                'date' => $date->format('Y-m-d')
            ]);
            if (!$isCompleted && $firstDayDone) {
                continue;
            }
            $firstDayDone = true;
            factory(DoneReportUser::class)->create([
                'user_id' => $userId,
                'done_report_id' => $done->id
            ]);
        }
    }

    public function testGetClub()
    {
        $club = Program::getClub($this->user->id, $this->program->id);
        $this->assertEquals($this->club->id, $club->id);
    }

    public function testGetProgramsUserJoined()
    {
        $programs = Program::getProgramsUserJoined($this->user->id);
        $this->assertEquals(1, count($programs));
    }

    public function testGetDoneReportByCurrentDate()
    {
        $doneReport = factory(DoneReport::class)->create([
            'post_id' => $this->post->id,
            'date' => date('Y-m-d')
        ]);
        $this->assertEquals(
            $doneReport->id,
            Program::getDoneReportByCurrentDate($this->user->id, $this->program->id)->id
        );
    }

    public function testTotalDoneReportUserCurrentDay()
    {
        $total = Program::totalDoneReportUserCurrentDay($this->user->id, $this->program->id);
        $this->assertEquals(0, $total);
    }

    public function testIsDoneReportUserToday()
    {
        $doneReport = factory(DoneReport::class)->create([
            'post_id' => $this->post->id,
            'date' => Carbon::today()
        ]);
        $isDone = Program::isDoneReportUserToday($this->user->id, $this->program->id);
        $this->assertFalse($isDone);
    }

    public function testCountUserJoinedClub()
    {
        $numberOfUsers = Program::countUserJoinedClub($this->user->id, $this->program->id);
        $this->assertEquals(1, $numberOfUsers);
    }
}
