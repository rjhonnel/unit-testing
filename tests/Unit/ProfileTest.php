<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Traits\UploadImage;
use App\Models\Profile;
use App\Models\Goal;
use App\Models\UserWeight;
use App\Services\ProfileService;
use App\User;

class ProfileTest extends TestCase
{
    private $user;
    private $profile;
    private $goal;
    private $p;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->profile = factory(Profile::class)->create(['user_id' => $this->user->id]);
        $this->goal = factory(Goal::class)->create(['user_id' => $this->user->id]);
        $this->p = new ProfileService;
    }

    public function testScopeUserIdOfProfile()
    {
        $profile = Profile::userId($this->user->id)->first();
        $this->assertEquals($this->profile->nickname, $profile->nickname);
    }

    public function testScopeUserIdOfGoal()
    {
        $goal = Goal::userId($this->user->id)->first();
        $this->assertEquals($this->goal->weight, $goal->weight);
    }

    public function testGetLastWeightReport()
    {
        factory(UserWeight::class)->create([
            'date' => '2017-12-01',
            'user_id' => $this->user->id
        ]);
        factory(UserWeight::class)->create([
            'date' => '2017-12-02',
            'user_id' => $this->user->id
        ]);
        $weight = factory(UserWeight::class)->create([
            'date' => '2017-12-03',
            'user_id' => $this->user->id
        ]);
        $this->assertEquals($weight->weight, UserWeight::getLastWeightReport($this->user->id)->weight);
    }

    public function testProfileServiceIsUpdateProfile()
    {
        $nickname = 'DonaDona';
        $data = ['nickname' => $nickname];
        $this->p->handleUpdateData($this->user->id, $data);
        $this->assertEquals($this->user->profile->nickname, $nickname);
    }

    public function testProfileServiceIsCreateProfile()
    {
        $u = factory(User::class)->create();
        $data = [
            'nickname' => 'DonaDona',
            'height' => 175,
            'weight' => 65,
            'birthday' => '1993-08-15',
            'gender' => 1,
            'plan' => 1,
            'user_id' => $u->id
        ];
        $this->p->handleUpdateData($u->id, $data);
        $this->assertEquals($data['nickname'], $u->profile->nickname);
    }

    public function testProfileServiceIsUpdateGoal()
    {
        $data = ['goalweight' => 60];
        $this->p->handleUpdateData($this->user->id, $data);
        $this->assertEquals(60, $this->user->goal->weight);
    }

    public function testProfileServiceIsCreateGoal()
    {
        $u = factory(User::class)->create();
        $data = [
            'weight' => 60,
            'deadline' => \Carbon\Carbon::tomorrow()
        ];
        $this->p->handleUpdateData($u->id, $data);
        $this->assertEquals($data['weight'], $u->goal->weight);
    }

    public function testGetPhotoImageProfile(){
        $profile = Profile::userId($this->user->id)->first();
        $this->assertEquals($this->profile->photo_image_url, $profile->photo_image_url);

    }
}
