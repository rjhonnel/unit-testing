<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Notification;
use App\User;

class NotificationTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    public function testDuplicateUserId()
    {
        try {
            $notification = Notification::createData(
                $this->user->id,
                $this->user->id,
                1,
                Notification::TYPE_LIKE_POST
            );
        } catch (\Exception $e) {}
        $this->assertFalse($notification);
    }

    public function testCreateNotification()
    {
        $userAction = factory(User::class)->create();
        $this->assertTrue(Notification::createData(
                            $this->user->id,
                            $userAction->id,
                            1,
                            Notification::TYPE_LIKE_POST
                        ));
    }

    public function testRemoveNotificationIfNotFound()
    {
        try {
            Notification::removeData(99, $this->user->id, 1, Notification::TYPE_LIKE_POST);
        } catch (\Exception $e) {
            $this->assertEquals('データが削除できません', $e->getMessage());
        }
    }

    public function testUpdateReadAtIfNotFound()
    {
        $this->assertEquals(Notification::FAIL_STATUS, Notification::updateReadAt(1));
    }

    public function testUpdateReadAt()
    {
        $actionUser = factory(User::class)->create();
        $notify = factory(Notification::class)->create([
            'user_id' => $this->user->id,
            'action_user_id' => $actionUser->id
        ]);
        $this->assertEquals(Notification::SUCCESS_STATUS, Notification::updateReadAt($notify->id));
    }
}
