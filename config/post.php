<?php

return [
    'image' => [
        'width' => 400,
        'height' => 400,
        'path' => 'posts',
        'url' => 'https://s3-ap-northeast-1.amazonaws.com/%s/%s/%s'
    ],
    'avatar' => [
        'width' => 200,
        'height' => 200,
        'path' => 'profiles',
        'url' => 'https://s3-ap-northeast-1.amazonaws.com/%s/%s/%s'
    ],
    'automaticly' => [
        'text' => 'This is content will be posted automaticly from system.'
    ]
];
