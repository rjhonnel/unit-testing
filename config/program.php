<?php

return [
    'image' => [
        'width' => 200,
        'height' => 200,
        'path' => 'programs',
        'url' => 'https://s3-ap-northeast-1.amazonaws.com/%s/%s/%s'
    ],
    'automaticly' => [
        'text' => 'This is content will be posted automaticly from system.'
    ]
];