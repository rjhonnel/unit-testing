<?php
return [
    'word' => [
        'nickname' => 'ニックネーム',
        'set_nickname' => 'ニックネーム登録',
        'height' => '身長',
        'set_height_weight' => '身長・体重登録',
        'weight' => '体重',
        'gender' => '性別',
        'male' => '男性',
        'female' => '女性',
        'set_gender' => '性別登録',
        'birthday' => '誕生日',
        'set_birthday' => '誕生日登録',
        'plan' => 'プラン',
        'set_plan' => 'プラン選択',
        'goal' => '目標',
        'set_goal' => '目標設定',
        'goal_weight' => '目標体重',
        'deadline' => '期日',
        'confirm_data' => '登録内容確認'
    ],
    'message' => [
        'try_again' => 'もう一度お願いします'
    ]
];
