<?php

return [
    'join' => '参加する',
    'properties' => '詳しくみる',
    'create_new' => '新規登録',
    'show' => '表示',
    'edit' => '編集',
    'cancel' => 'キャンセル',
    'submit' => '登録',
    'show_club' => 'クラブ表示',
    'search' => '検索',
    'back' => '戻る',
    'show_join_user' => '参加ユーザ表示',
    'send' => '送信',
    'like' => 'いいね',
    'comments' => 'コメント',
    'done' => '報告',
    'reported' => '報告済み',
    'save' => '保存',
    'login' => 'ログイン',
    'logout' => 'ログアウト',
];
