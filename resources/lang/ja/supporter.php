<?php
return [
    'word' => [
        'id' => 'ID',
        'user_id' => 'ユーザID',
        'nickname' => 'ニックネーム',
        'image' => '画像',
        'cover_image' => 'カバー画像',
        'self_introduction' => '自己紹介',
        'show' => '表示',
        'edit' => '編集',
        'list' => 'リスト',
        'create' => '新規登録',
        'supporter' => 'サポーター',
        'supporter_id' => 'サポーターID',
        'email' => 'Email',
        'manage_programs' => 'プログラム管理',
        'program_id' => 'プログラムID',
        'title' => 'タイトル',
        'description' => '説明',
        'password' => 'パスワード'
    ],
    'message' => [
        'update_success' => '更新に成功しました。',
        'create_success' => '登録に成功しました。'
    ]
];
