<?php

return [
    'club_list' => 'クラブリスト',
    'id' => 'ID',
    'start_date' => '開始日',
    'end_date' => '終了日',
    'number_of_join' => '参加人数',
    'program_id' => 'プログラムID',
    'program_title' => 'プログラムタイトル',
    'program_description' => 'プログラム説明',
    'club_id' => 'クラブID',
    'show_club' => 'クラブ表示',
];
