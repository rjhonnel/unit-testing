<?php

return [
    'update_goal' => '目標更新',
    'weight' => '体重',
    'deadline' => '期日',
    'update_profile' => 'プロフィール更新',
    'nickname' => 'ニックネーム',
    'height' => '身長',
    'birthday' => '誕生日',
    'gender' => '性別',
    'male' => '男性',
    'female' => '女性',
    'plan' => 'プラン',
    'edit' => '編集',
    'goal_weight' => '目標体重',
];
