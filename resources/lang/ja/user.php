<?php
return [
    'word' => [
        'user' => 'ユーザ',
        'create' => '新規登録',
        'email' => 'Email',
        'password' => 'パスワード',
        'list' => 'リスト',
        'nickname' => 'ニックネーム',
        'gender' => '性別',
        'age' => '年齢',
        'height' => '身長',
        'weight' => '体重',
        'plan' => 'プラン',
        'goal_weight' => '目標体重',
        'deadline' => '期日',
        'image' => '画像',
        'detail' => '詳細',
        'male' => '男性',
        'female' => '女性',
        'goal_of_weight' => '目標体重',
        'created_at' => 'Created at',
        'join_program' => '参加プログラム',
        'program_id' => 'プログラムID',
        'title' => 'タイトル',
        'description' => '説明',
        'start' => '開始日',
        'end' => '終了日',
        'club_join_user' => 'クラブ参加ユーザ'
    ],
    'message' => [
        'create_success' => '登録に成功しました'
    ]
];
