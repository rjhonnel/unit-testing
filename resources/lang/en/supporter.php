<?php
return [
    'word' => [
        'id' => 'ID',
        'user_id' => 'user ID',
        'nickname' => 'Nickname',
        'image' => 'Image',
        'cover_image' => 'Cover image',
        'self_introduction' => 'Self Intoroduction',
        'show' => 'Show',
        'edit' => 'Edit',
        'list' => 'List',
        'create' => 'Create new',
        'supporter' => 'Supporter',
        'supporter_id' => 'Supporter ID',
        'email' => 'Email',
        'manage_programs' => 'Manage programs',
        'program_id' => 'Program ID',
        'title' => 'Title',
        'description' => 'Description',
        'password' => 'Password'
    ],
    'message' => [
        'update_success' => 'Update have been successfully',
        'create_success' => 'Update have been successfully'
    ]
];