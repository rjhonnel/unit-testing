<?php
return [
    'word' => [
        'nickname' => 'Nickname',
        'set_nickname' => 'Set nickname',
        'height' => 'Height',
        'set_height_weight' => 'Set Height Weight',
        'weight' => 'Weight',
        'gender' => 'Gender',
        'male' => 'Male',
        'female' => 'Female',
        'set_gender' => 'Set Gender',
        'birthday' => 'Birthday',
        'set_birthday' => 'Set Birthday',
        'plan' => 'Plan',
        'set_plan' => 'Set Plan',
        'goal' => 'Goal',
        'set_goal' => 'Set Goal',
        'goal_weight' => 'Goal Weight',
        'deadline' => 'Deadline',
        'confirm_data' => 'Confirm data'
    ],
    'message' => [
        'try_again' => 'Please try again'
    ]
];