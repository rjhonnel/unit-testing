<?php

return [
    'join' => 'Join',
    'properties' => 'Properties',
    'create_new' => 'Create new',
    'show' => 'Show',
    'edit' => 'Edit',
    'cancel' => 'Cancel',
    'submit' => 'Submit',
    'show_club' => 'Show Clubs',
    'search' => 'Search',
    'back' => 'Back',
    'show_join_user' => 'Show join user',
    'send' => 'Send',
    'like' => 'Like',
    'comments' => 'Comments',
    'done' => 'Done',
    'reported' => 'Reported',
    'save' => 'Save',
    'login' => 'Login',
    'logout' => 'Logout',
];