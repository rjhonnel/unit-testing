<?php
return [
    'word' => [
        'id' => 'ID',
        'user' => 'User',
        'create' => 'Create new',
        'email' => 'Email',
        'password' => 'Password',
        'list' => 'List',
        'nickname' => 'Nickname',
        'gender' => 'Gender',
        'age' => 'Age',
        'height' => 'Height',
        'weight' => 'Weight',
        'plan' => 'Plan',
        'goal_weight' => 'Goal weight',
        'deadline' => 'Deadline',
        'image' => 'Image',
        'detail' => 'Detail',
        'male' => 'Male',
        'female' => 'Female',
        'goal_of_weight' => 'Goal of weight',
        'created_at' => 'Created at',
        'join_program' => 'Join programs',
        'program_id' => 'Program ID',
        'title' => 'Title',
        'description' => 'Description',
        'start' => 'Start',
        'end' => 'End',
        'club_join_user' => 'club join user'
    ],
    'message' => [
        'create_success' => 'User have been added successfully.'
    ]
];