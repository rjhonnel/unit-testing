<?php

return [
    'club_list' => 'Club list',
    'id' => 'Club list',
    'start_date' => 'Start date',
    'end_date' => 'End date',
    'number_of_join' => 'Number of join',
    'program_id' => 'Program ID',
    'program_title' => 'Program title',
    'program_description' => 'Program description',
    'club_id' => 'Club ID',
    'show_club' => 'Show club',
];