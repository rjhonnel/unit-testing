<?php

return [
    'update_goal' => 'Update Goal',
    'weight' => 'Weight',
    'deadline' => 'Deadline',
    'update_profile' => 'Update Profile',
    'nickname' => 'Nickname',
    'height' => 'Height',
    'birthday' => 'Birthday',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'plan' => 'Plan',
    'edit' => 'Edit',
    'goal_weight' => 'Goal weight',
];