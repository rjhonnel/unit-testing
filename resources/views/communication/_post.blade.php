  <div class="col-sm-12 box" id="post-{{ $post->id }}">
    <div class="row">
      <div class="col-xs-2">
        <img src="{{ $post->user->profile->getPhotoImage() }}" alt="" class="img-responsive img-circle">
      </div>
      <div class="col-xs-8 text-left">
        <b>{{ $post->user->profile->nickname }}</b>
        <p>{{ $post->created_at }}</p>
      </div>

      {{-- Popup Menu --}}
      <div class="col-xs-2">
        @if (Auth::user()->id == $post->user->id)
          <div class="dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-h pull-right"></i></a>
            <ul class="dropdown-menu" style="left: -125px!important">
              <li><a href="#" onclick="return removePost({{ $post->id }})"><i class="fa fa-trash"></i>  この投稿を削除する</a></li>
              <div class="dropdown-divider"></div>
              <li><a href="{{ route('posts.edit', $post->id) }}"><i class="fa fa-edit"></i>  この投稿を編集する</a></li>
              <div class="dropdown-divider"></div>
              <li><a href="#" onclick="shareSns({{ $post->id }})"><i class="fa fa-share-alt"></i>  この投稿をシェアする</a></li>
            </ul>
          </div>
        @endif
      </div>

      <div class="col-xs-12">
        <p>{{ $post->text }}</p>
      </div>

      @foreach($post->images as $image)
        <div class="col-xs-4">
          <img src="{{ url($image->image_url) }}" alt="" class="img-responsive">
        </div>
      @endforeach

      <div class="col-xs-12">
        @if ($post->userLikes->contains('pivot.user_id', Auth::user()->id))
          <p data-id="{{ $post->id }}" onclick="deleteLike({{ $post->id }})" class="fa fa-heart text-pink like-icon"></p>
        @else
          <p data-id="{{ $post->id }}" onclick="like({{ $post->id }})" class="fa fa-heart-o like-icon"></p>
        @endif
        <span class="like" data-id="{{ $post->id }}" onclick="likePage({{ $post->id }})">{{ $post->user_likes_count }} @lang('button.like')</span>
        &nbsp; &nbsp; &nbsp; &nbsp;
        <span onclick="clickComment({{ $post->id }})" class="comment-icon"><p class="fa fa-comment-o"></p> {{ $post->comments_count }} @lang('button.comments')</span>
      </div>
    </div>
  </div>
