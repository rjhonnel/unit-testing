@extends('layouts.app', ['title' => $club->program->title, 'back_url' => '/mypage' ])

@section('content')
<div class="container">
  @if (session('success'))
    <span class="col-xs-12 alert alert-success">
      <strong>{{ session('success') }}</strong>
    </span>
  @elseif (session('error'))
    <span class="col-xs-12 alert alert-danger">
      <strong>{{ session('error') }}</strong>
    </span>
  @endif


  {{-- Post --}}

  <div class="col-sm-12 box">
    <div class="row">
      <div class="col-xs-2">
        <img src="{{ Auth::user()->profile->getPhotoImage() }}" alt="" class="img-responsive img-circle">
      </div>
      <div class="col-xs-10">
        <input type="text" placeholder="@lang('communication.how_do_you_feel')" class="form-control" id="input-post"/>
      </div>
    </div>
  </div>

  {{-- Posts --}}

  @foreach ($posts as $post)
    @if ($post->doneReports->count() > 0)
    		{{-- Post of report --}}
    		@component('communication._postReport', [
               'postReport' => $post,
               'joinUserCount' => $joinUserCount,
               'club' => $club
            ])
        @endcomponent
    @else
    		@component('communication._post', ['post'=>$post]) @endcomponent
    @endif
  @endforeach

  {{ $posts->links() }}

</div>
@endsection

@section('style')

<style type="text/css" media="screen">

  .box {
    background: #FFF;
    padding: 8px;
    border: solid 1px #CCC;
    margin-bottom: 16px;
  }
  .like-icon, .comment-icon, .report-icon, .like, .report-type {
    cursor: pointer;
  }
</style>
@endsection

@section('script')
<script>
  function shareSns(id)
  {
    var domain = "{{ env('APP_URL') }}";
    var url = domain + '/posts/' + id;
    var fbpopup = window.open("https://www.facebook.com/sharer/sharer.php?u=" + url, "pop", "width=600, height=400, scrollbars=no");
    return false;
  }
  function clickComment($postId) {
    window.location = "/comments/" + $postId;
  }
  function likePage(postId) {
    window.location = "/post-likes/" + postId;
  }
  $("#input-post").click(function() {
    $(this).attr('disabled', 'disabled');
    window.location = '/posts/?club_id=' + {{ $club->id }};
  });

  function like(postId) {
    $(".like-icon[data-id='"+ postId +"']").css("pointer-events", "none");
    var countLike = $(".like[data-id='"+ postId +"']").text();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: '/like/posts/' + postId,
      type: 'POST',
      dataType: 'JSON',
    })
    .done(function(res) {
      $('.like-icon').css("pointer-events", 'auto');
      if (res.status == 1) {
        $(".like-icon[data-id='"+ postId +"']").removeClass('fa-heart-o').addClass('text-pink').addClass('fa-heart').attr('onclick', "deleteLike("+ postId +")");
        $(".like[data-id='"+ postId +"']").text(parseInt(countLike) + 1 + ' Like');
      }
    })
  }

  function deleteLike(postId) {
    $(".like-icon[data-id='"+ postId +"']").css("pointer-events", "none");
    var countLike = $(".like[data-id='"+ postId +"']").text();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: '/like/posts/' + postId,
      type: 'DELETE',
      dataType: 'JSON',
    })
    .done(function(res) {
      $('.like-icon').css("pointer-events", 'auto');
      if (res.status == 1) {
        $(".like-icon[data-id='"+ postId +"']").removeClass('fa-heart').removeClass('text-pink').addClass('fa-heart-o').attr('onclick', "like("+ postId +")");
        $(".like[data-id='"+ postId +"']").text(parseInt(countLike) - 1 + ' Like');
      }
    })
  }

  function removePost(postId)
  {
    if (confirm('{{ trans('message.sure_delete') }}')) {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: '/posts/delete/' + postId,
        type: 'DELETE',
        dataType: 'JSON',
      })
      .done(function(res) {
        if (res.status == 1) {
          $('#post-' + postId).remove();
        }
      })
    }
    return false;
  }

  function report(reportType, doneReportId) {
    if (reportType == 2) {
      window.location = '/report-weight/' + doneReportId;
      return;
    }
	var container = $("#report_"+doneReportId);
	container.find(".report-icon").css("pointer-events", "none");
    var countReport = container.find(".report").text();
    var total = {{ $joinUserCount }};
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: '/posts/report/' + doneReportId,
      type: 'POST',
      dataType: 'JSON',
    })
    .done(function(res) {
        container.find(".report-icon").css("pointer-events", "auto");
        if (res.status) {
            container.find(".report-icon").addClass("text-info").attr("onclick", "deleteReport("+ doneReportId +")");
            container.find(".report").text(parseInt(countReport) + 1);
            container.find(".progress-bar").css("width", 100*(parseInt(countReport) + 1)/total + "%");
            container.find(".ratio").text(parseInt(countReport) + 1 + "/" + total);
            container.find(".report-status").text("Reported");
        }
    })
  }

  function deleteReport(reportType, doneReportId) {
    if (reportType == 2) {
        return;
    }
    var container = $("#report_"+doneReportId);
    container.find(".report-icon").css("pointer-events", "none");
    var countReport = container.find(".report").text();
    var total = {{ $joinUserCount }};
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: '/posts/report/' + doneReportId,
      type: 'DELETE',
      dataType: 'JSON',
    })
    .done(function(res) {
        container.find(".report-icon").css("pointer-events", "auto");
        if (res.status) {
        		container.find(".report-icon").removeClass("text-info").attr("onclick", "report("+ doneReportId +")");
        		container.find(".report").text(parseInt(countReport) - 1);
        		container.find(".progress-bar").css("width", 100*(parseInt(countReport) - 1)/total + "%");
        		container.find(".ratio").text(parseInt(countReport) - 1 + "/" + total);
        		container.find(".report-status").text("Done");
        }
    })
  }
</script>
@endsection
