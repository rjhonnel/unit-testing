    <div class="col-sm-12 box">
      <div class="row" id="report_{{ $postReport->doneReportId }}">
        <div class="col-xs-2">
          <img src="{{ $postReport->user->profile->getPhotoImage() }}" alt="" class="img-responsive img-circle">
        </div>
        <div class="col-xs-8 text-left">
          <b>{{ $postReport->user->profile->nickname }}</b>
          <p>{{ substr($postReport->created_at, 0, 10) }}</p>
        </div>

        <div class="col-xs-12">
          <p>{{ $postReport->text }}</p>
        </div>
        <div class="col-xs-12">
          <p class="pull-left">@lang('communication.progress_program')</p>
          <p class="pull-right ratio">{{ $postReport->countDoneReportUsers }} / {{ $joinUserCount }}</p>
        </div>
        <div class="col-xs-12">
          <div class="progress">
            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: {{ 100 * $postReport->countDoneReportUsers/$joinUserCount }}%"></div>
          </div>
        </div>
        <div class="col-xs-12">
          {{-- check post repo.navbar-default rt by supporter --}}
          @if (empty(Auth::user()->supporter))

            {{-- check this user already reported --}}
            @if ($postReport->isDoneReport)
              <p data-id="{{ $postReport->doneReportId }}" onclick="deleteReport({{ $club->program->done_report_type }}, {{ $postReport->doneReportId }})" class="fa fa-lock text-info report-icon"></p>
              <span class="report" data-id="{{ $postReport->doneReportId }}">{{ $postReport->countDoneReportUsers }}</span>
              <span class="report-status"> @lang('button.reported')</span>
            @else
              <p data-id="{{ $postReport->doneReportId }}" onclick="report({{ $club->program->done_report_type }}, {{ $postReport->doneReportId }})" class="fa fa-lock report-icon"></p>
              <span class="report" data-id="{{ $postReport->doneReportId }}">{{ $postReport->countDoneReportUsers }}</span> <span class="report-status">@lang('button.done')</span>
            @endif
            &nbsp; &nbsp; &nbsp; &nbsp;
          @endif
          @if ($postReport->userLikes->contains('pivot.user_id', Auth::user()->id))
            <p data-id="{{ $postReport->id }}" onclick="deleteLike({{ $postReport->id }})" class="fa fa-heart text-pink like-icon"></p>
          @else
            <p data-id="{{ $postReport->id }}" onclick="like({{ $postReport->id }})" class="fa fa-heart-o like-icon"></p>
          @endif
          <span class="like" data-id="{{ $postReport->id }}" onclick="likePage({{ $postReport->id }})">{{ $postReport->userLikes->count() }} @lang('button.like')</span>
          &nbsp; &nbsp; &nbsp; &nbsp;
          <span onclick="clickComment({{ $postReport->id }})" class="comment-icon"><p class="fa fa-comment-o"></p> {{ $postReport->comments->count() }} @lang('button.comments')</span>
        </div>

      </div>
    </div>
