@extends("layouts.app")

@section('style')
  <style>
    .reported {color: #3097D1;}
    .st-count {font-size: 14px;color: #000;margin-left: 5px;}
    .mr-top-5{margin-top: 5px;}
    .ct-border {border-radius: 0px;margin-left:45px;}
    .st-bell{font-size: 17px; margin-top:10px;color:#000000;}
    .number-notice{background: red;color:#ffffff;font-size:10px;padding:2px;margin-left:-5px;}
  </style>
@endsection

@section("content")
<div class="container">
  <div class="block">
    <div class="row">
        <div class="col-xs-9"></div>
        <div class="col-xs-3">
          <a href="/notifications">
            <i class="fa fa-bell pull-right st-bell" aria-hidden="true">
              @if ($numberNewsNotifications > 0 )
                <span class="img-circle number-notice">
                  {{ $numberNewsNotifications }}
                </span>
              @endif
            </i>
          </a>
        </div>
      </div>

      <h4>参加プログラム一覧</h4>
      @include('particitals._program_joined_particital')
    </div>

	@if($programsClubs)
        <h4>@lang('programs.program_list_supporter')</h4>
        @foreach ($programsClubs as $program)
            @foreach ($program->clubs as $club)
             <div class="row block">
                <div class="col-xs-3">
                  <img src="{{ $program->image_url }}" class="img-circle" alt="Cinque Terre" width="60" height="60">
                </div>
                <div id="parent_div" class="col-xs-7">
                  <p >{{$club->title}} ({{$club->club_id}})</p>
                </div>
                <div class="col-xs-1">
                  <a href="{{ route('communications.list', $club->club_id) }}"><i class="fa fa-chevron-right" aria-hidden="true" style="margin-top: 1em;font-size: 20px"></i></a>
                </div>
            </div>
            @endforeach
        @endforeach
    @endif
  </div>

    @if (!empty($endProgram))
      <div id="myModal" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">{{ h($endProgram['programName']) }}の期間が終了しました！</h4>
            </div>
            <div class="modal-body">
              <h4>アクション達成状況</h4>
              <div class="container block">
                <table>
                  <tr>
                    @foreach($endProgram['dayOfWeeks'] as $w)
                      <td class="day_of_week">{{ $w }}</td>
                    @endforeach
                  </tr>
                  <tr>
                    @foreach($endProgram['days'] as $d)
                      <td class="day">{{ $d }}</td>
                    @endforeach
                  </tr>
                  <tr>
                    @foreach($endProgram['joins'] as $j)
                      @if ($j)
                       <td class="text-success">○</td>
                      @else
                       <td class="text-danger">×</td>
                      @endif
                    @endforeach
                  </tr>
                </table>
              </div>
              <a id="end_check_program" class="btn btn-primary" href="/programs">次のプログラムを探す</a>
            </div>
          </div>
        </div>
      </div>
    @endif
  </div>
</div>
@endsection

@section('style')
<style>
  .day_of_week {
    font-size: 18pt;
  }
  .day {
    font-size: 8pt;
  }
  td {
    width: 15%;
    text-align: center;
  }
</style>
@endsection

@section('script')
  <script>
    @if (!empty($endProgram))
      $(window).on('load',function(){
        $('#myModal').modal('show');
      });
  
      $(function() {
        $('#end_check_program').on('click', function (e) {
          e.preventDefault();
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
            type: 'POST',
            url: "{{ route('mypage.close', $endProgram['clubJoinUserId']) }}",
            data: {},
            dataType: 'json',
            success: function(data) {
              window.location.replace('{{ route('program.list') }}');
            }
          });
        });
      });
    @endif
    $(document).ready(function () {
      var enable = true;
      $('.daily_report .fa-lock').on('click', function () {
        if (enable) {
          var done_report_type = $(this).attr("data-id");
          if (done_report_type == 1) {
            var number = $(this).parent().children().find('.count_reported');
            var count_reported = $(number).text();
            var program_id = $(this).attr('data-program-id');
            $.ajax({
              type: "POST",
              url: "{{ route('ajax.daily_report') }}",
              data: {done_report_type:done_report_type, program_id:program_id},
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
              context: this,
              beforeSend: function() {
                enable = false;
                $(this).css('color', '#ccc');
              },
              success: function (data) {
                if (data.add == true) {
                  $(this).css('color', '#3097D1');
                  $(number).html(parseInt(count_reported) + 1);
                } else {
                  $(this).css('color', '#636b6f');
                  $(number).html(parseInt(count_reported) - 1);
                }
                enable = true;
              }
            });
          }
        }
      });
    })
  </script>
@endsection
