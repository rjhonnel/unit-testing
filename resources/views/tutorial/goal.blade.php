@extends("layouts.app", ['title' => trans('tutorial.word.set_goal'), 'no_link' => 'yes'])

@section("content")
<div class="container">
  <div class="row">
    @include('tutorial.step')

    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
      <form action="{{ route('tutorial.goal.post') }}" method="POST" role="form">
        {{ csrf_field() }}
        <div class="form-group">
          <label>@lang('tutorial.word.goal_weight')</label>
          <input type="number" min="1" max="1000" step="any" class="form-control" name="weight" value="{{ old('weight') }}">
        </div>
        @if ($errors->has('weight'))
          <p class="text-danger">{{ $errors->first('weight') }}</p>
        @endif
        <div class="form-group">
          <label>@lang('tutorial.word.deadline')</label>
          <input type="date" class="form-control" name="deadline" value="{{ old('deadline') }}">
        </div>
        @if ($errors->has('deadline'))
          <p class="text-danger">{{ $errors->first('deadline') }}</p>
        @endif
        <div class="col-xs-6 col-xs-offset-3">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">登録</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script>
    $("#step-6").removeAttr('disabled').removeClass('btn-default').addClass('btn-primary');
  </script>
@endsection
