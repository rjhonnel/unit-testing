@extends("layouts.app", ['title' => trans('tutorial.word.set_nickname'), 'no_link' => 'yes'])

@section("content")
<div class="container">
  <div class="row">
    @if (session('success'))
      <span class="col-xs-12 alert alert-success">
        <strong>{{ session('success') }}</strong>
      </span>
    @elseif (session('error'))
      <span class="col-xs-12 alert alert-danger">
        <strong>{{ session('error') }}</strong>
      </span>
    @endif
    @include('tutorial.step')

    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
      <form action="{{ route('tutorial.nickname.post') }}" method="POST" role="form">
        {{ csrf_field() }}
        <div class="form-group">
          <label>@lang('tutorial.word.nickname')</label>
          <input type="text" class="form-control" name="nickname" value="{{ old('nickname') }}">
        </div>
        @if ($errors->has('nickname'))
          <p class="text-danger">{{ $errors->first('nickname') }}</p>
        @endif
        <div class="col-xs-6 col-xs-offset-3">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">登録</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script>
    $("#step-1").removeAttr('disabled').removeClass('btn-default').addClass('btn-primary');
  </script>
@endsection
