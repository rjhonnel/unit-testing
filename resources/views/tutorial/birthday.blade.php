@extends("layouts.app", ['title' => trans('tutorial.word.set_birthday'), 'no_link' => 'yes'])

@section("content")
<div class="container">
  <div class="row">
    @include('tutorial.step')

    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
      <form action="{{ route('tutorial.birthday.post') }}" method="POST" role="form">
        {{ csrf_field() }}
        <div class="form-group">
          <label>@lang('tutorial.word.birthday')</label>
          <input type="date" class="form-control" name="birthday" value="{{ old('birthday') }}">
        </div>
        @if ($errors->has('birthday'))
          <p class="text-danger">{{ $errors->first('birthday') }}</p>
        @endif
        <div class="col-xs-6 col-xs-offset-3">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">登録</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script>
    $("#step-4").removeAttr('disabled').removeClass('btn-default').addClass('btn-primary');
  </script>
@endsection
