@extends("layouts.app", ['title' => trans('tutorial.word.confirm_data'), 'no_link' => 'yes'])

@section("content")
<div class="container">
  <div class="row">
    @include('tutorial.step')

    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
      <table class="table">
        <tbody>
          <tr>
            <td>@lang('tutorial.word.nickname')</td>
            <td>{{ session('nickname') }}</td>
          </tr>
          <tr>
            <td>@lang('tutorial.word.height')</td>
            <td>{{ session('height') }}cm</td>
          </tr>
          <tr>
            <td>@lang('tutorial.word.weight')</td>
            <td>{{ session('weight') }}Kg</td>
          </tr>
          <tr>
            <td>@lang('tutorial.word.gender')</td>
            <td>{{ session('gender') == 1 ? trans('tutorial.word.male') : trans('tutorial.word.female') }}</td>
          </tr>
          <tr>
            <td>@lang('tutorial.word.birthday')</td>
            <td>{{ session('birthday') }}</td>
          </tr>
          <tr>
            <td>@lang('tutorial.word.plan')</td>
            <td>{{ config('profile.plan')[session('plan')] }}</td>
          </tr>
          @if (!empty(session('goal_weight')))
            <tr>
              <td>@lang('tutorial.word.goal_weight')</td>
              <td>{{ session('goal_weight') }}Kg</td>
            </tr>
            <tr>
              <td>@lang('tutorial.word.deadline')</td>
              <td>{{ session('deadline') }}</td>
            </tr>
          @endif
        </tbody>
      </table>
      <div class="col-xs-12 col-sm-offset-3">
        <form action="{{ route('tutorial.confirm.post') }}" method="POST" role="form">
          {{ csrf_field() }}
          <input type="submit" class="btn btn-default" name="cancel" value="Cancel">
          <input type="submit" class="btn btn-primary" name="ok" value="OK">
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script>
    $("#step-7").removeAttr('disabled').removeClass('btn-default').addClass('btn-primary');
  </script>
@endsection
