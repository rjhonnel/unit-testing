@extends('layouts.app', ['no_link' => 'yes'])

@section('content')
  <div class="container">
    <div class="col-sm-12">
      <div id="messages">
      </div>
    </div>
  </div>
@endsection

@section('script')
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.ja.js') }}"></script>
    <script>
        $(function() {
            var profile = {};
            var goal = {};
            var plan = [null, '体重を減らす', '体重をキープ' ,'筋力アップ'];

            // Scenario
            var scenario = [];

            if (window.location.search == "") {
                scenario.push(function() { show_say("あなたのサポートを担当します。よろしくお願いします。"); });
                scenario.push(function() { show_say("美活部でなりたいからだを、目指しましょう"); });
            }
            scenario.push(function() { show_say("名前を教えてください。"); });
            scenario.push(function() { show_input('@input_nickname_ニックネーム'); });
            scenario.push(function() { show_say(profile.nickname + "さんですね、よろしくお願いします。"); });
            scenario.push(function() { show_say("最適なプログラムを提案できるように、" + profile.nickname + 'さんの身長と体重を教えてください' ); });
            scenario.push(function() { show_input('@input_height_身長'); });
            scenario.push(function() { show_input('@input_weight_体重'); });
            scenario.push(function() { show_say("あなたの性別を教えてください。"); });
            scenario.push(function() { show_radio('@radio_gender_性別'); });
            scenario.push(function() { show_say("あなたの誕生日を教えてください。"); });
            scenario.push(function() { show_input('@input_birthday_誕生日'); });

            scenario.push(function() { show_say("プランを教えてください"); });
            scenario.push(function() { show_radio("@radio_plan_プラン"); });

            // Goal weight & deadline
            scenario.push(function() { if(profile.plan==1) { show_say("目標体重と、期日を設定しましょう。") } else { nextMessage(); }} );
            scenario.push(function() { if(profile.plan==1) { show_input("@input_goalweight_目標体重") } else { nextMessage(); }} );
            scenario.push(function() { if(profile.plan==1) { show_input("@input_deadline_期日") } else { nextMessage(); }} );

            // Confirm
            scenario.push(function() {
                text = '<p>以下内容でまちがいないですか？</p>';
                text = text + "<p>ニックネーム:" + profile.nickname + "</p>";
                text = text + "<p>身長:" + profile.height + "</p>";
                text = text + "<p>体重:" + profile.weight + "</p>";
                text = text + "<p>誕生日:" + profile.birthday + "</p>";
                text = text + "<p>性別:" + (profile.gender == 1 ? "男性" : "女性") + "</p>";
                text = text + "<p>プラン:" + plan[profile.plan] + "</p>";

                if(profile.plan == 1) {
                    text = text + "<p>目標体重:" + goal.goalweight + "</p>";
                    text = text + "<p>期日:" + goal.deadline + "</p>";
                }
                show_say(text);
            });
            scenario.push(function() { show_confirm(); });

            //
            scenario.push(function() { show_say("あなたの入力した情報を元にオススメのプログラムを用意しました。") });
            scenario.push(function() { show_recommend(); });

            // Start Scenario
            nextMessage();


            // ---
            function show_input(msg) {
                var $table = $("<table><tr>");
                let array = msg.split('_');
                $fields = $("<td><div>").addClass("form-group pull-right");
                $fields.append($("<label>")).text(array[2]);
                if (array[1] != 'birthday' && array[1] != 'deadline') {
                    $fields.append($("<input>").attr("type", "text").attr("id", array[1]).addClass("form-control").addClass(array[1]));
                } else if (array[1] == 'birthday') {
                    $fields.append($("<input>").attr("type", "date").attr("id", array[1]).attr('value', '1990-01-01').addClass("form-control").addClass(array[1]));
                } else if (array[1] == 'deadline') {
                    $fields.append($("<input>").attr("type", "date").attr("id", array[1]).addClass("form-control").addClass(array[1]));
                }

                $button = $('<button>');
                $fields.append(($button).addClass("btn btn-primary pull-right").text("OK").attr("style", "margin-top: 4px").click(function() {
                    var error = validate(array[1], $('.'+array[1]).val());
                    if (error != '') {
                        alert(error);
                    } else {
                        $('#' + array[1]).attr('disabled', 'disabled');
                        $button.hide();
                        nextMessage();
                    }
                }));
                $table.append($fields).attr("style", "width: 100%");

                $("#messages").append($table);
                scrollTo();
            }

            function show_radio(msg) {
                var $table = $("<table><tr>");
                let array = msg.split('_');
                $fields = $("<td><div>").addClass("form-group pull-right");
                if (array[1] == 'gender') {
                    $fields.append($("<label>")).text(array[2]);
                    $fields.append("<br>男性 &nbsp;");
                    $fields.append($("<input>").attr("type", "radio").attr("name", "gender").addClass(array[1]).val(1));
                    $fields.append("&nbsp;&nbsp;&nbsp;女性 &nbsp;");
                    $fields.append($("<input>").attr("type", "radio").attr("name", "gender").addClass(array[1]).val(2));
                    $fields.append("<br>");
                } else {
                    $fields.append($("<label>")).text(array[2]);
                    $fields.append("<br>");
                    $fields.append($("<input>").attr("type", "radio").attr("name", "plan").addClass(array[1]).val(1));
                    $fields.append("&nbsp;&nbsp; " + plan[1] + "<br>");
                    $fields.append($("<input>").attr("type", "radio").attr("name", "plan").addClass(array[1]).val(2));
                    $fields.append("&nbsp;&nbsp; " + plan[2] + " <br>");
                    $fields.append($("<input>").attr("type", "radio").attr("name", "plan").addClass(array[1]).val(3));
                    $fields.append("&nbsp;&nbsp; " + plan[3] + " <br>");
                }
                $fields.append($("<button>").addClass("btn btn-primary pull-right").text("OK").attr("style", "margin-top: 4px").click(function() {
                    var error = validate(array[1], $('.'+array[1] + ':checked').val());
                    if (error != '') {
                        alert(error);
                    } else {
                        nextMessage();
                        $('button').hide();
                    }
                    $('.' + array[1]).attr('disabled', 'disabled');
                }));
                $table.append($fields).attr("style", "width: 100%");
                $("#messages").append($table);
                scrollTo();
            }

            function show_say(msg) {
                var $table = $("<table><tr>");
                $message = $('<p>');
                $table.append($("<tr>")
                    .append(
                        $("<td>")
                            .addClass("icon")
                            .append(
                                $("<img>")
                                    .addClass("img-circle icon-image")
                                    .attr("src", "/images/woman.png")
                        )
                    )
                    .append(
                        $("<td>")
                            .addClass("message")
                            .append(
                                $("<div>")
                                    .addClass("balloon2-left")
                                    .append(
                                        $message.text('...')
                                    )
                            )
                    )
                );

                $("#messages").append($table);
                scrollTo();

                setTimeout(function() {
                    $message.html(msg);
                    scrollTo();
                    setTimeout(function() {
                        nextMessage();
                    }, 2000);
                }, 500);
            }

            function show_confirm() {

                $fields = $("<div id='confirm'>").addClass("form-group pull-right");
                $fields.append(`<a href="/tutorial/?reset=true" class="btn btn-default">NO</a> &nbsp;`);
                $fields.append(
                    $("<a>").addClass("btn btn-primary").text("YES").click(function() {
                        $('#confirm').hide();

                        // Update Profile & goal
                        createProfile(profile, function() {
                            if (goal.deadline !== undefined) {
                                setGoal(goal, function() { nextMessage(); });
                            } else {
                                nextMessage();
                            }
                        });

                    })
                );
                $("#messages").append($fields);
                scrollTo();
            }

            function show_recommend() {
                $("#messages").append(`<a href="/recommend_programs" title="" class="btn btn-primary pull-right">オススメプログラムを見る</a>`);
                scrollTo();
            }

            function nextMessage() {
                s = scenario.shift();
                if(s === undefined) {
                    return;
                }
                s();
            }

            function scrollTo() {
                $("html,body").animate( { scrollTop: 999999 } );
            }



            // --- check functions

            function checkEmpty(key, value) {
                if (value == '') {
                    return 'The ' + key + ' field is required\n';
                }
                return '';
            }

            function checkString(key, value) {
                var pattern = /^[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[a-zA-Z0-9]+|[ａ-ｚＡ-Ｚ０-９]+|[々〆〤]+$/;
                if (!pattern.test(value)) {
                    return 'The ' + key + ' field is string\n';
                }
                return '';
            }

            function checkNumeric(key, value) {
                var pattern = /\D/ig;
                if (pattern.test(value)) {
                    return 'The ' + key + ' field is numeric\n';
                }
                return '';
            }

            function checkDecimal(key, value) {
                var pattern = /^[0-9]{1,4}(\.[0-9]{1})?$/;
                if (!pattern.test(value)) {
                    return 'The ' + key + ' field is decimal';
                }
                return '';
            }

            function checkDateFormat(key, value) {
                var pattern = /^[0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}$/;
                if (!pattern.test(value)) {
                    return 'The ' + key + ' field is date format (dd/mm/yyyy)\n';
                }
                var timeArray = value.split('-');
                date = new Date();
                if (key == 'birthday') {
                    if (timeArray[0] > date.getFullYear()) {
                        return 'The ' + key + ' field can\'t after year ' + date.getFullYear();
                    }
                    if (timeArray[1] > (date.getMonth() + 1) && timeArray[0] == date.getFullYear()) {
                        return 'The ' + key + ' field can\'t after month ' + (date.getMonth() + 1);
                    }
                    if (timeArray[2] > date.getDate() && timeArray[1] == (date.getMonth() + 1) && timeArray[0] == date.getFullYear()) {
                        return 'The ' + key + ' field can\'t after date ' + date.getDate();
                    }
                }
                if (key == 'deadline') {
                    if (timeArray[0] < date.getFullYear()) {
                        return 'The ' + key + ' field can\'t before year ' + date.getFullYear();
                    }
                    if (timeArray[1] < (date.getMonth() + 1) && timeArray[0] == date.getFullYear()) {
                        return 'The ' + key + ' field can\'t before month ' + (date.getMonth() + 1);
                    }
                    if (timeArray[2] < date.getDate() && timeArray[1] == (date.getMonth() + 1) && timeArray[0] == date.getFullYear()) {
                        return 'The ' + key + ' field can\'t before date ' + date.getDate();
                    }
                }
                return '';
            }

            function validate(key, value) {
                if (key == 'plan' || key == 'height' || key == 'gender') {
                    let error = checkEmpty(key, value) + checkNumeric(key, value);
                    if (error != '') {
                        return error;
                    }
                    profile[key] = value;

                    return error;
                }
                if (key == 'weight') {
                    let error = checkEmpty(key, value) + checkDecimal(key, value);
                    if (error != '') {
                        return error;
                    }
                    profile[key] = value;

                    return error;
                }
                if (key == 'nickname') {
                    let error = checkEmpty(key, value) + checkString(key, value);
                    if (error != '') {
                        return error;
                    }
                    profile[key] = value;
                    return error;
                }
                if (key == 'birthday') {
                    let error = checkEmpty(key, value) + checkDateFormat(key, value);
                    if (error != '') {
                        return error;
                    }
                    profile[key] = value;
                    return error;
                }
                if (key == 'goalweight') {
                    let error = checkEmpty(key, value) + checkDecimal(key, value);
                    if (error != '') {
                        return error;
                    }
                    goal[key] = value;
                    return error;
                }
                if (key == 'deadline') {
                    let error = checkEmpty(key, value) + checkDateFormat(key, value);
                    if (error != '') {
                        return error;
                    }
                    goal[key] = value;

                    return error;
                }
            }


            // ---
            // Update Progfile
            function createProfile(data, cb) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route('profile.create') }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                })
                .done(function(response) {
                    cb();
                })
                .fail(function(err) {
                    alert('エラーが発生しました。通信状況の良いところでもう一度試してください');
                    $('#confirm').show();
                });

            }

            function setGoal(data, cb) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route('goal.create') }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                })
                .done(function(response) {
                    cb();
                })
                .fail(function(err) {
                    alert('エラーが発生しました。通信状況の良いところでもう一度試してください');
                    $('#confirm').show();
                });
            }
        });
    </script>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
<style>
  .balloon2-left {
    position: relative;
    display: inline-block;
    margin: 1.5em 0 1.5em 15px;
    padding: 7px 10px;
    min-width: 120px;
    max-width: 100%;
    color: #555;
    font-size: 16px;
    background: #e9e9f5;
    border-radius: 8px;
    box-sizing: border-box;
  }
  .balloon2-left:before{
    content: "";
    position: absolute;
    top: 50%;
    left: -24px;
    margin-top: -12px;
    border: 12px solid transparent;
    border-right: 12px solid #e9e9f5;
    z-index: 2;
  }
  .balloon2-left:after{
    content: "";
    position: absolute;
    top: 50%;
    left: -30px;
    margin-top: -14px;
    border: 14px solid transparent;
    z-index: 1;
  }
  .balloon2-left p {
    margin: 0;
    padding: 0;
  }
  #id {
    height: 100%;
    overflow: scroll;
    background: #fbb;
  }
  .icon-image {
        width: 64px;
        height: 64px;
        margin: 8px;
    }
</style>
@endsection
