@extends("layouts.app", ['title' => trans('tutorial.word.set_height_weight'), 'no_link' => 'yes'])

@section("content")
<div class="container">
  <div class="row">
    @include('tutorial.step')

    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
      <form action="{{ route('tutorial.height_weight.post') }}" method="POST" role="form">
        {{ csrf_field() }}
        <div class="form-group">
          <label>@lang('tutorial.word.height')</label>
          <input type="number" class="form-control" name="height" value="{{ old('height') }}">
        </div>
        @if ($errors->has('height'))
          <p class="text-danger">{{ $errors->first('height') }}</p>
        @endif
        <div class="form-group">
          <label>@lang('tutorial.word.weight')</label>
          <input type="number" min="1" max="1000" step="any" class="form-control" name="weight" value="{{ old('weight') }}">
        </div>
        @if ($errors->has('weight'))
          <p class="text-danger">{{ $errors->first('weight') }}</p>
        @endif
        <div class="col-xs-6 col-xs-offset-3">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">登録</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script>
    $("#step-2").removeAttr('disabled').removeClass('btn-default').addClass('btn-primary');
  </script>
@endsection
