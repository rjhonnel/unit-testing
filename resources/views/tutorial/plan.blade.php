@extends("layouts.app", ['title' => trans('tutorial.word.set_plan'), 'no_link' => 'yes'])

@section("content")
<div class="container">
  <div class="row">
    @include('tutorial.step')

    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
      <form action="{{ route('tutorial.plan.post') }}" method="POST" role="form">
        {{ csrf_field() }}
        <div class="form-group">
          <label>@lang('tutorial.word.plan')</label>
          <select name="plan" class="form-control">
              <option value="1">体重を減らす</option>
              <option value="2">体重をキープ</option>
              <option value="3">筋力アップ</option>
          </select>
        </div>
        @if ($errors->has('plan'))
          <p class="text-danger">{{ $errors->first('plan') }}</p>
        @endif
        <div class="col-xs-6 col-xs-offset-3">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">登録</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script>
    $("#step-5").removeAttr('disabled').removeClass('btn-default').addClass('btn-primary');
  </script>
@endsection
