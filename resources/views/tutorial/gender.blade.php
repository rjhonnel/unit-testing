@extends("layouts.app", ['title' => trans('tutorial.word.set_gender'), 'no_link' => 'yes'])

@section("content")
<div class="container">
  <div class="row">
    @include('tutorial.step')

    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
      <form action="{{ route('tutorial.gender.post') }}" method="POST" role="form">
        {{ csrf_field() }}
        <div class="form-group text-center">
          <div class="radio">
            <input id="male" type="radio" name="gender" value="1">
            <label for="male"><b>@lang('tutorial.word.male')</b></label>
            <input id="female" type="radio" name="gender" value="2">
            <label for="female"><b>@lang('tutorial.word.female')</b></label>
          </div>
        </div>
        @if ($errors->has('gender'))
          <p class="text-danger">{{ $errors->first('gender') }}</p>
        @endif
        <div class="col-xs-6 col-xs-offset-3">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">登録</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script>
    $("#step-3").removeAttr('disabled').removeClass('btn-default').addClass('btn-primary');
  </script>
@endsection
@section('style')
  <style type="text/css" media="screen">
    label {display: inline-block; cursor: pointer; position: relative; padding-left: 25px; margin-right: 15px; font-size: 13px; }
    input[type=radio] {display: none;}
    label:before {content: ""; display: inline-block; width: 16px; height: 16px; margin-right: 10px; position: absolute; left: 0; bottom: 1px; background-color: #aaa; box-shadow: inset 0px 2px 3px 0px rgba(0, 0, 0, .3), 0px 1px 0px 0px rgba(255, 255, 255, .8); }
    .radio label:before {border-radius: 8px; }
    input[type=radio]:checked + label:before {content: "\2022"; color: #f3f3f3; font-size: 30px; text-align: center;line-height: 18px;}
  </style>
@endsection
