@extends('admin.layouts.master')

@section('style')

@endsection

@section('content')
  <div id="page-wrapper">
    @if (session('success'))
       <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('success') }}
      </div>
    @endif
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h3>@lang('club.club_list')</h3>
          <hr style="margin-top:5px;">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <table id="program-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>@lang('club.id')</th>
                <th>@lang('club.start_date')</th>
                <th>@lang('club.end_date')</th>
                <th>@lang('club.number_of_join')</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($clubs as $club)
              <tr>
                <td>{{ $club->id }}</td>
                <td>{{ formatDateTime($club->created_at) }}</td>
                <td>{{ $club->getEndDateOfClub($club->id) }}</td>
                <td>{{ $club->joinUsers()->count(). '/'  . $club->program->capacity }}</td>
                <td><a href="{{ route('admin.club.detail', $club->id) }}" target="_blank" type="button" class="btn btn-primary">@lang('button.show')</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>

        </div>
      </div>
      {{ $clubs->links() }}
    </div>
  </div>
@endsection