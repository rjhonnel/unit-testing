@extends('admin.layouts.master')

@section('content')
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h3>@lang('club.show_club')</h3>
          <hr style="margin-top:5px;">
        </div>
       </div>
       <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('club.program_id')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $club->program->id }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('club.program_title')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $club->program->title }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('club.program_description')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $club->program->description }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('club.club_id')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $club->id }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('club.start_date')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ formatDateTime($club->created_at) }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('club.end_date')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $club->getEndDateOfClub($club->id) }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('club.number_of_join')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $club->joinUsers()->count() . '/' . $club->program->capacity }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12" style="margin-top :10px;margin-left: 15px;">
          <a type="button" class="btn btn-primary" href="{{ route('admin.club_join_user.list') }}?club_id={{ $club->id }}">@lang('button.show_join_user')</a>
        </div>
      </div>
    </div>
  </div>
@endsection