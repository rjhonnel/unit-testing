@extends('admin.layouts.master', ['no_link'=>true])

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <div class="login-panel panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">@lang('auth.pls_sign_in')</h3>
        </div>
        <div class="panel-body">
          <form role="form" action="{{ route('post.admin.login') }}" method="POST">
            {{ csrf_field() }}
            <fieldset>
              <div class="form-group">
                <input class="form-control" placeholder="E-mail" name="email" type="email" value="{{ old('email') }}" autofocus required>
                @if ($errors->has('email'))
                  <p class="text-danger">{{ $errors->first('email') }}</p>
                @endif
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Password" name="password" type="password" required>
                @if ($errors->has('password'))
                  <p class="text-danger">{{ $errors->first('password') }}</p>
                @endif
              </div>
              <div class="checkbox">
                <label>
                  <input name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>@lang('auth.remember_me')
                </label>
              </div>
              <!-- Change this to a button or input when using this as a form -->
              <button class="btn btn-lg btn-success btn-block">@lang('auth.login')</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
