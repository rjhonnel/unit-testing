@extends('admin.layouts.master')

@section('content')
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="col-xs-12">

        <div class="row">
          <h3 class="page-header">@lang('supporter.word.show')</h3>
          <div class="col-xs-12">

            <table class="table text-left">
              <tbody>
                <tr>
                  <td style="width: 200px"><b>@lang('supporter.word.supporter_id')</b></td>
                  <td colspan="2">{{ $supporter->id }}</td>
                </tr>
                <tr>
                  <td style="width: 200px"><b>@lang('supporter.word.user_id')</b></td>
                  <td colspan="2">{{ $supporter->user->id }}</td>
                </tr>
                <tr>
                  <td><b>@lang('supporter.word.nickname')</b></td>
                  <td colspan="2">{{ $supporter->user->profile->nickname or '--' }}</td>
                </tr>
                <tr>
                  <td><b>@lang('supporter.word.self_introduction')</b></td>
                  <td colspan="2">{{ $supporter->user->profile->self_introduction or '--' }}</td>
                </tr>
                <tr>
                  <td><b>@lang('supporter.word.image')</b></td>
                  <td class="text-center" colspan="2">
                    @if (!empty($supporter->user->profile))
                      <img src="{{ $supporter->user->profile->getPhotoImage() }}" alt="" class="avatar">
                    @endif
                  </td>
                </tr>
                <tr>
                  <td><b>@lang('supporter.word.cover_image')</b></td>
                  <td class="text-center" colspan="2">
                    @if (!empty($supporter->user->profile))
                      <img src="{{ $supporter->user->profile->getCoverPhoto() }}" alt="" class="avatar">
                    @endif
                  </td>
                </tr>
                <tr>
                  <td><b>@lang('supporter.word.email')</b></td>
                  <td colspan="2">{{ $supporter->user->email }}</td>
                </tr>

                </tr>
                  <td colspan="3"><b>@lang('supporter.word.manage_programs')</b></td>
                <tr>
                <tr>
                  <td>@lang('supporter.word.program_id')</td>
                  <td>@lang('supporter.word.title')</td>
                  <td style="width: 400px">@lang('supporter.word.description')</td>
                </tr>
                @if (!empty($supporter->programs))
                  @foreach ($supporter->programs as $item)
                    <tr>
                      <td>{{ $item->id }}</td>
                      <td>{{ $item->title }}</td>
                      <td>{{ $item->description }}</td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          <a href="javascript: history.go(-1)" class="btn btn-info"><i class="fa fa-angle-double-left"></i> @lang('button.back')</a><br><br><br><br>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('style')
  <style type="text/css" media="screen">
    .avatar { max-width: 200px }
  </style>
@endsection