@extends('admin.layouts.master')

@section('content')
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="col-xs-12">

        <div class="row">
          <h3 class="page-header">@lang('supporter.word.edit')</h3>
          <div class="col-xs-12">
            <form action="{{ route('admin.supporter.update', $supporter->id) }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{ method_field('PUT') }}
              <div class="input-group col-xs-12 col-md-12 {{ $errors->has('nickname') ? 'has-error' : '' }}">
                <div class="col-xs-3 col-md-2">
                  <b>@lang('supporter.word.nickname')<span class="text-danger">*</span></b>
                </div>
                <div class="col-xs-9 col-md-6">
                  <input type="text" class="form-control col-xs-10" name="nickname" value="{{ $supporter->user->profile->nickname or old('nickname') }}">
                </div>
                @if ($errors->has('nickname'))
                  <div class="col-xs-9 col-xs-offset-3 col-md-6 col-md-offset-2">
                    <span class="help-block text-danger">
                      <strong>{{ $errors->first('nickname') }}</strong>
                    </span>
                  </div>
                @endif
              </div>
              <br>

              <div class="input-group col-xs-12 col-md-12 {{ $errors->has('self_introduction') ? 'has-error' : '' }}">
                <div class="col-xs-3 col-md-2">
                  <b>@lang('supporter.word.self_introduction')<span class="text-danger">*</span></b>
                </div>
                <div class="col-xs-9 col-md-6">
                  <input type="text" class="form-control col-xs-10" name="self_introduction" value="{{ $supporter->user->profile->self_introduction or old('self_introduction') }}">
                </div>
                @if ($errors->has('self_introduction'))
                  <div class="col-xs-9 col-xs-offset-3 col-md-6 col-md-offset-2">
                    <span class="help-block text-danger">
                      <strong>{{ $errors->first('self_introduction') }}</strong>
                    </span>
                  </div>
                @endif
              </div>
              <br>

              <div class="input-group col-xs-12 col-md-12 {{ $errors->has('image') ? 'has-error' : '' }}">
                <div class="col-xs-3 col-md-2">
                  <b>@lang('supporter.word.image')</b>
                </div>
                <div class="col-xs-9 col-md-6">
                  @if (!empty($supporter->user->profile))
                    <img src="{{ $supporter->user->profile->photo_image_url }}" class="bb-image-upload" id="image">
                  @endif
                  <input type="file" name="image" onchange="readURL(this, 1);">
                </div>
                @if ($errors->has('image'))
                  <div class="col-xs-9 col-xs-offset-3 col-md-6 col-md-offset-2">
                    <span class="help-block text-danger">
                      <strong>{{ $errors->first('image') }}</strong>
                    </span>
                  </div>
                @endif
              </div>
              <br>

              <div class="input-group col-xs-12 col-md-12 {{ $errors->has('cover_image') ? 'has-error' : '' }}">
                <div class="col-xs-3 col-md-2">
                  <b>@lang('supporter.word.cover_image')</b>
                </div>
                <div class="col-xs-9 col-md-6">
                  @if (!empty($supporter->user->profile))
                    <img src="{{ $supporter->user->profile->cover_image_url }}" alt="" class="bb-image-upload" id="cover_image">
                  @endif
                  <input type="file" name="cover_image" onchange="readURL(this, 2);">
                </div>
                @if ($errors->has('cover_image'))
                  <div class="col-xs-9 col-xs-offset-3 col-md-6 col-md-offset-2">
                    <span class="help-block text-danger">
                      <strong>{{ $errors->first('cover_image') }}</strong>
                    </span>
                  </div>
                @endif
              </div>
              <br>

              <div class="input-group col-xs-12 col-md-12 {{ $errors->has('email') ? 'has-error' : '' }}">
                <div class="col-xs-3 col-md-2">
                  <b>@lang('supporter.word.email')<span class="text-danger">*</span></b>
                </div>
                <div class="col-xs-19 col-md-6">
                  <input type="text" class="form-control col-xs-10" name="email" value="{{ $supporter->user->email or old('email') }}" disabled="">
                </div>
                @if ($errors->has('email'))
                  <div class="col-xs-9 col-xs-offset-3 col-md-6 col-md-offset-2">
                    <span class="help-block text-danger">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  </div>
                @endif
              </div>
              <br>

              <div class="input-group col-xs-12 col-md-12 {{ $errors->has('password') ? 'has-error' : '' }}">
                <div class="col-xs-3 col-md-2">
                  <b>@lang('supporter.word.password')</b>
                </div>
                <div class="col-xs-19 col-md-6">
                  <input type="text" class="form-control col-xs-10" name="password">
                </div>
                @if ($errors->has('password'))
                  <div class="col-xs-9 col-xs-offset-3 col-md-6 col-md-offset-2">
                    <span class="help-block text-danger">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  </div>
                @endif
              </div><br>

              <div class="col-xs-12 col-md-9 text-center">
                <button type="reset" class="btn btn-default">@lang('button.cancel')</button>
                <button type="submit" class="btn btn-primary">@lang('button.submit')</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
  <script>
    function readURL(input, type) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            if (type == 1) {
              $("#image").attr('src', e.target.result).addClass('bb-image');
            } else {
              $("#cover_image").attr('src', e.target.result).addClass('bb-image-cover');
            }
          };
          reader.readAsDataURL(input.files[0]);
      }
  }
  </script>
@endsection
