@extends('admin.layouts.master')

@section('content')
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="col-xs-12">

        <div class="row">
          <h3 class="page-header col-xs-12">
            @lang('supporter.word.list')
            <a href="{{ route('admin.supporter.create') }}" class="btn btn-primary pull-right" title=""><i class="fa fa-plus" aria-hidden="true"></i> @lang('supporter.word.create')</a>
          </h3>
          <div class="col-xs-12">
            @if (session('success'))
              <div class="alert alert-success">
                <strong>{{ session('success') }}</strong>
              </div>
            @elseif (session('error'))
              <div class="alert alert-danger">
                <strong>{{ session('error') }}</strong>
              </div>
            @endif
          </div>

          <div class="row">
            <div class="col-xs-12">
              <table id="program-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>@lang('supporter.word.id')</th>
                    <th>@lang('supporter.word.user_id')</th>
                    <th>@lang('supporter.word.nickname')</th>
                    <th>@lang('supporter.word.image')</th>
                    <th>@lang('supporter.word.cover_image')</th>
                    <th>@lang('supporter.word.self_introduction')</th>
                    <th style="width: 170px"></th>
                  </tr>
                </thead>
                <tbody>
                  @if (!empty($supporters))
                    @foreach ($supporters as $item)
                      <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->user->id }}</td>
                        <td>{{ $item->user->profile->nickname or '--' }}</td>
                        <td class="text-center">
                          @if (!empty($item->user->profile))
                            <img src="{{ $item->user->profile->getPhotoImage() }}" alt="" class="bb-image">
                          @endif
                        </td>
                        <td class="text-center">
                          @if (!empty($item->user->profile))
                            <img src="{{ $item->user->profile->getCoverPhoto() }}" alt="" class="bb-image-cover">
                          @endif
                        </td>
                        <td>{{ $item->user->profile->self_introduction or '--' }}</td>
                        <td>
                          <a href="{{ route('admin.supporter.show', $item->id) }}" class="btn btn-info" title="action">@lang('supporter.word.show') <i class="fa fa-angle-double-right"></i></a>
                          <a href="{{ route('admin.supporter.edit', $item->id) }}" class="btn btn-primary" title="action">@lang('supporter.word.edit') <i class="fa fa-edit"></i></a>
                        </td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {{ $supporters->links() }}
      </div>
    </div>
  </div>
@endsection
@section('style')
  <style type="text/css" media="screen">
    .avatar { max-width: 100px }
  </style>
@endsection
