@extends('admin.layouts.master')

@section('content')
  <div id="page-wrapper">
    <div class="container-fluid">
      @if (session('error'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          {{ session('error') }}
        </div>
      @endif
      <div class="row">
        <div class="col-lg-12">
          <h3>@lang('programs.edit')</h3>
          <hr style="margin-top:5px;">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-9 col-lg-offset-1">
          <form class="form-horizontal" action="{{ route('admin.programs.update', $program->id) }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
              <label class="control-label col-lg-3" for="title">@lang('programs.title') <span class="text-danger">*</span></label>
              <div class="col-lg-9">
                <input type="text" class="form-control" name="title" value="{{ isset($program->title) ? $program->title : old('title') }}">
                {{ validateForm($errors, 'title') }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3" for="description">@lang('programs.description') <span class="text-danger">*</span></label>
              <div class="col-lg-9">
                <textarea class="form-control" rows="5" name="description">{{ isset($program->description) ? $program->description : old('description') }}</textarea>
                {{ validateForm($errors, 'description') }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3" for="done_report_type">@lang('programs.done_report_type') <span class="text-danger">*</span></label>
              <div class="col-lg-5">
                <select id="done_report_type" class="form-control" name="done_report_type">
                  <option value="1">@lang('programs.normal')</option>
                  <option value="2">@lang('programs.weight')</option>
                  <option value="3">@lang('programs.image')</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3" for="capacity">@lang('programs.capacity') <span class="text-danger">*</span></label>
              <div class="col-lg-4">
                <input type="number" class="form-control" name="capacity" value="{{ $program->capacity}}">
                {{ validateForm($errors, 'capacity') }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3" for="program_days">@lang('programs.program_day') <span class="text-danger">*</span></label>
              <div class="col-lg-4">
                <input type="number" class="form-control" name="program_days" value="{{ $program->program_days }}">
                {{ validateForm($errors, 'program_days') }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3" for="program_level">@lang('programs.program_level') <span class="text-danger">*</span></label>
              <div class="col-lg-5">
                <select id="program_level" class="form-control" name="program_level">
                 {{ showStar() }}
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3" for="image_url">@lang('programs.image') <span class="text-danger">*</span></label>
              <div class="col-lg-9">
                <input type="file" name="image_url" id="imgInp">
                <img id="image-upload" src="{{ (!empty($program) ? $program->image_url : '') }}" class="bb-image-upload"/>
                {{ validateForm($errors, 'image_url') }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3" for="supporter_id">@lang('programs.supporter') <span class="text-danger">*</span></label>
              <div class="col-lg-5">
                <select class="form-control" name="supporter_id">
                  @foreach ($supporters as $supporter)
                  <option value="{{ $supporter->id }}" {{ ($program->supporter_id == $supporter->id) ? 'selected' : '' }}>{{ $supporter->nickname }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-3 col-lg-offset-3">
                <button type="submit" class="btn btn-primary pull-left">@lang('button.submit')</button>
              </div>
              <div class="col-lg-6">
                <a href="{{ route('admin.program.index') }}" type="submit" class="btn btn-default pull-right">@lang('button.cancel')</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    showStarOfProgramLevel({{ $program->done_report_type }}, '#done_report_type');
    showStarOfProgramLevel({{ $program->program_level }}, '#program_level');

    $("#imgInp").change(function() {
      readURL(this);
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#image-upload').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    function showStarOfProgramLevel(level, id_input)
    {
      $(id_input + ' option').each(function () {
        if ($(this).val() == level) {
          $(this).attr('selected','selected');
        }
      });
    }
  });
</script>
@endsection
