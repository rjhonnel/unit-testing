@extends('admin.layouts.master')

@section('content')
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h3>@lang('programs.show_program')</h3>
          <hr style="margin-top:5px;">
        </div>
       </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('programs.title')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $program->title }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('programs.description')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $program->description }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('programs.done_report_type')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $program->done_report_type . ': '}}
              @if ($program->done_report_type === 1)
                @lang('programs.normal')
              @elseif ($program->done_report_type === 2)
                @lang('programs.weight')
              @else
                @lang('programs.image')
              @endif
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('programs.capacity')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $program->capacity }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('programs.program_day')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $program->program_days }}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('programs.program_level')</label>
          </div>
          <div class="col-lg-8">
            <p id="dificultLevelStart"></p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('programs.image')</label>
          </div>
          <div class="col-lg-8">
            <p><img src="{{ (!empty($program)) ? $program->image_url : '' }}" class='bb-image'></p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-4">
            <label for="">@lang('programs.supporter')</label>
          </div>
          <div class="col-lg-8">
            <p>{{ $program->supporter_id . ': ' . $program->supporter->user->profile->nickname}}</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12" style="margin-top :10px;margin-left: 15px;">
          <a type="button" class="btn btn-primary" href="{{ route('admin.club.show') }}?program_id={{ $program->id }}">@lang('button.show_club')</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    $('#dificultLevelStart').html(dificultLevelStart({{ $program->program_level }}))
    function dificultLevelStart(program_level) {
      var levelStarColor = '';
      var levelStarNoColor = '';
      for (i = 1; i<=5; i++) {
        if (i <= program_level) {
          levelStarColor = levelStarColor + '<span class="fa fa-star level-check-pr"></span>';
        } else {
          levelStarNoColor = levelStarNoColor + '<span class="fa fa-star"></span>';
        }
      }
      return levelStarColor + levelStarNoColor;
    }
  });
</script>
@endsection
