@extends('admin.layouts.master')

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<style>
  th{ font-size: 12px;}
  #program-table_processing{position: fixed;top:40%;left:60%;}

</style>
@endsection

@section('content')
  <div id="page-wrapper">
    @if (session('success'))
       <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('success') }}
      </div>
    @endif
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <h3 style="text-align:center;">@lang('programs.program_list')</h3>
          <hr style="margin:-5px 0;">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12" style="margin:20px 0;">
          <a href="{{ route('admin.program.create') }}" type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>
            @lang('button.create_new')
          </a>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <table id="program-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>@lang('programs.id')</th>
                <th>@lang('programs.title')</th>
                <th>@lang('programs.description')</th>
                <th>@lang('programs.done_report_type')</th>
                <th>@lang('programs.capacity')</th>
                <th>@lang('programs.program_day')</th>
                <th>@lang('programs.program_level')</th>
                <th>@lang('programs.image')</th>
                <th>@lang('programs.supporter')</th>
                <th></th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<!-- DataTables JavaScript -->
 <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function(){
    $('#program-table').DataTable({
      "processing": true,
      "oLanguage": {
         "sProcessing": "<img src='{{ url('/images/loader/Preloader_2.gif') }}'/>"
       },
      "serverSide": true,
      "pageLength": 15,
      "language": {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Japanese.json",
      },
      "ajax": "{{ route('datatable.programs') }}",
      "columns": [
        {data: 'id', name: 'id'},
        {data: 'title', name: 'title'},
        {
          data: null,
          render: function (data) {
            return data.description.substring(0, 99) + '...';
          },
          name: 'description'},
        {
          data: null,
          render: function (data) {
            return optionDoneReportType(data.done_report_type);
          },
          name: 'done_report_type'},
        {data: 'capacity', name: 'capacity'},
        {data: 'program_days', name: 'program_days'},
        {
          data: null,
          render: function (data) {
            return dificultLevelStart(data.program_level)
          },
          name: 'program_level'
        },
        {
          data: null,
          render: function (data) {
            return '<img src="'+ data.image_url +'" class="bb-image">';
          },
          name: 'image_url',
          orderable: false},
        {data: 'supporter_id', name: 'supporter_id'},
        {data: 'action', name: 'action', width : "15%",orderable: false, searchable: false}
      ]
    });

    function dificultLevelStart(program_level) {
      var levelStarColor = '';
      var levelStarNoColor = '';
      for (i = 1; i<=5; i++) {
        if (i <= program_level) {
          levelStarColor = levelStarColor + '<span class="fa fa-star level-check-pr"></span>';
        } else {
          levelStarNoColor = levelStarNoColor + '<span class="fa fa-star"></span>';
        }
      }
      return levelStarColor + levelStarNoColor;
    }

    function optionDoneReportType(done_report_type) {
      if (done_report_type == 1) {
        return '@lang('programs.normal')';
      } else if (done_report_type == 2) {
        return '@lang('programs.weight')';
      }
        return '@lang('programs.image')';
    }
  });
</script>
@endsection
