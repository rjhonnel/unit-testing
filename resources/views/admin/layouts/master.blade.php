<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@lang('common.title')</title>

  @yield('style')
  <link href="@css(/css/app.css)" rel="stylesheet">
  <!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- MetisMenu CSS -->
  <link href="{{ asset('css/metisMenu.min.css') }}" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
  <!-- Custom Fonts -->
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<style>
  .level-check-pr{color: #ef7878;}
  nav{margin-top: -70px;}
</style>
@yield('style')
<body>
  <div id="wrapper">
    @if (Auth::guard('admin')->check() && !(isset($no_link) && $no_link))
      <!-- Navigation -->
      @include('admin.layouts.header')
    @endif
    <!-- Page Content -->
    @yield('content')
  </div>
  <!-- /#wrapper -->

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <!-- Metis Menu Plugin JavaScript -->
  <script src="{{ asset('js/metisMenu.min.js') }}"></script>
  <!-- Custom Theme JavaScript -->
  <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

  @yield('script')
</body>
</html>
