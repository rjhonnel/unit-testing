<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="{{ route('admin.dashboard') }}">@lang('common.title')</a>
  </div>
  <!-- /.navbar-header -->

  <ul class="nav navbar-top-links navbar-right">
    <li>{{ Auth::guard('admin')->user()->name }}</li>
    <li>
      <a href="{{ route('admin.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <i class="fa fa-sign-out fa-fw"></i>
        @lang('auth.logout')
      </a>
      <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form>
    </li>
    <!-- /.dropdown -->
  </ul>
  <!-- /.navbar-top-links -->

  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
          <a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> @lang('common.dashboard')</a>
        </li>

        <li>
          <a href="#"><i class="fa fa-th-large"></i> @lang('programs.programs')<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="{{ route('admin.program.index') }}">@lang('programs.list')</a>
            </li>
            <li>
              <a href="{{ route('admin.program.create') }}">@lang('button.create_new')</a>
            </li>
          </ul>
        </li>

        <li>
          <a href="#"><i class="fa fa-user"></i> @lang('user.word.user')<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="{{ route('admin.user.list') }}">@lang('user.word.list')</a>
            </li>
            <li>
              <a href="{{ route('admin.user.create') }}">@lang('user.word.create')</a>
            </li>
          </ul>
          <!-- /.nav-second-level -->
        </li>
        <li>
          <a href="#"><i class="fa fa-wheelchair-alt"></i> @lang('supporter.word.supporter')<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="{{ route('admin.supporter.list') }}">@lang('supporter.word.list')</a>
            </li>
            <li>
              <a href="{{ route('admin.supporter.create') }}">@lang('supporter.word.create')</a>
            </li>
          </ul>
          <!-- /.nav-second-level -->
        </li>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>
@section('script')
  <script>
    var link = $('a');
    $.each(link, function(index, val) {
      if ((window.location.origin + window.location.pathname) == $(val).attr('href')) {
        $(val).parents('li').addClass('active');
      }
    });
  </script>
@endsection
