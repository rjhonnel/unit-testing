@extends('admin.layouts.master')

@section('content')
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="col-xs-12">

        <div class="row">
          <h3 class="page-header">@lang('user.word.create')</h3>
          <div class="col-xs-12">
            <form action="{{ route('admin.user.store') }}" method="post" accept-charset="utf-8">
              {{ csrf_field() }}
              <div class="input-group-lg col-xs-12 col-md-12 {{ $errors->has('email') ? 'has-error' : '' }}">
                <div class="col-xs-2 col-md-1">
                  <b>@lang('user.word.email')<span class="text-danger">*</span></b>
                </div>
                <div class="col-xs-10 col-md-6">
                  <input type="email" class="form-control col-xs-10" name="email" placeholder="example@gmail.com" value="{{ old('email') }}">
                </div>
                @if ($errors->has('email'))
                  <div class="col-xs-10 col-xs-offset-2 col-md-6 col-md-offset-1">
                    <span class="help-block text-danger">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  </div>
                @endif
              </div>
              <br><br><br>

              <div class="input-group-lg col-xs-12 col-md-12 {{ $errors->has('password') ? 'has-error' : '' }}">
                <div class="col-xs-2 col-md-1">
                  <b>@lang('user.word.password')<span class="text-danger">*</span></b>
                </div>
                <div class="col-xs-10 col-md-6">
                  <input type="text" class="form-control" name="password" value="{{ old('password') }}">
                </div>
                @if ($errors->has('password'))
                  <div class="col-xs-10 col-xs-offset-2 col-md-6 col-md-offset-1">
                    <span class="help-block text-danger">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  </div>
                @endif
              </div>
              <br><br><br>

              <div class="col-xs-12 col-md-7 text-center">
                <button type="reset" class="btn btn-default">@lang('button.cancel')</button>
                <button type="submit" class="btn btn-primary">@lang('button.submit')</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection