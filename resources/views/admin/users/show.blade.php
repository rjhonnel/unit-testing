@extends('admin.layouts.master')

@section('content')
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="col-xs-12">

        <div class="row">
          <h3 class="page-header">@lang('user.word.create')</h3>
          <div class="col-xs-12">

            <table class="table text-left">
              <tbody>
                <tr>
                  <td style="width: 200px"><b>@lang('user.word.id')</b></td>
                  <td colspan="4">{{ $user->id }}</td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.nickname')</b></td>
                  <td colspan="4">{{ $user->profile->nickname or '--' }}</td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.email')</b></td>
                  <td colspan="4">{{ $user->email }}</td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.image')</b></td>
                  <td colspan="4">
                    @if (!empty($user->profile))
                      <img src="{{ $user->profile->getPhotoImage() }}" alt="" class="avatar">
                    @endif
                  </td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.height')</b></td>
                  <td colspan="4">{{ $user->profile->height or '--' }}cm</td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.weight')</b></td>
                  <td colspan="4">{{ $user->profile->weight or '--' }}Kg</td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.plan')</b></td>
                  <td colspan="4">
                    @if (!empty($user->profile->plan))
                      {{ config('profile.plan')[$user->profile->plan] }}
                    @else
                      --
                    @endif
                  </td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.goal_of_weight')</b></td>
                  <td colspan="4">{{ $user->goal->weight or '--' }} kg</td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.deadline')</b></td>
                  <td colspan="4">{{ $user->goal->deadline or '--' }}</td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.created_at')</b></td>
                  <td colspan="4">{{ $user->created_at }}</td>
                </tr>
                <tr>
                  <td><b>@lang('user.word.join_program')</b></td>
                  <td colspan="4"></td>
                </tr>
                <tr>
                  <td>@lang('user.word.program_id')</td>
                  <td>@lang('user.word.title')</td>
                  <td style="width: 400px">@lang('user.word.description')</td>
                  <td>@lang('user.word.start')</td>
                  <td>@lang('user.word.end')</td>
                </tr>
                @foreach ($clubs as $item)
                  <tr>
                    <td>{{ $item->program->id }}</td>
                    <td>{{ $item->program->title }}</td>
                    <td>{{ $item->program->description }}</td>
                    <td>{{ $item->pivot->start_date }}</td>
                    <td>{{ $item->pivot->end_date }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          <a href="javascript: history.go(-1)" class="btn btn-info"><i class="fa fa-angle-double-left"></i> @lang('button.back')</a><br><br><br><br>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection