@extends('admin.layouts.master')

@section('content')
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="col-xs-12">

        <div class="row">
          <h3 class="page-header col-xs-12">
            @if (!empty($action) && $action == 'club_join_user')
              <div class="col-xs-12">
                @lang('user.word.list') {{ !empty($action) ? trans('user.word.club_join_user') : '' }}
              </div>
            @else
              <div class="col-xs-2">
                @lang('user.word.list')
              </div>
              <div class="col-xs-8">
                <div class="row">
                  <form action="{{ route('admin.user.list') }}" method="get" accept-charset="utf-8">
                    <div class="col-xs-8">
                      <input type="text" name="search" class="form-control" placeholder="Search for nickname, user ID">
                    </div>
                    <div class="col-xs-4 pull-left">
                      <button type="submit" class="btn btn-primary">@lang('button.search')</button>
                    </div>
                  </form>
                </div>
              </div>
              <div class="col-xs-2">
                <a href="{{ route('admin.user.create') }}" class="btn btn-primary pull-right" title=""><i class="fa fa-plus" aria-hidden="true"></i> @lang('user.word.create')</a>
              </div>
            @endif
          </h3>
          <div class="col-xs-12">
            @if (session('success'))
              <div class="alert alert-success">
                <strong>{{ session('success') }}</strong>
              </div>
            @elseif (session('error'))
              <div class="alert alert-danger">
                <strong>{{ session('error') }}</strong>
              </div>
            @endif
          </div>

          <div class="row">
            <div class="col-xs-12">
              <table id="program-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>@lang('user.word.id')</th>
                    <th>@lang('user.word.nickname')</th>
                    <th>@lang('user.word.email')</th>
                    <th>@lang('user.word.gender')</th>
                    <th>@lang('user.word.age')</th>
                    <th>@lang('user.word.height')</th>
                    <th>@lang('user.word.weight')</th>
                    <th>@lang('user.word.plan')</th>
                    <th>@lang('user.word.goal_weight')</th>
                    <th>@lang('user.word.deadline')</th>
                    <th>@lang('user.word.image')</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $item)
                    <tr>
                      <td>{{ $item->id }}</td>
                      <td>{{ $item->profile->nickname or '--' }}</td>
                      <td>{{ $item->email }}</td>
                      <td>
                        @if (!empty($item->profile))
                          @if ($item->profile->gender == 1)
                            <span class="label label-warning">@lang('user.word.male')</span>
                          @else
                            <span class="label label-primary">@lang('user.word.female')</span>
                          @endif
                        @else
                          --
                        @endif
                      </td>
                      <td>
                        @if (!empty($item->profile))
                           {{ getAgeFromBirthday($item->profile->birthday) }}
                        @else
                          --
                        @endif
                      </td>
                      <td>{{ $item->profile->height or '--' }} cm</td>
                      <td>{{ $item->profile->weight or '--' }} kg</td>
                      <td>
                        @if (!empty($item->profile->plan))
                          {{ config('profile.plan')[$item->profile->plan] }}
                        @else
                          --
                        @endif
                      <td>{{ $item->goal->weight or '--' }} kg</td>
                      <td>{{ $item->goal->deadline or '--' }}</td>
                      <td>
                        @if (!empty($item->profile))
                          <img src="{{ $item->profile->getPhotoImage() }}" alt="" class="bb-image">
                        @endif
                      </td>
                      <td><a href="{{ route('admin.user.show', $item->id) }}" class="btn btn-info btn-xs" title="action">@lang('user.word.detail') <i class="fa fa-angle-double-right"></i></a></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {{ $users->links() }}
      </div>
    </div>
  </div>
@endsection
@section('style')
  <style type="text/css" media="screen">
    .avatar { max-width: 100px }
  </style>
@endsection
