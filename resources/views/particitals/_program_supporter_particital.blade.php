@foreach ($supporters as $supporter)
  <div class="row">
    <div class="col-xs-3">
      <img class="pull-left img-responsive img-circle" src="{{ asset($supporter->photo_image_url) }}" width="60" height="60" />
      <div class="text-center">{{ h($supporter->nickname) }}</div>
    </div>
    <div class="col-xs-7">
      <p><i class="fa fa-heart text-pink" aria-hidden="true">&nbsp;{{ $supporter->cnt_liked }}</i></p>
      <p>{{ trimStr($supporter->self_introduction, 50) }}</p>
    </div>
    <div class="col-xs-1 vcenter">
      <a href="{{ route('supporters.detail', $supporter->id) }}"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
    </div>
  </div>

  @if (!$loop->last)
    <hr />
  @endif
@endforeach
