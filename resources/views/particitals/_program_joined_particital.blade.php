@foreach ($joinedPrograms as $program)
  <div class="row block">
      <div class="col-xs-3">
        <img src="{{ $program->image_url }}" class="img-circle" width="60" height="60">
      </div>
      <div id="parent_div" class="col-xs-7">
        <p >{{ $program->title }}</p>
        @if (!is_null($program->getDoneReportByCurrentDate(Auth::user()->id, $program->id)))
          <div class="daily_report">
            @if ($program->done_report_type === 1)
              <i class="fa fa-lock {{ ($program->isDoneReportUserToday(Auth::user()->id, $program->id)) ? 'reported' : '' }}" aria-hidden="true" data-id="{{ $program->done_report_type }}" data-program-id="{{ $program->id }}"></i>
               &nbsp;&nbsp;
            @elseif ($program->done_report_type === 2)
              <a class="{{ ($program->isDoneReportUserToday(Auth::user()->id, $program->id)) ? 'reported' : '' }}" href="/report-weight/{{ $program->getDoneReportByCurrentDate(Auth::user()->id, $program->id)->id }}">
                <i class="fa fa-lock" aria-hidden="true"></i>
              </a>
            @endif
            <span class="st-count">
              <span class="count_reported">{{ $program->totalDoneReportUserCurrentDay(Auth::user()->id, $program->id) }}</span>/{{ $program->countUserJoinedClub(Auth::user()->id, $program->id) }}
            </span>
          </div>
        @endif
      </div>
      <div class="col-xs-1">
        <a href="{{ route('communications.list', $program->getClub(Auth::user()->id, $program->id)) }}"><i class="fa fa-chevron-right" aria-hidden="true" style="margin-top: 1em;font-size: 20px"></i></a>
      </div>
  </div>
@endforeach