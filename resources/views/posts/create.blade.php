@extends('layouts.app', ['no_link' => 'yes', 'back_url' => '/communications/' . $club_id])
@section('content')
<div class='container'>
  @if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
  @endif
  <div class="row">
    <div class="col-xs-12">
      <form class="form-horizontal" method="post" action="{{ route('posts.store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
          <div class="col-xs-12">
            <textarea class="form-control" name="text" rows="4" cols="50" placeholder="今何をしていますか？">{{ old('text') }}</textarea>
            @if ($errors->has('text'))
              <span class="help-block">
                <strong>{{ $errors->first('text') }}</strong>
              </span>
            @endif
          </div>
        </div>
        <div class="form-group{{ $errors->has('images.*') ? ' has-error' : '' }}">
          <div class="col-xs-12">
            <label for="images" class="col-xs-4 control-label">画像</label>
            <div class="col-xs-8">
              <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple>
              @if ($errors->has('images.*'))
                <span class="help-block">
                  <strong>{{ $errors->first('images.*') }}</strong>
                </span>
              @endif
            </div>
          </div>
        </div>
        <div class="form-group" id="image_preview"></div>
        <div class="form-group">
          <div class="col-xs-6 col-xs-offset-4">
            <input type="hidden" name="club_id" value="{{ $club_id }}">
            <button type="submit" class="btn btn-primary">投稿</button>
          </div>
        </div>
      </form>
    </div>
  </div>

</div>
@endsection

@section('script')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://www.expertphp.in/js/jquery.form.js"></script>
<script>
function preview_images()
{
 var total_file=document.getElementById("images").files.length;
 for (var i = 0; i < total_file; i++) {
  $('#image_preview').append("<div class='col-xs-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
 }
}
</script>
@endsection
