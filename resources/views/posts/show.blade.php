@extends('layouts.app', ['no_link' => 'yes'])

@section('content')
<div class="container">

  <!-- post -->
  <div class="col-sm-12 box">
    <div class="row">
      <div class="col-xs-2">
        <img src="{{ $post->user->profile->getPhotoImage() }}" alt="" class="img-responsive">
      </div>
      <div class="col-xs-8 text-left">
        <b>{{ $post->user->profile->nickname }}</b>
        <p>{{ $post->created_at }}</p>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <p>{{ $post->text }}</p>
      </div>
    </div>
    <div class="row">
      @foreach($post->images as $image)
        <div class="col-xs-4">
          <img src="{{ url($image->image_url) }}" alt="" class="img-responsive">
        </div>
      @endforeach
    </div>
  </div>

</div>
@endsection

@section('style')
<style type="text/css" media="screen">
  .box {
    background: #FFF;
    padding: 8px;
    border: solid 1px #CCC;
    margin-bottom: 16px;
  }
</style>
@endsection
