<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Mail Content</title>
</head>
<style>
.content-mail {
  width: 50em;
  margin: 0 auto;
}
.center-layout {
  text-align: center;
}
</style>
<body>
  <div class="content-mail">
    <div class="center-layout">
      <h1>Welcome to my Website</h1>
      <p>To: <i>{{ $user->email }}</i></p>
    </div><br>
    <hr><br>
    <p>
      Hi, {{ $user->name }}. Welcome to my Website!
      Please activate your account using the following <a href="{{ env('APP_URL') . "/login_from_email/?token=" . $token }}">this Link</a><br>
    </p>
    <p>Password: {{ $user->password }}</p>
  </div>
</body>
</html>