@extends('layouts.app')

@section('style')
  <style>
    .star-left {margin-left: -30px; font-size: 17px;min-width: 135px;}
    .vcenter {display: inline-block; vertical-align: middle; float: none; margin-top: 2.3em;}
    hr { background-color: #ccc; height: 1px; width:90%; border: 0;}
    .menu-tab-ui{margin-bottom: 15px;}
    .menu-tab-ui a{color:#9DA0A2; padding:2px 5px;text-decoration: none;}
    .tab-menu-active{border-bottom: 2px solid #EEA8D8;}
  </style>
@endsection
@section('content')

<div class="container text-center">
  <div class="row">
    <div class="col-xs-12 menu-tab-ui">
      <div class="row">
          <div class="col-xs-4 pull-left">
            <a data-toggle="pill" href="#programs" class="tab-menu-active">@lang('programs.tab_program')</a>
          </div>
          <div class="col-xs-4">
            <a data-toggle="pill" href="#supporters">@lang('programs.tab_supporter')</a>
          </div>
          <div class="col-xs-4 pull-right">
            <a data-toggle="pill" href="#joined">@lang('programs.tab_joined')</a>
          </div>
      </div>
    </div>
  </div>

  <div class="tab-content">
    <div id="programs" class="tab-pane fade in active">
      <div class="block">
        @if (!empty($programs))
          <!--BEGIN Program-->
          @foreach ($programs as $program)
          <div class="row">
            <div class="col-xs-2">
              <img src="{{ $program->image_url }}" class="img-circle" width="60" height="60">
            </div>
            <div class="col-xs-8">
              <div class="text-left">
                <div class="col-xs-12">{{ $program->title }}</div>
                <div class="col-xs-12">
                  @component('programs._difficult', ['program_level' => $program->program_level]) @endcomponent
                </div>
              </div>
            </div>
            <div class="col-xs-1 vcenter">
              <a href="{{ route('program.detail', $program->id) }}"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </div>
          </div>
          <hr>
          @endforeach
          <!--END Program-->
          {{ $programs->links() }}
        @endif
      </div>
    </div>
    <div id="supporters" class="tab-pane fade">
      <div class="row block">
        @include('particitals._program_supporter_particital')
      </div>
    </div>
    <div id="joined" class="tab-pane fade">
      @include('particitals._program_joined_particital')
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    $('.menu-tab-ui a').on('click', function () {
      $('.menu-tab-ui a').removeClass('tab-menu-active');
      $(this).addClass('tab-menu-active');
    });
  });
</script>
@endsection

