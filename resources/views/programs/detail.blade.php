@extends('layouts.app', ['back_url' => $backUrl, 'no_link' => ($isShow === true) ? '' : 'yes'])

@section('content')

<div class="container">
  <div class="block">
    <h4>{{ $program->title }}</h4>
    <p>@component('programs._difficult', ['program_level' => $program->program_level]) @endcomponent</p>

    <div class="row">
      <div class="col-xs-3">
        <img src="{{ $program->image_url }}" class="image-circle" width="60" height="60" />
      </div>
      <div class="col-xs-9">
        <p>{{ $program->description }}</p>
      </div>
    </div>

    @if ($isShow)
      <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
          @if ($isJoinedProgram) 
            参加しています
          @else
            <button type="button" id="btn-join" class="btn btn-primary btn-block btn-margin">@lang('button.join')</button>
          @endif
        </div>
      </div>
    @endif


    @if (!empty($profileOfSupporter))

      <hr />
      <h4>このプログラムのサポーター</h4>

      <div class="row">
        <div class="col-xs-3 text-center">
          <img src="{{ $profileOfSupporter->photo_image_url }}" class="img-circle" alt="Supporters Avatar" width="60" height="60">
          <p>{{ $profileOfSupporter->nickname }}</p>
        </div>
        <div class="col-xs-8">
          <p class="text-left">{{ $profileOfSupporter->self_introduction }}</p>
        </div>
      </div>

      @if ($isShow === true)
        <div class="row">
          <div class="col-xs-6 col-xs-offset-3">
            <a href="{{ route('supporters.detail', $program->supporter->id) }}" class="btn btn-primary btn-block">@lang('button.properties')</a>
          </div>
        </div>
      @endif
    @endif

  </div>
</div>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      var programId = {{ $program->id }};
      $('#btn-join').on('click', function () {
        $.ajax({
          type: "POST",
          url: "{{ route('ajax.update_user_join_club') }}",
          data: {programId:programId},
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          success: function (data) {
            $("#btn-join").hide();
            alert(data);
          }
        });
      });
    })
  </script>
@endsection
