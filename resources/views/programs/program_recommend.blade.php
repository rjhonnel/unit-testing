@extends('layouts.app', ['no_link' => 'yes'])
@section('content')
<div class='container'>
  @if (session('error'))
    <h1 class="text-danger">{{ session('error') }}</h1>
  @endif
  <h4>あなたにオススメのプログラムです</h4>
  <form class="form-horizontal" method="POST" action="{{ route('recommend_programs.join') }}">
    {{ csrf_field() }}
    <div class="row block">
      @foreach ($programs as $program)
        <div class="row">
          <div class="col-xs-3">
            <img class="icon-image pull-left img-responsive" src="{{ asset($program->program->image_url) }}" />
          </div>
          <div class="col-xs-6">
            <p>{{ $program->program->title }}</p>
            @component('programs._difficult', ['program_level' => $program->program->program_level]) @endcomponent
          </div>
          <div class="col-xs-3">
            <a class="btn btn-primary" href="/programs/{{ $program->program->id }}?r=1">詳しく</a>
          </div>
        </div>
        <input type="hidden" name="program_id[]" value="{{ $program->program->id }}">
        @if (!$loop->last) <hr /> @endif
      @endforeach
    </div>
  <button type="submit" class="btn btn-primary btn-block">このプログラムに参加する</button>
  <div class="comment">
    <p>※プログラムはあとで変更できます</p>
  </div>
  </form>
</div>
@endsection
