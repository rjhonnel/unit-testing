@extends('layouts.app', ['title' => 'Notifications', 'back_url' => '/mypage/' ])
@section('content')
  <div class="container">
    <div class="col-sm-12 block">
      @if (count($notifications) > 0)
        @foreach ($notifications as $item)
          @if ($item->notification_type == 2 || $item->notification_type == 4)
            <div id="div_{{ $item->id }}"  onclick="return detail({{ $item->id }},{{ $item->notification_type }},{{ $item->comment->post->id }})">
          @elseif ($item->notification_type == 1)
            <div id="div_{{ $item->id }}" onclick="return detail({{ $item->id }},{{ $item->notification_type }},{{ $item->post->id }})">
          @else
            <div id="div_{{ $item->id }}" onclick="return detail({{ $item->id }},{{ $item->notification_type }},{{ $item->supporter->id }})">
          @endif

            <div class="notifiation {{ empty($item->read_at) ? 'unread' : 'read' }}">
              <div class="row">
                <div class="col-xs-3">
                  <img src="{{ $item->actionUser->profile->getPhotoImage() }}" alt="" class="img-responsive img-circle" width="60" height="60">
                </div>
                <div class="col-xs-9 text-left">
                  <p><strong>{{ $item->actionUser->profile->nickname }}</strong>{{ config('notification.content')[$item->notification_type] }}</p>
                  @if ($item->notification_type == 2 || $item->notification_type == 4)
                    <p>{{ showContentNotification($item->comment->text) }}</p>
                  @elseif ($item->notification_type == 1)
                    <p>{{ showContentNotification($item->post->text) }}</p>
                  @endif
                  <p>{{ $item->created_at }}</p>
                </div>
              </div>
            </div>
          </div>
          @if (!$loop->last)
            <hr />
          @endif
        @endforeach
      @else
        <p>@lang('message.no_data')</p>
      @endif
    </div>
    {{ $notifications->links() }}
  </div>
@endsection
@section('style')
<style type="text/css" media="screen">
  .unread {
    background: #EDF2FA;
  }
  .read {
  }
  .notification {
    padding: 8px;
    margin-top: 8px;
  }
  p { margin: 0 }
</style>
@endsection

@section('script')
  <script>
    function detail(id, type, routerId) {
        $("#div_"+id).addClass('menu-anime');
        $("#div_"+id+" div").removeClass('unread');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
          url: '/notifications/read/' + id,
          type: 'POST',
          dataType: 'JSON',
        })
        .done(function() {
          if (type != 3) {
            window.location = '/comments/' + routerId;
          } else {
            window.location = '/supporters' + routerId;
          }
        })
        return false;
    }
  </script>
@endsection
