@extends('layouts.app', ['title' => 'Comments', 'back_url' => '/communications/' . $post->club->id ])

@section('content')
<div class="container">

  <!-- post -->
  <div class="col-sm-12 box">
    <div class="row">
      <div class="col-xs-2">
        <img src="{{ $post->user->profile->getPhotoImage() }}" alt="" class="img-responsive img-circle">
      </div>
      <div class="col-xs-8 text-left">
        <b>{{ $post->user->profile->nickname }}</b>
        <p>{{ $post->created_at }}</p>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <p>{{ $post->text }}</p>
      </div>
    </div>
    <div class="row">
      @foreach($post->images as $image)
        <div class="col-xs-4">
          <img src="{{ url($image->image_url) }}" alt="" class="img-responsive">
        </div>
      @endforeach
    </div>
  </div>

  <!-- comments -->
  <div class="col-sm-12 box">
    <div id="comment-box">
      @foreach ($comments as $comment)
  			<div class="row box-{{ $comment->id }}">
  				<div class="col-xs-2">
  					<img src="{{ $comment->user->profile->getPhotoImage() }}" alt="" class="img-responsive img-circle">
  				</div>
  				<div class="col-xs-8 text-left">
  					<b>{{ $comment->user->profile->nickname }}</b>
  					<p>{{ $comment->created_at }}</p>
  				</div>
  				<div class="col-xs-2">
            @if (Auth::user()->id == $comment->user->id)
              <div class="dropdown">
                <a href="javascript:void(0)" class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-h pull-right"></i></a>
                <ul class="dropdown-menu" style="left: -145px!important">
                  <li><a href="#" onclick="return removeComment({{ $comment->id }})"><i class="fa fa-trash"></i>  このコメントを削除する</a></li>
                  <div class="dropdown-divider"></div>
                  <li><a href="#" onclick="return editComment({{ $comment->id }})"><i class="fa fa-edit"></i>  このコメントを編集する</a></li>
                </ul>
              </div>
            @endif
  				</div>

  				<div class="col-xs-12 content" data-id="{{ $comment->id }}">
  					<p data-id="{{ $comment->id }}">{{ $comment->text }}</p>
  				</div>
  				<div class="col-xs-12">
            @if ($comment->userLikes->contains('pivot.user_id', Auth::user()->id))
              <p data-id="{{ $comment->id }}" onclick="deleteLike({{ $comment->id }})" class="fa fa-heart text-pink like-icon"></p>
            @else
              <p data-id="{{ $comment->id }}" onclick="like({{ $comment->id }})" class="fa fa-heart-o like-icon"></p>
            @endif
            <span class="like" data-id="{{ $comment->id }}" onclick="likePage({{ $comment->id }})">{{ $comment->userLikes->count() }} @lang('button.like')</span>&nbsp;&nbsp;&nbsp;&nbsp;
            <small class="toolbox" style="color: #B5B5B5" data-toggle="tooltip" title="{{ $comment->updated_at }}" data-id="{{ $comment->id }}">
              @if (!empty($comment->edited_at))
                編集済み
              @endif
            </small>
  				</div>
  			</div>
        <hr />
      @endforeach
    </div>

    <!-- Input -->
    <div class="row">
      <div class="col-xs-9">
        <input type="text" placeholder="Write a comment" class="form-control" id="comment_text"  />
      </div>
      <div class="col-xs-3">
        <button class="btn btn-primary btn-submit" onclick="comment({{ $post->id }}, 'POST')">@lang('button.send')</button>
      </div>
    </div>
    {{ $comments->links() }}
  </div>

</div>
@endsection

@section('style')
<style type="text/css" media="screen">
  .box {
    background: #FFF;
    padding: 8px;
    border: solid 1px #CCC;
    margin-bottom: 16px;
  }
  .like-icon, .comment-icon, .like {
    cursor: pointer;
  }
</style>
@endsection

@section('script')
<script>
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
  })
  function likePage(id) {
    window.location = '/comment-likes/' + id;
  }
  function comment(id) {
    $('.btn-submit').attr('disabled', 'disabled').text('Sending...');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: '/comments/create/' + id,
      type: 'POST',
      dataType: 'HTML',
      data: {
        'content': $('#comment_text').val()
      },
    })
    .done(function(response) {
      $('.btn-submit').removeAttr('disabled').text('Send');
      $('#comment_text').val('');
      $("#comment-box").append(response);
      $("html,body").animate( { scrollTop: 999999 } );
    });
  }

  function removeComment(commentId) {
    if (confirm('Are you sure delete?') == true) {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        url: '/comments/delete/' + commentId,
        type: 'DELETE',
        dataType: 'JSON',
      })
      .done(function(res) {
        if (res.status == 1) {
          $('.box-' + commentId).html(`
              <div class="col-xs-12 text-left text-pink">
                <strong>このコメントは削除されました</strong>
              </div>
          `);
        }
      })
    }
    return false;
  }

  function like(commentId) {
    $(".like-icon[data-id='"+ commentId +"']").css("pointer-events", "none");
    var countLike = $(".like[data-id='"+ commentId +"']").text();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: '/like/comments/' + commentId,
      type: 'POST',
      dataType: 'JSON',
    })
    .done(function(res) {
      $('.like-icon').css("pointer-events", 'auto');
      if (res.status == 1) {
        $(".like-icon[data-id='"+ commentId +"']").removeClass('fa-heart-o').addClass('text-pink').addClass('fa-heart').attr('onclick', "deleteLike("+ commentId +")");
        $(".like[data-id='"+ commentId +"']").text(parseInt(countLike) + 1 + ' Like');
      }
    })
  }

  function deleteLike(commentId) {
    $(".like-icon[data-id='"+ commentId +"']").css("pointer-events", "none");
    var countLike = $(".like[data-id='"+ commentId +"']").text();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: '/like/comments/' + commentId,
      type: 'DELETE',
      dataType: 'JSON',
    })
    .done(function(res) {
      $('.like-icon').css("pointer-events", 'auto');
      if (res.status == 1) {
        $(".like-icon[data-id='"+ commentId +"']").removeClass('fa-heart').removeClass('text-pink').addClass('fa-heart-o').attr('onclick', "like("+ commentId +")");
        $(".like[data-id='"+ commentId +"']").text(parseInt(countLike) - 1 + ' Like');
      }
    })
  }

  function editComment(commentId) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var text = $(".content p[data-id='"+ commentId +"']").text();
    $(".content[data-id='"+ commentId +"']").html(`
        <input class="form-control input-edit" value="`+ text +`" data-id="`+ commentId +`">
        <button type="button" class="btn btn-primary pull-right edit-submit" data-id="`+ commentId +`">Edit</button>
        <button type="button" class="btn btn-default pull-right edit-cancel" data-id="`+ commentId +`">Cancel</button>
      `);
    $(".edit-submit[data-id='"+ commentId +"']").on('click', function() {
      var commentId = $(this).attr('data-id');
      $(this).addClass('disabled');
      $.ajax({
          url: '/comments/edit/' + commentId,
          type: 'PUT',
          dataType: 'JSON',
          data: {
            'content': $(".input-edit[data-id='"+ commentId +"']").val()
          },
        })
        .done(function(response) {
          if (response.status == 1) {
            $(".content[data-id='"+ commentId +"']").html(`
                <p data-id="`+ commentId +`">`+ $(".input-edit[data-id='"+ commentId +"']").val() +`</p>
            `);
            $(".toolbox[data-id='"+commentId+"']").text(' 編集済み');
          }
        });
    });
    $(".edit-cancel[data-id='"+ commentId +"']").on('click', function() {
      var commentId = $(this).attr('data-id');
      $(".content[data-id='"+ commentId +"']").html(`
          <p data-id="`+ commentId +`">`+ $(".input-edit[data-id='"+ commentId +"']").val() +`</p>
      `);
    });
    return false;
  }
</script>
@endsection
