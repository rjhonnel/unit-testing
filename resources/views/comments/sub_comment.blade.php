@if (!empty($comment))
<div class="row box-{{ $comment->id }}">
    <div class="col-xs-2">
        <img src="{{ $comment->user->profile->getPhotoImage() }}" alt="" class="img-responsive img-circle">
    </div>
    <div class="col-xs-8 text-left">
        <b>{{ $comment->user->profile->nickname }}</b>
        <p>{{ $comment->created_at }}</p>
    </div>
    <div class="col-xs-2">
@if (Auth::user()->id == $comment->user->id)
  <div class="dropdown">
    <a href="javascript:void(0)" class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-h pull-right"></i></a>
    <ul class="dropdown-menu" style="left: -145px!important">
      <li><a href="#" onclick="return removeComment({{ $comment->id }})"><i class="fa fa-trash"></i>  このコメントを削除する</a></li>
      <div class="dropdown-divider"></div>
      <li><a href="#" onclick="return editComment({{ $comment->id }})"><i class="fa fa-edit"></i>  このコメントを編集する</a></li>
    </ul>
  </div>
@endif
    </div>

    <div class="col-xs-12 content" data-id="{{ $comment->id }}">
        <p data-id="{{ $comment->id }}">{{ $comment->text }}</p>
    </div>
    <div class="col-xs-12">
@if ($comment->userLikes->contains('pivot.user_id', Auth::user()->id))
  <p data-id="{{ $comment->id }}" onclick="deleteLike({{ $comment->id }})" class="fa fa-heart text-pink like-icon"></p>
@else
  <p data-id="{{ $comment->id }}" onclick="like({{ $comment->id }})" class="fa fa-heart-o like-icon"></p>
@endif
<span class="like" data-id="{{ $comment->id }}" onclick="likePage({{ $comment->id }})">{{ $comment->userLikes->count() }} @lang('button.like')
    </div>
</div>
<hr />
@endif
