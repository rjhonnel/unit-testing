<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@lang('common.title')</title>

  <!-- Styles -->
  <link href="@css(/css/app.css)" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  @yield('style')
</head>
<style>
  #se-pre-con{
    display: none;
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    opacity: 0.7;
    background: url('{{ url('images/loader/Preloader_2.gif') }}') center no-repeat #fff;
  }
  #title_nav_top{
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: inline-block;
    max-width: 100%;
  }
</style>
<body>
  <div id="app">
    @if (!isset($no_header) || $no_header !== "yes")
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="row">
        <div class="col-xs-12 navbar-header">
          @if (isset($back_url))
          <div class="col-xs-1">
            <div class="row">
              <a class="navbar-brand" href="{{ url($back_url) }}"><i class="fa fa-arrow-circle-left" aria-hidden="true" style="font-size:25px;"></i></a>
            </div>

          </div>
          @endif
          <!-- Branding Image -->
          <div class="col-xs-10">
            <div class="row">
               <span id="title_nav_top" class="navbar-brand center-block pull-left">{{ isset($title) ? $title : __('common.title') }}</span>
            </div>

          </div>
        </div>
      </div>
    </nav>
    @endif

    @yield('content')
    <div id="se-pre-con"></div>
  </div>

  <footer>
    @if ((!isset($no_header) || $no_header !== "yes") && (!isset($no_link) || $no_link !== "yes"))
      <nav class="menu-bg navbar-fixed-bottom">
        <table class="table table-bordered custom-menu-bottom">
          <tr>
            <td id="menu-mypage" @if (isset($menu) && $menu === "mypage") class="menu-active" @endif>
              <i class="fa fa-home fa-2x" aria-hidden="true"></i>
              <div class="menu-label">マイページ</div>
            </td>
            <td id="menu-supporter" @if (isset($menu) && $menu === "supporter") class="menu-active" @endif>
              <i class="fa fa-user-md fa-2x" aria-hidden="true"></i>
              <div class="menu-label">サポーター</div>
            </td>
            <td id="menu-program" @if (isset($menu) && $menu === "program") class="menu-active" @endif>
              <i class="fa fa-book fa-2x" aria-hidden="true"></i>
              <div class="menu-label">プログラム</div>
            </td>
            <td id="menu-log" @if (isset($menu) && $menu === "log") class="menu-active" @endif>
              <i class="fa fa-list-alt fa-2x" aria-hidden="true"></i>
              <div class="menu-label">@lang('common.log')</div>
            </td>
            <td id="menu-account" @if (isset($menu) && $menu === "account") class="menu-active" @endif>
              <i class="fa fa-user fa-2x" aria-hidden="true"></i>
              <div class="menu-label">@lang('common.account')</div>
            </td>
          </tr>
        </table>
      </nav>
    @endif
  </footer>

  <!-- Scripts -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="{{ asset('js/app.js') }}"></script>
  <script>
    $("a").on('click', function (e) {
      $link = $(this).attr('href');
      if ($link !== undefined && $link !== '#programs' && $link !== '#supporters' && $link !== '#joined' && $link !== '#') {
        $('#se-pre-con').show();
      }
    });
    function move($elm, url) {
      location.href = url;
      $('td').removeClass('menu-active');
      $elm.addClass('menu-anime').addClass('menu-active');
    }
    $('#menu-mypage').click(function() {
      move($(this), "{{ route('mypage.index') }}");
      $('#se-pre-con').show();
    });
    $('#menu-supporter').click(function() {
      move($(this), "{{ route('supporters.list') }}");
      $('#se-pre-con').show();
    });
    $('#menu-program').click(function() {
      move($(this), "{{ route('program.list') }}");
      $('#se-pre-con').show();
    });
    $('#menu-log').click(function() {
      move($(this), "{{ route('weight.index') }}");
      $('#se-pre-con').show();
    });
    $('#menu-account').click(function() {
      move($(this), "{{ route('profile.show') }}");
      $('#se-pre-con').show();
    });
  </script>
  @yield('script')
</body>
</html>
