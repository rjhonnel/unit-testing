@extends('layouts.app', ['title' => '体重管理', 'back_url' => '/mypage/'] )

@section('content')
  <div class="container">
    <div class="col-sm-12 block">
      <canvas id="canvas"></canvas>
    </div>
    <div class="col-sm-12 block">
      @foreach ($weights as $item)
        <table class="table">
          <tbody>
            <tr>
              <td style="width: 100px">{{ $item['date'] }}</td>
              @if ($item['is_report'])
                <td style="width: 100px" date="{{ $item['date'] }}">
                  <a href="#" onclick="return updateWeight('{{ $item['date'] }}', '{{ $item['weight'] }}')">
                    {{ $item['weight'] }}kg
                  </a>
                </td>
                <td>[@lang('button.reported')]</td>
              @else
                <td style="width: 100px" date="{{ $item['date'] }}">
                  <a href="#" onclick="return updateWeight('{{ $item['date'] }}')">--</a>
                </td>
                <td>[@lang('message.no_data')]</td>
              @endif
            </tr>
          </tbody>
        </table>
      @endforeach
    </div>
    <div class="col-xs-12 text-center" style="padding: 30px">
      <a href="/weight/?time={{ mktime(10, 10, 10, date('m', $time) - 1, date('d', $time), date('y', $time)) }}">
        <i class="fa fa-angle-double-left"></i> 前月</a>
      |
      <a href="/weight/?time={{ mktime(10, 10, 10, date('m', $time) + 1, date('d', $time), date('y', $time)) }}">
        次月 <i class="fa fa-angle-double-right"></i></a>
    </div>
  </div>
@endsection

@section('style')
<style type="text/css" media="screen">
  .weight-data {
    margin-left: 45px;
  }
</style>

@endsection
@section('script')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
  <script>
    function updateWeight(date, weight = null) {
      $('form').remove();
      $('.fade').removeClass('fade');
      $("td[date='"+ date +"'] a").addClass('fade');
      $("td[date='"+ date +"']").append(`
        <form action="/weight/update" method="post" accept-charset="utf-8">
          {{ csrf_field() }}
          <input type="number" name="weight" value="`+ (typeof(weight) != null ? weight : '') +`" class="form-control" placeholder="kg" step="0.1" required>
          <input type="hidden" name="date" value="`+ date +`">
          <button type="submit" class="btn btn-primary submit-weight">OK</button>
          <button type="reset" class="btn btn-default reset" data="` + date + `">Cancel</button>
        </form>
      `);
      $('.reset').on('click', function() {
        $('form').remove();
        $('.fade').removeClass('fade');
      });
      return false;
    }
    $(document).ready(function() {
      var goalWeightArray = new Array();
      var currentWeightArray = new Array();
      var dateLabel = new Array();
      @foreach ($weights as $key => $item)
        dateLabel.push({{ $key + 1 }});
        goalWeightArray.push({{ !empty(Auth::user()->goal) ? Auth::user()->goal->weight : 'NaN' }});
        @if ($item['is_report'])
          currentWeightArray.push({{ $item['weight'] }});
        @else
          currentWeightArray.push(NaN);
        @endif
      @endforeach
        var canvas = $("#canvas")[0].getContext('2d');
        new Chart(canvas, {
            type: 'line',
            data: {
                labels: dateLabel,
                datasets: [
                    {
                        strokeColor: "rgba(255,255,255,1)",
                        borderColor: "rgba(173,173,173,1)",
                        pointStrokeColor: "#fff",
                        data: currentWeightArray,
                        label: '体重'
                    },
                    {
                        strokeColor: "rgba(255,255,255,1)",
                        borderColor: "red",
                        pointStrokeColor: "#fff",
                        data: goalWeightArray,
                        pointRadius: 0,
                        label: '目標'
                    }
                ]
            },
            options: {
                elements: {
                    line: {
                        tension: 0, // disables bezier curves
                    }
                },
                scales: {
                    yAxes: [{
                        // ticks: {
                        //     beginAtZero:true,
                        // },
                        scaleLabel: {
                            display: true,
                            labelString: '体重(kg)'
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: '日付'
                        }
                    }]
                }
            }
        });
    });
  </script>
@endsection
