@extends('layouts.app', ['back_url' => '/communications/' . $doneReport->post->club_id] )

@section('content')
  <div class="container">
    <div class="row block">
      <div class="col-xs-12">
        <p class="text-center">体重記録</p>

        <form action="/report-weight/{{ $doneReport->id }}" method="POST" role="form">
          {{ csrf_field() }}
          @if ($errors->has('done_report_id'))
            <p class="text-danger">{{ $errors->first('done_report_id') }}</p>
          @endif
          <div class="form-group">
            <input type="text" min="1" max="1000" step="1" class="form-control" name="weight" value="{{ old('weight') }}">
          </div>
          @if ($errors->has('weight'))
            <p class="text-danger">{{ $errors->first('weight') }}</p>
          @endif
          <div class="col-xs-6 col-xs-offset-3">
            <button type="submit" id="send_report" class="btn btn-primary center-block btn-block no-border">登録</button>
          </div>
        </form>
      </div>
    </div>
    <p>※入力した体重は他のユーザーに見えることはありません。</p>
  </div>
@endsection

