@extends('layouts.app', ['no_header' => 'yes'])

@section('content')

<div class="row">
  <div class="col-xs-8 col-xs-offset-2 text-center">
    <h1 class="">@lang('common.title')</h1>
    <div>@lang('top.description')</div>
    <br>
    <div class="col-xs-12">
      <a id="tutorial" href="{{ $link }}" class="btn btn-primary center-block">@lang('top.start')</a>
    </div>
    <br><br>
    @if (Auth::user()->agreement_flag === 0)
      <div class="col-xs-12"><br>
        <label for="terms">
        <input id="terms" type="checkbox" value="1">
        <a href="/terms_of_service">@lang('top.terms')</a> @lang('top.agree')
        </label>
      </div>
    @endif
  </div>
</div>

@endsection
@if (Auth::user()->agreement_flag === 0)
  @section('script')
    <script>
      $("#tutorial").on("click", function (e) {
        e.preventDefault();
        if ($("input[type='checkbox']").prop("checked") == true) {
          $.ajax({
            type: "POST",
            url: "{{ route('ajax.agree_term') }}",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (data) {
               window.location.replace(data);
            }
          });
        } else {
          alert("@lang('top.need_agree')");
          $('#se-pre-con').hide();
        }
      });
    </script>
  @endsection
@endif
