@extends('layouts.app', ['back_url' => route('supporters.list')])

@section('content')
<div class='container'>

  {{-- Supporter's profile --}}
  <div class="row">
    <img class="img-fluid img-responsive" src="{{ asset($supporter->cover_image_url) }}" />
  </div>
  <div class="row block">
    <div class="row">
      <div class="col-xs-3 text-center">
        <img class="img-responsive img-circle" src="{{ asset($supporter->photo_image_url) }}" width="60" height="60" />
        {{ h($supporter->nickname) }}
      </div>
      <div class="col-xs-8">
        <div class="row">
          <div class="col-xs-6">
            <i class="fa fa-heart text-pink" aria-hidden="true">&nbsp;<span id="num_like">{{ $supporter->cnt_liked }}</span></i>
            @if ($isLike)
              <a class="btn btn-danger" id="like_supporter" status="unlike">取り消す</a>
            @else
              <a class="btn btn-primary" id="like_supporter" status="like">お気に入り</a>
            @endif
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        {{ h($supporter->self_introduction) }}
      </div>
    </div>

  </div>

  {{-- Programs --}}
  <div class="container block">
    @foreach ($programs as $program)
      <div class="row">
        <div class="col-xs-3">
          <img class="icon-image pull-left img-responsive" src="{{ asset($program->image_url) }}" width="60" height="60" />
        </div>
        <div class="col-xs-6">
          <p>{{ $program->title }}</p>
          <p>@component('programs._difficult', ['program_level' => $program->program_level]) @endcomponent</p>
        </div>
        <div class="col-xs-3">
          <a class="btn btn-primary" href="{{ route('program.detail', $program->id) }}">詳しく</a>
        </div>
      </div>
      @if (!$loop->last) <hr /> @endif
    @endforeach
  </div>

</div>
@endsection

@section('script')
<script>
$(function(){
  $('#like_supporter').click(function(e) {
    $(this).attr('disabled', true);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    e.preventDefault();
    let url = "{{ route('supporters.like', $supporter->id)}}";
    let type = "post";
    let status = $(this).attr('status');
    if (status === 'unlike') {
      url = "{{ route('supporters.unlike', $supporter->id)}}";
      type = "delete";
    }
    $.ajax({
      type: type,
      url: url,
      data: {},
      dataType: 'json',
      success: function(data) {
        if (data.success) {
          if (status === 'like') {
            $('#like_supporter').removeClass('btn-primary').addClass('btn-danger');
            $('#like_supporter').text('取り消す');
            $('#like_supporter').attr('status', 'unlike');
          } else {
            $('#like_supporter').removeClass('btn-danger').addClass('btn-primary');
            $('#like_supporter').text('お気に入り');
            $('#like_supporter').attr('status', 'like');
          }
          $('#num_like').text(data.num_like);
        } else {
          alert(data.message);
        }
        $('#like_supporter').removeAttr('disabled');
      },
      error: function(data) {
      }
    });
  });
});
</script>
@endsection
