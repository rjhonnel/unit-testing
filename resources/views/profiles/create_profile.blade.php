@extends('layouts.app', ['back_url' => route('profile.show')] )

@section('style')
  <style>
    .mr-t-10 {margin-top: 20px;margin-bottom: 20px;}
    .no-border {border:none;border-radius:0;}
    .action { margin-top: 15px }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="col-xs-12 mr-t-10 block">
      <legend class="text-center no-border">@lang('profile.update_profile')</legend>
      <form action="{{ route('profile.update') }}" method="POST" role="form">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT" />
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon1">@lang('profile.nickname')</span>
          <input type="text" name="nickname" class="form-control" value="{{ old('nickname') }}">
        </div>
        @if ($errors->has('nickname'))
          <p class="text-danger">{{ $errors->first('nickname') }}</p>
        @endif<br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon1">@lang('profile.height')</span>
            <input type="number" name="height" class="form-control" value="{{ old('height') }}">
        </div>
        @if ($errors->has('height'))
          <p class="text-danger">{{ $errors->first('height') }}</p>
        @endif <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon1">@lang('profile.weight')</span>
            <input type="number" step="0.1" name="weight" class="form-control" value="{{ $currentWeight or old('weight') }}">
        </div>
        @if ($errors->has('weight'))
          <p class="text-danger">{{ $errors->first('weight') }}</p>
        @endif <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon1">@lang('profile.birthday')</span>
            <input type="date" name="birthday" class="form-control" value="{{ old('birthday') }}">
        </div>
        @if ($errors->has('birthday'))
          <p class="text-danger">{{ $errors->first('birthday') }}</p>
        @endif <br>
        <div class="form-group">
            <label for="">@lang('profile.gender')</label>
            <input type="radio" name="gender" value="1"> @lang('profile.male') &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="gender" value="2"> @lang('profile.female') &nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        @if ($errors->has('gender'))
          <p class="text-danger">{{ $errors->first('gender') }}</p>
        @endif <br>
        <div class="form-group">
            <label for="">@lang('profile.plan')</label>
            @foreach (config('profile.plan') as $key => $value)
              <input type="radio" name="plan" value="{{ $key }}"> {{ $value }} &nbsp;&nbsp;&nbsp;&nbsp;
            @endforeach
        </div>
        @if ($errors->has('plan'))
          <p class="text-danger">{{ $errors->first('plan') }}</p>
        @endif <br>

        <div class="col-xs-6 col-xs-offset-3 action">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">@lang('button.save')</button>
        </div>
      </form>
    </div>
  </div>
@endsection
