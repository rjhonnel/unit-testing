@extends("layouts.app", ['title' => 'プロフィール'])

@section("content")
<div class="container">
  @if (session('success'))
    <span class="col-xs-12 alert alert-success">
      <strong>{{ session('success') }}</strong>
    </span>
  @elseif (session('error'))
    <span class="col-xs-12 alert alert-danger">
      <strong>{{ session('error') }}</strong>
    </span>
  @endif
  <div class="container">
    <div class="row">
      <div class="col-xs-3">
        <div class="row">
          <img src="{{ !$profile ? '' : $profile->getPhotoImage() }}" alt="" class="img-circle avatar">
          <span class="fa-stack fa-lg pull-right btn-select">
            <i class="fa fa-circle fa-stack-2x" style="color: #F5F5F5"></i>
            <i class="fa fa-camera fa-stack-1x text-info"></i>
          </span>
          <form action="{{ route('avatar.update') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="form">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT" />
            <input type="file" name="avatar" class="fade" id="choose-avatar">
          </form>
        </div>
      </div>
      <div class="col-xs-9 text-center">
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-primary">
            ログアウト
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        @if ($errors->has('avatar'))
          <p class="text-danger">{{ $errors->first('avatar') }}</p>
        @endif
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 clearfix">
        <div class="row">
          <ul class="list-group">
            <li class="list-group-item text-center" id='p-nickname'>
              <span class="pull-left item text-left">@lang('profile.nickname')</span> {{ $profile->nickname or '--' }}
              <i class="fa fa-2x fa-angle-right pull-right icon"></i>
            </li>

            <li class="list-group-item text-center" id='p-height'>
              <span class="pull-left item text-left">@lang('profile.height') </span> {{ $profile->height or '--' }} cm
              <i class="fa fa-2x fa-angle-right pull-right icon"></i>
            </li>

            <li class="list-group-item text-center" id='p-weight'>
              <span class="pull-left item text-left">@lang('profile.weight')</span> {{ $currentWeight or '--' }} kg
              <i class="fa fa-2x fa-angle-right pull-right icon"></i>
            </li>

            <li class="list-group-item text-center" id='p-goalweight'>
              <span class="pull-left item text-left">@lang('profile.goal_weight') </span> {{ $goal->weight or '--' }} kg
              <i class="fa fa-2x fa-angle-right pull-right icon"></i>
            </li>

            <li class="list-group-item text-center" id='p-deadline'>
              <span class="pull-left item text-left">@lang('profile.deadline') </span> {{ $goal->deadline or '--' }}
              <i class="fa fa-2x fa-angle-right pull-right icon"></i>
            </li>

            <li class="list-group-item text-center" id='p-plan'>
              <span class="pull-left item text-left">@lang('profile.plan')</span>
              {{ !empty($profile->plan) ? config('profile.plan.' . $profile->plan) : '--' }}
              <i class="fa fa-2x fa-angle-right pull-right icon"></i>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('style')
  <style type="text/css" media="screen">
    .avatar { max-height: 150px; width: 100% }
    .icon { margin-top: -3px }
    .item { width: 100px; }
    .btn-select {top: -35px; cursor: pointer; margin-right: 10px}
  </style>
@endsection

@section('script')
  <script>
    $('.btn-select').on('click', function() {
      $('#choose-avatar').click();
    });
    $("#choose-avatar").on('change', function(e) {
      $("#form").submit();
    });

    goal_param = '';
    profile_param = '';
    @if (empty($goal)) goal_param = "&create=goal"; @endif
    @if (empty($profile)) profile_param = "&create=profile"; @endif
    function moveTo($elm, $url) {
      location.href = $url;
      $elm.addClass('menu-anime');
    }
    $('#p-nickname').click(function() {
      moveTo($(this), "/profile/edit?f=nickname" + profile_param);
    });
    $('#p-height').click(function() {
      moveTo($(this), "/profile/edit?f=height" + profile_param);
    });
    $('#p-weight').click(function() {
      moveTo($(this), "{{ route('weight.index') }}");
    });
    $('#p-goalweight').click(function() {
      moveTo($(this), "/profile/edit?f=goalweight" + goal_param);
    });
    $('#p-deadline').click(function() {
      moveTo($(this), "/profile/edit?f=deadline" + goal_param);
    });
    $('#p-plan').click(function() {
      moveTo($(this), "/profile/edit?f=plan" + profile_param);
    });
  </script>
@endsection
