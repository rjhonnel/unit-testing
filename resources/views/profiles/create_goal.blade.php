@extends('layouts.app', ['back_url' => route('profile.show')] )

@section('style')
  <style>
    .mr-t-10 {margin-top: 20px;margin-bottom: 20px;}
    .no-border {border:none;border-radius:0;}
    .action { margin-top: 15px }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="col-xs-12 mr-t-10 block">
      <legend class="text-center no-border">@lang('profile.update_goal')</legend>
      <form action="{{ route('profile.update') }}" method="POST" role="form">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT" />
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon1">@lang('profile.weight')</span>
          <input type="text" name="weight" class="form-control" value="{{ $goal->weight or old('weight') }}">
        </div>
        @if ($errors->has('weight'))
          <p class="text-danger">{{ $errors->first('weight') }}</p>
        @endif<br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon1">@lang('profile.deadline')</span>
            <input type="date" name="deadline" class="form-control" value="{{ $goal->deadline or old('deadline') }}">
        </div>
        @if ($errors->has('deadline'))
          <p class="text-danger">{{ $errors->first('deadline') }}</p>
        @endif
        <div class="col-xs-6 col-xs-offset-3 action">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">@lang('button.save')</button>
        </div>
      </form>
    </div>
  </div>
@endsection
