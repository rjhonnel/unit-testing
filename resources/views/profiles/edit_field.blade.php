@extends('layouts.app', ['back_url' => route('profile.show') ] )

@section('style')
  <style>
    .mr-t-10 {margin-top: 20px;margin-bottom: 20px;}
    .no-border {border:none;border-radius:0;}
    .action { margin-top: 15px }
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="col-xs-12 mr-t-10 block">
      <legend class="text-center no-border">@lang('profile.edit') {{ $field }}</legend>
      <form action="{{ route('profile.update') }}" method="POST" role="form">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT" />
        <div class="form-group">
          @if ($field == 'plan')
            <div class="col-xs-7 col-xs-offset-4">
              <div class="row">
              @foreach (config('profile.plan') as $key => $value)
                <input type="radio" name="plan" value="{{ $key }}" {{ !empty($profile->plan) && $profile->plan == $key ? 'checked' : '' }}> {{ $value }} <br>
              @endforeach
              </div>
            </div>
          @elseif ($field == 'deadline')
            <input type="date" name="deadline" class="form-control" value="{{ $goal->deadline or old($field) }}">
          @elseif ($field == 'goalweight')
            <input type="text" name="goalweight" class="form-control" value="{{ $goal->weight or old($field) }}">
          @else
            <input type="text" class="form-control" name="{{ $field }}" value="{{ $profile->$field or old($field) }}" required>
          @endif
        </div>
        @if ($errors->has($field))
          <p class="text-danger">{{ $errors->first($field) }}</p>
        @endif
        <div class="col-xs-6 col-xs-offset-3 action">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">@lang('button.save')</button>
        </div>
      </form>
    </div>
  </div>
@endsection
