@extends('layouts.app', !empty($clubId) ? ['title' => 'Vote page', 'back_url' => '/communications/' . $clubId] : ['title' => 'Vote page', 'back_url' => '/supporters/' . $supporterId])

@section('style')
  <style>
  </style>
@endsection

@section('content')
  <div class="container">
    <div class="block">
      @if (count($likedUsers) > 0)
        @foreach ($likedUsers as $user)
        <div class="row">
          <div class="col-xs-2">
              <img src="{{ !empty($user->photo_image_url) ? url($user->photo_image_url) : '/images/no-photo.png' }}" class="img-responsive img-circle" width="60" height="60">
          </div>
          <div class="col-xs-8">
            <div>{{ $user->nickname }}</div>
            <div><i>{{ $user->created_at }}</i></div>
          </div>
        </div>
        @if (!$loop->last)
          <hr />
        @endif
        @endforeach
      @else
      <p>まだ、いいねがありません。</p>
      @endif
    </div>
    <div class="col-xs-12 text-center">
      {{ $likedUsers->links() }}
    </div>
  </div>
@endsection
