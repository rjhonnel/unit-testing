# Requirement.

- git
- docker & docker-comporse

# How to make development env.

## Clone Repositories.

```
$ cd {your path}
$ git clone git@github.com:blue-bee-one/bikatsu-bu-infra.git
$ git clone git@github.com:blue-bee-one/bikatsu-bu-web.git
```

## Setup Infra.

```
$ cd {your path}
$ cp bikatsu-bu-infra/docker-compose.example ./docker-compose.yml
$ docker-compose up -d
```

## Setup Application.

```
$ docker-compose exec app /bin/bash

/var/www $ composer install
/var/www $ cp .env.example .env
/var/www $ php artisan key:generate
/var/www $ php artisan migrate --seed
```

Open browser ->  `http://localhost:80`

# Reference
- Directory Stractures.

```
├── bikatsu-bu-infra
├── bikatsu-bu-web
├── data
│   ├── mysql
│   └── redis
└── docker-compose.yml

```

* docker registry server
	* http://registry.bikatsubu.jp/
* dev
	* http://dev.bikatsubu.jp/
* stg
	* http://stg.bikatsubu.jp/