FROM registry.bikatsubu.jp:5000/bikatsubu-app-base:latest

COPY ./ /var/www
RUN chown -R laravel: ../www
RUN cp .env.dev .env
RUN sudo -u laravel composer install
