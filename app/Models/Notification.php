<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Log;

class Notification extends Model
{
    const PER_PAGE = 10;
    const TYPE_LIKE_POST = 1;
    const TYPE_LIKE_COMMENT = 2;
    const TYPE_LIKE_SUPPORTER = 3;
    const TYPE_ADD_COMMENT = 4;
    const FAIL_STATUS = 0;
    const SUCCESS_STATUS = 1;

    protected $table = 'notifications';
    protected $fillable = ['user_id', 'action_user_id', 'notification_type', 'read_at', 'router_id'];

    public static function getListNotificationByUserId($userId)
    {
        return self::where('user_id', $userId)
                    ->orderBy('id', 'DESC')
                    ->paginate(self::PER_PAGE);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function actionUser()
    {
        return $this->belongsTo('App\User', 'action_user_id');
    }

    public static function createData($userId, $actionUserId, $uriId, $type)
    {
        if ($userId != $actionUserId) {
            try {
                self::create([
                    'user_id' => $userId,
                    'action_user_id' => $actionUserId,
                    'router_id' => $uriId,
                    'notification_type' => $type,
                ]);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                throw new \Exception(trans('message.cant_add_data'));
            }
            return true;
        }
        return false;
    }

    public static function removeData($userId, $actionUserId, $uriId, $type)
    {
        if ($userId != $actionUserId) {
            try {
                $notify = self::where([
                    'user_id' => $userId,
                    'action_user_id' => $actionUserId,
                    'router_id' => $uriId,
                    'notification_type' => $type
                ])->first();
                if (!empty($notify)) {
                    $notify->delete();
                } else {
                    Log::warning('Not Found: ' . json_encode([
                        'user_id' => $userId,
                        'action_user_id' => $actionUserId,
                        'router_id' => $uriId,
                        'notification_type' => $type
                    ]));
                    throw new \Exception(trans('message.cant_found_data'));
                }
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                throw new \Exception(trans('message.cant_delete_data'));
            }
            return true;
        }
        return false;
    }

    public static function updateReadAt($id)
    {
        try {
            $notification = self::findOrFail($id);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return self::FAIL_STATUS;
        }
        $notification->read_at = \Carbon\Carbon::now();
        $notification->save();
        return self::SUCCESS_STATUS;
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post', 'router_id', 'id');
    }

    public function comment()
    {
        return $this->belongsTo('App\Models\Comment', 'router_id', 'id');
    }

    public function supporter()
    {
        return $this->belongsTo('App\Models\Supporter', 'router_id');
    }

    public static function countNewsNotifications($userId)
    {
        return self::where('user_id', $userId)
                   ->whereNull('read_at')
                   ->count();
    }
}
