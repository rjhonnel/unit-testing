<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LikeSupporter extends Model
{
    const PER_PAGE = 10;

    protected $table = 'like_supporters';

    protected $fillable = ['supporter_id', 'user_id'];

    public function scopeSupporterUserId($query, $supporterId, $userId)
    {
        return $query->where('supporter_id', '=', $supporterId)
                ->where('user_id', '=', $userId);
    }

    public function scopeSupporterId($query, $supporterId)
    {
        return $query->where('supporter_id', '=', $supporterId);
    }

    public static function getUserLikedSupporter ($supporterId) {
        return DB::table('like_supporters')
                 ->select('profiles.nickname', 'profiles.photo_image_url', 'like_supporters.created_at')
                 ->join('profiles', 'profiles.user_id', '=', 'like_supporters.user_id')
                 ->where('like_supporters.supporter_id', $supporterId)
                 ->orderBy('like_supporters.id', 'DESC')
                 ->paginate(self::PER_PAGE);
    }
}
