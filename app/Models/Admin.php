<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Admin extends Model
{
    protected $table = 'admins';
    protected $fillable = ['user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
