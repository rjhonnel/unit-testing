<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;
use App\Models\DoneReportUser;

class Program extends Model
{
    protected $table = 'programs';

    protected $fillable = [
        'supporter_id',
        'title',
        'description',
        'done_report_type',
        'capacity',
        'program_level',
        'program_days',
        'image_url'
    ];

    public function scopeSupporterId($query, $supporterId)
    {
        return $query->where('supporter_id', '=', $supporterId);
    }

    public function supporter()
    {
        return $this->belongsTo('App\Models\Supporter');
    }

    public function clubs()
    {
        return $this->hasMany('App\Models\Club');
    }

    public static function getProgramsUserJoined($userId)
    {
        $clubs = User::find($userId)->clubs;
        $programs = [];
        foreach ($clubs as $club) {
            if (!$club->original['pivot_program_end_checked_at']) {
                $programs[] = $club->program;
            }
        }
        return $programs;
    }

    public static function getDoneReportByCurrentDate($userId, $programId)
    {
        $club = Program::getClub($userId, $programId);
        return $club->doneReports()
                    ->where('done_reports.date', Carbon::today())
                    ->first();
    }

    public static function totalDoneReportUserCurrentDay($userId, $programId)
    {
        $doneReportToDay = Program::getDoneReportToday($userId, $programId);
        if (!empty($doneReportToDay)) {
            return  $doneReportToDay->doneReportUsers->count();
        }
        return 0;
    }

    public static function getClub($userId, $programId)
    {
        $user = User::find($userId);
        return $user->clubs()->where('clubs.program_id', $programId)->first();
    }

    public static function isDoneReportUserToday($userId, $programId)
    {
        $doneReportId = Program::getDoneReportByCurrentDate($userId, $programId);
        $doneReportUser = DoneReportUser::where('done_report_id', $doneReportId->id)
                                        ->where('user_id', $userId)
                                        ->count();
        if ($doneReportUser === 0) {
            return false;
        }
        return true;
    }

    public static function countUserJoinedClub($userId, $programId)
    {
        $numberUserOfClub = Program::getClub($userId, $programId)->joinUsers();
        if (!empty($numberUserOfClub)) {
            return $numberUserOfClub->count();
        }
        return 0;
    }

    public static function getDoneReportToday($userId, $programId)
    {
        $club = Program::getClub($userId, $programId);
        return  $club->doneReports()
                     ->where('done_reports.date', Carbon::today())
                     ->first();
    }
}
