<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $table = 'comments';
    protected $fillable = ['post_id', 'user_id', 'text'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

    public function userLikes()
    {
        return $this->belongsToMany('App\User', 'like_comments', 'comment_id', 'user_id')
                    ->withPivot('user_id', 'comment_id');
    }

    public static function checkUserLikedComment($userId, $commentId)
    {
        if (self::find($commentId)->userLikes->contains('pivot.user_id', $userId)) {
            return false;
        }
        return true;
    }
}

