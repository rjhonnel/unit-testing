<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Club;
use DB;
use Log;

class Post extends Model
{
    const FAIL_STATUS = 0;
    const SUCCESS_STATUS = 1;

    protected $table = 'posts';
    protected $fillable = ['club_id', 'user_id', 'text'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function club()
    {
        return $this->belongsTo('App\Models\Club');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function images()
    {
        return $this->hasMany('App\Models\PostImage');
    }

    public function userLikes()
    {
        return $this->belongsToMany('App\User', 'like_posts', 'post_id', 'user_id')
                    ->withPivot('user_id', 'post_id');
    }

    public function doneReports()
    {
        return $this->hasMany('App\Models\DoneReport');
    }

    public static function getPostByClubId($clubId)
    {
        return self::withCount('comments', 'userLikes')
                    ->leftJoin('done_reports as dr', 'posts.id', '=', 'dr.post_id')
                    ->where('club_id', $clubId)
                    ->orderByRaw('SUBSTR(posts.created_at, 1, 10) DESC')
                    ->orderBy('dr.date', 'DESC')
                    ->orderBy('id', 'DESC');
    }

    public static function deleteRecord($userId, $postId)
    {
        $post = Post::find($postId);
        if ($post->user->id !== $userId) {
            Log::warning('Permission denied for deletion: ' . json_encode([
                'user_id' => $userId,
                'post_id' => $postId
            ]));
            return self::FAIL_STATUS;
        }
        try {
            $post->delete();
        } catch (\Exception $e) {
            return self::FAIL_STATUS;
        }
        return self::SUCCESS_STATUS;
    }

    public static function checkUniqueDoneReportCurrentDate($clubId)
    {
        $post = self::where('club_id', $clubId)->whereHas('doneReports', function($query) {
            $query->orderBy('id', 'DESC')->whereDate('date', date('Y-m-d'));
        })->first();
        if (!empty($post->doneReports) && $post->doneReports->count() > 0) {
            return false;
        }
        return true;
    }

    public static function createPostBySupporter($clubId)
    {
        $club = Club::find($clubId);
        $userIdOfSupporter = $club->program->supporter->user->id;
        $newPost= Post::create([
            'club_id' => $clubId,
            'user_id' => $userIdOfSupporter,
            'text' => config('post.automaticly.text')
        ]);
        return $newPost->id;
    }
}
