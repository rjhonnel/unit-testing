<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Club extends Model
{
    const PER_PAGE = 15;

    protected $table = 'clubs';
    protected $fillable = ['program_id'];

    public function program() {
        return $this->belongsTo('App\Models\Program');
    }
    public function posts() {
        return $this->hasMany('App\Models\Post')->orderBy('id', 'desc');
    }

    public function doneReports()
    {
        return $this->hasManyThrough('App\Models\DoneReport', 'App\Models\Post');
    }

    public static function getLatestClubOfProgram($programId) {
        return Club::where('program_id', $programId)->orderBy('id', 'DESC')->first();
    }

    public function joinUsers()
    {
        return $this->belongsToMany('App\User', 'club_join_users', 'club_id', 'user_id')
                    ->withPivot('club_id', 'user_id', 'start_date', 'end_date')->get();
    }

    public function isJoin($userId)
    {
        $ret = DB::table('club_join_users as cju')
            ->where('user_id', '=', $userId)
            ->where('club_id', '=', $this->id)->get();
        return (count($ret) > 0);
    }

    public static function getClubNotFinish()
    {
        return DB::table('clubs as c')
                    ->join('club_join_users as cju', 'cju.club_id', '=', 'c.id')
                    ->join('programs as p', 'p.id', '=', 'c.program_id')
                    ->join('supporters as sp', 'sp.id', '=', 'p.supporter_id')
                    ->join('users as u', 'u.id', '=', 'sp.user_id')
                    ->select(DB::raw('DISTINCT c.id as club_id, u.id as user_id'))
                    ->where(function($query) {
                        $query->whereDate('cju.end_date', '>', \Carbon\Carbon::now())
                                ->orWhereNull('cju.end_date');
                    })
                    ->get();
    }
    
    public static function getClubNotFinishOfProgram($programId) {
        $sub = DB::table('clubs as c')
                    ->join('club_join_users as cju', 'cju.club_id', '=', 'c.id')
                    ->join('programs as p', 'p.id', '=', 'c.program_id')
                    ->select(DB::raw('p.id as program_id, p.title as title, c.id as club_id'))
                    ->where('c.program_id', '=', $programId)
                    ->where(function($q) {
                        $q->whereDate('cju.end_date', '>', \Carbon\Carbon::now())
                        ->orWhereNull('cju.end_date');
                    })
                    ->orderBy('cju.end_date', 'DESC');
        
        $clubs = DB::table( DB::raw("({$sub->toSql()}) as sub"))
                ->select('program_id', 'title', 'club_id')
                ->mergeBindings($sub)
                ->groupBy('club_id')
                ->get();
        return $clubs;
    }

    public function getEndDateOfClub($clubId)
    {
        $timeStamp = [];
        $club = Club::find($clubId);
        foreach ($club->joinUsers() as $user) {
            $timeStamp[] = strtotime($user->pivot->end_date);
        }
        if (!empty($timeStamp)) {
            return date('d/m/Y', max($timeStamp));
        }
        return '';
    }

    public static function getUserJoinedClub($club)
    {
        return $club->belongsToMany('App\User', 'club_join_users', 'club_id', 'user_id')
                    ->paginate(self::PER_PAGE);
    }
}
