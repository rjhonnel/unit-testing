<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class Goal extends Model
{
    protected $table = 'goals';
    protected $fillable = ['user_id', 'weight', 'deadline'];

    public function scopeUserId($query, $userId)
    {
        return $query->where('user_id', $userId);
    }
}
