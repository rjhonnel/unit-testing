<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Log;

class DoneReportUser extends Model
{
    const FAIL_STATUS = 0;
    const SUCCESS_STATUS = 1;

    protected $table = 'done_report_users';
    protected $fillable = ['user_id', 'done_report_id'];

    public function doneReport()
    {
        return $this->belongsTo('App\Models\DoneReport');
    }

    public static function createData($userId, $doneReportId)
    {
        try {
            self::create([
            'user_id' => $userId,
            'done_report_id' => $doneReportId
        ]);
            return self::SUCCESS_STATUS;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return self::FAIL_STATUS;
        }
    }

    public static function deleteRecord($userId, $doneReportId)
    {
        try {
            self::where([
                'user_id' => $userId,
                'done_report_id' => $doneReportId
            ])->delete();
            return self::SUCCESS_STATUS;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return self::FAIL_STATUS;
        }
    }
}
