<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Club;
use App\User;
use Carbon\Carbon;
use DB;

class ClubJoinUser extends Model
{
    protected $table = 'club_join_users';
    protected $fillable = ['club_id', 'user_id', 'start_date', 'end_date', 'program_end_checked_at'];

    public static function createUserJoinClub($userId, $program)
    {
        $club = Club::getLatestClubOfProgram($program->id);
        $data = [
            'user_id' => $userId,
            'start_date' => Carbon::today(),
            'end_date' => self::endDate($program->program_days)
        ];
        if (!empty($club)) {
            $countNumberUsersJoinedClub = ClubJoinUser::where('club_id', $club->id)->count();
            if ($countNumberUsersJoinedClub < $program->capacity) {
                $data['club_id'] = $club->id;
                ClubJoinUser::create($data);
                return true;
            }
        }
        # Program have not club or club full slot
        $newClubOfProgram = Club::create(['program_id' => $program->id]);
        $data['club_id'] = $newClubOfProgram->id;
        ClubJoinUser::create($data);
        return true;
    }

    public static function isUserJoinedProgram($userId, $programId)
    {
        $program = DB::table('clubs')
                    ->join('club_join_users', 'clubs.id', '=', 'club_join_users.club_id')
                    ->where('program_id', '=', $programId)
                    ->where('user_id', '=', $userId)
                    ->first();
        return empty($program) ? false : true;
    }

    public static function endDate($days)
    {
        return !empty($days) ? Carbon::parse(Carbon::today())->addDays($days) : null;
    }

    public function userWeights()
    {
        return $this->hasMany('App\Models\UserWeight', 'user_id', 'user_id');
    }

    public static function getUserWeightByCurrentDate($clubId)
    {
        return self::withCount('userWeights')
                    ->where('club_id', $clubId)
                    ->whereHas('userWeights', function($query) {
                        $query->whereDate('date', date('Y-m-d'));
                    })->count();
    }

    public static function isUserDoneReport($userId, $postId)
    {
        $doneReport = DB::table('done_report_users as dru')
                        ->join('done_reports as dr', 'dru.done_report_id', '=', 'dr.id')
                        ->where([
                            'dru.user_id' => $userId,
                            'dr.post_id' => $postId
                        ])
                        ->select(DB::raw('count(dru.id) as count_done_report'))
                        ->first();
        if ($doneReport->count_done_report == 0) {
            return false;
        }
        return true;
    }

    public function scopeUserEndDate($query, $userId, $endDate)
    {
        return $query->where('user_id', '=', $userId)
                     ->where('end_date', '<', $endDate);
    }

    public function club()
    {
        return $this->belongsTo('App\Models\Club');
    }
}
