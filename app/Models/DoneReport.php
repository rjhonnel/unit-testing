<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Program;
use App\Models\Post;
use Carbon\Carbon;

class DoneReport extends Model
{
    protected $table = 'done_reports';
    protected $fillable = ['post_id', 'date'];

    public function doneReportUsers()
    {
        return $this->hasMany('App\Models\DoneReportUser');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

    public static function createDoneReportIfNotExist($userId, $programId)
    {
        $doneReportToday = Program::getDoneReportToday($userId, $programId);
        if(empty($doneReportToday)) {
            $newPostId = Post::createPostBySupporter(Program::getClub($userId, $programId)->id);
            $lastId = DoneReport::create([
                'post_id' => $newPostId,
                'date' => Carbon::today()
            ])->id;
        } else {
            $lastId = $doneReportToday->id;
        }

        return $lastId;

    }
}
