<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecommendProgram extends Model
{
    protected $table = 'recommend_programs';
    protected $fillable = ['user_id', 'program_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function program()
    {
        return $this->belongsTo('App\Models\Program');
    }

    public function scopeUserId($query, $userId)
    {
        return $query->where('user_id', '=', $userId);
    }
}

