<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Program;
use DB;

class Supporter extends Model
{
    protected $table = 'supporters';

    protected $fillable = ['user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function programs() {
        return $this->hasMany('App\Models\Program');
    }

    public static function getNameOfSupporter($program)
    {
        $userIdOfSupporter =  $program->supporter->user_id;
        $user = User::find($userIdOfSupporter);
        return $user->profile->nickname;
    }

    public static function getListOfSupporter()
    {
        return DB::table('users')
            ->join('supporters', 'supporters.user_id', '=', 'users.id')
            ->join('profiles', 'profiles.user_id', '=', 'users.id')
            ->select('profiles.nickname', 'supporters.id')
            ->get();
    }
}
