<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = ['user_id', 'nickname', 'gender', 'birthday', 'weight', 'height', 'photo_image_url', 'cover_image_url', 'self_introduction', 'plan'];

    public function getPhotoImage() {
        return $this->photo_image_url ? $this->photo_image_url : "/images/no-photo.png";
    }

    public function getCoverPhoto()
    {
        return $this->cover_image_url ? $this->cover_image_url : "/images/no-photo.png";
    }

    public static function existProfile ($userId) {
        if (!empty(Profile::where('user_id', $userId)->first())) {
            return true;
        }
        return false;
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeUserId($query, $userId)
    {
        return $query->where('user_id', $userId);
    }
}
