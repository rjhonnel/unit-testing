<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Log;

class UserWeight extends Model
{
    protected $table = 'user_weights';
    protected $fillable = ['user_id', 'weight', 'date'];

    public static function createData($userId, $weight, $date = null)
    {
        try {
            self::updateOrCreate([
                    'user_id' => $userId,
                    'date' => !empty($date) ? $date : date('Y-m-d')
                ],[
                    'weight' => $weight
                ]
            );
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    public static function getDataByMonth($userId, $time)
    {
        $data = [];
        $weight = self::where('user_id', $userId)
                    ->whereMonth('created_at', date('m', $time))
                    ->get();
        for ($i = 1; $i <= date('t', $time); $i++) {
            $date = self::convertToDate($time, $i);
            if (!empty($weight->where('date', $date)->first())) {
                $data[$i] = [
                    'is_report' => true,
                    'weight' => $weight->where('date', $date)->first()->weight,
                    'date' => $date
                ];
            } else {
                $data[$i] = [
                    'is_report' => false,
                    'date' => $date
                ];
            }
        }
        return $data;
    }

    public static function convertToDate($time, $i) {
        if ($i <= 9) {
            return date('Y-m', $time) . "-0$i";
        }
        return date('Y-m', $time) . "-$i";
    }

    public static function getLastWeightReport($userId)
    {
        return self::where('user_id', $userId)->orderBy('date', 'DESC')->first();
    }
}
