<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LikeComment extends Model
{
    const PER_PAGE = 10;

    protected $table = 'like_comments';
    protected $fillable = ['comment_id', 'user_id'];

    public static function getUserLikeCommentOfPost ($commentId) {
        return DB::table('like_comments')
                 ->select('profiles.nickname', 'profiles.photo_image_url', 'like_comments.created_at')
                 ->join('profiles', 'profiles.user_id', '=', 'like_comments.user_id')
                 ->where('like_comments.comment_id', $commentId)
                 ->orderBy('like_comments.id', 'DESC')
                 ->paginate(self::PER_PAGE);
    }
}
