<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LikePost extends Model
{
    const PER_PAGE = 10;

    protected $table = 'like_posts';
    protected $fillable = ['post_id', 'user_id'];

    public static function getUserLikedPost ($postId) {
        return DB::table('like_posts')
                 ->select('profiles.nickname', 'profiles.photo_image_url', 'like_posts.created_at')
                 ->join('profiles', 'profiles.user_id', '=', 'like_posts.user_id')
                 ->where('like_posts.post_id', $postId)
                 ->orderBy('like_posts.id', 'DESC')
                 ->paginate(self::PER_PAGE);
    }
}
