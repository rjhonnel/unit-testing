<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    const PER_PAGE_USER = 15;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'agreement_flag'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static function getRecordFromUser() {
        return User::select('id', 'name', 'email')->get();
    }

    protected static function checkLogin($id) {
        return Auth::login(User::find($id)->first(), true);
    }

    protected static function checkDuplicateEmail($email)
    {
        if (User::where('email', $email)->count() > 0) {
            return true;
        }
        return false;
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    public function goal()
    {
        return $this->hasOne('App\Models\Goal');
    }

    public function supporter()
    {
        return $this->hasOne('App\Models\Supporter');
    }

    public function likePosts()
    {
        return $this->belongsToMany('App\Models\Post', 'like_posts', 'user_id', 'post_id');
    }

    public function likeComments()
    {
        return $this->belongsToMany('App\Models\Comment', 'like_comments', 'user_id', 'comment_id');
    }

    public function clubs()
    {
        return $this->belongsToMany('App\Models\Club', 'club_join_users', 'user_id', 'club_id')
                    ->withPivot('start_date', 'end_date', 'program_end_checked_at');
    }

    public function admin()
    {
        return $this->hasOne('App\Models\Admin', 'user_id', 'id');
    }

    public static function isAdmin($email)
    {
        $user = self::where('email', $email)->first();
        if (!empty($user) && !empty($user->admin)) {
            return true;
        }
        return false;
    }

    public static function seachUserByIdOrNickname($data)
    {
        return self::where('id', $data)
                    ->orWhereHas('profile', function($query) use ($data) {
                        $query->where("nickname", "like", "%{$data}%");
                    })->paginate(self::PER_PAGE_USER);
    }

    public static function isOwnClub($userId, $clubId) {
        $res = DB::table('users as u')
                    ->join('supporters as sp', 'sp.user_id', '=', 'u.id')
                    ->join('programs as p', 'p.supporter_id', '=', 'sp.id')
                    ->join('clubs as c', 'c.program_id', '=', 'p.id')
                    ->select(DB::raw('u.id as user_id'))
                    ->where('u.id', '=', $userId)
                    ->where('c.id', '=', $clubId)
                    ->first();
        if(!empty($res) && !empty($res->user_id)) {
            return true;
        }
        return false;
    }
}
