<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Token;
use Carbon\Carbon ;
use App\Mail\SendMailUser;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use DB;

class ImportUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:user {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import user data to database from csv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        #import file csv to database;
        $file = $this->argument('path');
        $delimiter = ',';
        if (!file_exists($file) || !is_readable($file)) {
            return $this->error(trans('message.error_file'));
        }
        $header = NULL;
        $data = array();
        $tmp_pwd_arr = array();
        if (($handle = fopen($file, 'r')) === FALSE) { return $this->error(trans('message.cant_open_file'));}
        $i = -1;
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
            $i++;
            if (!$header) {
                $header = ['name', 'email'];
            } else {
                $data[$i] = array_combine($header, $row);
                if (!($this->validateCsv($data[$i]))) {
                    $errors[$i] = "===>" . sprintf(trans('message.record_invalid'), $i);
                    $this->comment($errors[$i]);
                }
            }
        }
        fclose($handle);

        #If some data of csv invalid
        if (!empty($errors)) {
            $this->comment(trans('message.correct_all_record'));
            exit;
        }
        $this->info(trans('message.records_valid'));
        #If all data of csv valid
        if ($this->confirm(trans('message.confirm'))) {
            for ($i = 1; $i <= count($data); $i++) {
                if (User::checkDuplicateEmail($data[$i]['email'])) {
                    continue;
                }
                DB::beginTransaction();
                try {
                    $data[$i]['password'] = str_random(20);
                    $newUsers = User::firstOrCreate($data[$i]);
                    $data[$i]['id'] = $newUsers->id;
                    $key_tmp_pwd_arr[] = $newUsers->id;
                    Token::create([
                        'user_id' => $newUsers->id,
                        'token' => str_random(30),
                        'expire_date' =>  date("Y-m-d H:i:s", strtotime(Carbon::now()) + 86400)
                    ]);
                    $this->sendMail($newUsers);
                    DB::commit();
                    $this->info(sprintf(trans('message.record_added'), $i));
                    $this->info(trans('message.mail_sended'));
                } catch (\Exception $e) {
                    DB::rollback();
                    throw $e;
                }
            }
        }
    }

    public function validateCsv($userCsv)
    {
        $validator = Validator::make($userCsv, [
            'name' => 'string|max:255',
            'email' => 'required|string|email|max:255|'
        ]);
        if ($validator->fails()) {
            foreach($validator->messages()->getMessages() as $fieldName => $messages) {
                foreach($messages as $message) {
                    $this->error($fieldName . ': ' . $message);
                }
            }
            return false;
        }
        return true;
    }

    private function sendMail($user)
    {
        $token = Token::getTokenByUserId(intval($user->id))->token;
        $email = new SendMailUser($user, $token);
        Mail::to($user->email)->send($email);
    }
}
