<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Club;
use App\Models\Post;
use App\Models\DoneReport;
use DB;

class AutoCreateDoneReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:create {club_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automaticly post done reports every day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fail = 0;
        if (empty($this->argument('club_id'))) {
            $clubs = Club::getClubNotFinish();
            if (!empty($clubs) && count($clubs) > 0) {
                foreach ($clubs as $club) {
                    if (!$this->createData($club->club_id, $club->user_id)) {
                        $fail++;
                        $this->error(sprintf(trans('message.club_have_created_detail'), $club->club_id));
                    }
                }
                $this->info(trans('message.success_cm') . (count($clubs) - $fail));
                $this->error(trans('message.fail_cm')        . $fail);
            } else {
                $this->info(trans('message.club_not_available'));
            }
        } else {
            $club = Club::find($this->argument('club_id'));
            if (!empty($club)) {
                if (!$this->createData($club->id, $club->program->supporter->user->id)) {
                    $fail++;
                    $this->error(trans('message.club_have_created'));
                }
            } else {
                $fail++;
                $this->error(trans('message.club_not_found'));
            }
            $this->info('-----------------------------');
            $this->info(trans('message.success_cm') . (1 - $fail));
            $this->error(trans('message.fail_cm')        . $fail);
            $this->info('-----------------------------');
        }
    }

    private function createData($clubId, $userId)
    {
        if (Post::checkUniqueDoneReportCurrentDate($clubId)) {
            $post = Post::create([
                'club_id' => $clubId,
                'user_id' => $userId,
                'text' => config('post.automaticly.text')
            ]);
            DoneReport::create([
                'post_id' => $post->id,
                'date' => date('Y-m-d')
            ]);
            return true;
        }
        return false;
    }
}
