<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Text or images validation
        $rules = [
            'text' => 'required_without_all:images.0'
        ];
        $images = count($this->input('images')) - 1;
        foreach (range(0, $images) as $index) {
            $rules['images.' . $index] = 'image|mimes:jpeg,jpg,gif,png|max:2048';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'text.required_without_all' => ':valuesが指定されていない場合、:attributeを指定してください。',
            'images.*.mimes' => ':attributeには、:valuesタイプの画像を指定してください',
            'images.*.max' => ':attributeには、:max KBまでのサイズの画像を指定してください。'
        ];
    }

    public function attributes()
    {
        return [
            'text' => 'テキスト',
            'images.*' => '画像',
            'images.0' => '画像'
        ];
    }
}
