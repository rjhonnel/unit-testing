<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Profile;

class CheckProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Profile::existProfile(Auth::user()->id)) {
            return $next($request);
        }
        return redirect()->route('toppage');
    }
}
