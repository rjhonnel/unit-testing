<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Supporter;
use Auth;

class CheckNoneSupporter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::user()->id;
        if (empty(Supporter::where('user_id', $userId)->first())) {
            return $next($request);
        }
        return redirect('/');
    }
}
