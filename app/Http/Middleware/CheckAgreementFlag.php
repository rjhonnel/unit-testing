<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckAgreementFlag
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->agreement_flag === 1) {
            return $next($request);
        }
        return redirect()->route('toppage');
    }
}
