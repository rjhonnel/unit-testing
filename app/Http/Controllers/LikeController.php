<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Supporter;
use App\Models\LikePost;
use App\Models\LikeComment;
use App\Models\LikeSupporter;
use App\Models\Notification;
use Auth;
use DB;
use Log;

class LikeController extends Controller
{
    const FAIL_STATUS = 0;
    const SUCCESS_STATUS = 1;

    public function likePost(Request $request, $postId)
    {
        try {
            $post = Post::findOrFail($postId);

            DB::transaction(function() use ($post) {
                LikePost::create([
                    'user_id' => Auth::user()->id,
                    'post_id' => $post->id
                ]);
                Notification::createData(
                    $post->user_id,
                    Auth::user()->id,
                    $post->id,
                    Notification::TYPE_LIKE_POST
                );
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'status' => self::FAIL_STATUS,
                'message' => trans('message.already_like_post')
            ]);
        }
        return response()->json(['status' => self::SUCCESS_STATUS]);
    }

    public function deleteLikePost($postId)
    {
        try {
            $post = Post::findOrFail($postId);

            DB::transaction(function() use ($post) {
                LikePost::where([
                    'post_id' => $post->id,
                    'user_id' => Auth::user()->id
                ])->delete();
                Notification::removeData(
                    $post->user_id,
                    Auth::user()->id,
                    $post->id,
                    Notification::TYPE_LIKE_POST
                );
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'status' => self::FAIL_STATUS,
                'message' => trans('message.already_cancel_post')
            ]);
        }
        return response()->json(['status' => self::SUCCESS_STATUS]);
    }

    public function likeComment($commentId)
    {
        try {
            $comment = Comment::findOrFail($commentId);
            DB::transaction(function() use ($comment) {
                LikeComment::create([
                    'user_id' => Auth::user()->id,
                    'comment_id' => $comment->id
                ]);
                Notification::createData(
                    $comment->user_id,
                    Auth::user()->id,
                    $comment->id,
                    Notification::TYPE_LIKE_COMMENT
                );
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'status' => self::FAIL_STATUS,
                'message' => $e->getMessage()
            ]);
        }
        return response()->json(['status' => self::SUCCESS_STATUS]);
    }

    public function deleteLikeComment($commentId)
    {
        try {
            $comment = Comment::findOrFail($commentId);
            DB::transaction(function() use ($comment) {
                LikeComment::where([
                    'comment_id' => $comment->id,
                    'user_id' => Auth::user()->id
                ])->delete();
                Notification::removeData(
                    $comment->user_id,
                    Auth::user()->id,
                    $comment->id,
                    Notification::TYPE_LIKE_COMMENT
                );
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'status' => self::FAIL_STATUS,
                'message' => trans('message.already_like_comment')
            ]);
        }
        return response()->json(['status' => self::SUCCESS_STATUS]);
    }

    public function showUsersLikedPost($postId)
    {
        $likedUsers = LikePost::getUserLikedPost($postId);
        $clubId = Post::find($postId)->club_id;
        return view('like_page.show', compact('likedUsers', 'clubId'));
    }

    public function showUsersLikedConmment($commentId)
    {
        $likedUsers = LikeComment::getUserLikeCommentOfPost($commentId);
        $clubId = Post::find(Comment::find($commentId)->post_id)->club_id;
        return view('like_page.show', compact('likedUsers', 'clubId'));
    }

    public function showUsersLikedSupporter($supporterId)
    {
        $likedUsers = LikeSupporter::getUserLikedSupporter($supporterId);
        return view('like_page.show', compact('likedUsers', 'supporterId'));
    }
}
