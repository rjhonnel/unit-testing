<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use App\Models\Profile;
use App\Models\Goal;

class TutorialController extends Controller
{
    public function nickname()
    {
        return view('tutorial.nickname');
    }

    public function postNickname(Request $request)
    {
        $this->validate($request, ['nickname' => 'required|string']);
        session(['nickname' => $request->nickname]);
        return redirect()->route('tutorial.height_weight');
    }

    public function heightWeight()
    {
        return view('tutorial.height_weight');
    }

    public function postHeightWeight(Request $request)
    {
        $this->validate($request, [
            'height' => 'required|numeric',
            'weight' => 'required|numeric'
        ]);
        session(['height' => $request->height]);
        session(['weight' => $request->weight]);
        return redirect()->route('tutorial.gender');
    }

    public function gender()
    {
        return view('tutorial.gender');
    }

    public function postGender(Request $request)
    {
        $this->validate($request, ['gender' => 'required|numeric']);
        session(['gender' => $request->gender]);
        return redirect()->route('tutorial.birthday');
    }

    public function birthday()
    {
        return view('tutorial.birthday');
    }

    public function postBirthday(Request $request)
    {
        $this->validate($request, ['birthday' => 'required|date']);
        session(['birthday' => $request->birthday]);
        return redirect()->route('tutorial.plan');
    }

    public function plan()
    {
        return view('tutorial.plan');
    }

    public function postPlan(Request $request)
    {
        $this->validate($request, ['plan' => 'required|numeric']);
        session(['plan' => $request->plan]);
        if ($request->plan == 1) {
            return redirect()->route('tutorial.goal');
        }
        return redirect()->route('tutorial.confirm');
    }

    public function goal()
    {
        return view('tutorial.goal');
    }

    public function postGoal(Request $request)
    {
        $this->validate($request, [
            'weight' => 'required|numeric',
            'deadline' => 'required|date'
        ]);
        session(['goal_weight' => $request->weight]);
        session(['deadline' => $request->deadline]);
        return redirect()->route('tutorial.confirm');
    }

    public function confirm()
    {
        return view('tutorial.confirm');
    }

    public function postData(Request $request)
    {
        if ($request->has('cancel')) {
            return redirect()->route('tutorial')->with('error', trans('tutorial.message.try_again'));
        }
        $profile = [
            'nickname' => Session::pull('nickname'),
            'height' => Session::pull('height'),
            'weight' => Session::pull('weight'),
            'gender' => Session::pull('gender'),
            'birthday' => Session::pull('birthday'),
            'plan' => Session::pull('plan')
        ];
        try {
            Profile::updateOrCreate([
                'user_id' => Auth::id()
            ], $profile);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return redirect()->route('tutorial')->with('error', $e->getMessage());
        }
        if (Session::has('goal_weight')) {
            $goal = [
                'weight' => Session::pull('goal_weight'),
                'deadline' => Session::pull('deadline')
            ];
            try {
                Goal::updateOrCreate([
                    'user_id' => Auth::id()
                ], $goal);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return redirect()->route('tutorial')->with('error', $e->getMessage());
            }
        }
        return redirect()->route('recommend');
    }
}
