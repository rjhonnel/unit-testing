<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Program;
use App\Models\DoneReportUser;
use App\Models\ClubJoinUser;
use App\Models\Notification;
use App\User;
use App\Models\Club;
use Carbon\Carbon;
use Auth;
use Report;
use Log;

class MyPageController extends Controller
{

    public function index()
    {
        $numberNewsNotifications = Notification::countNewsNotifications(Auth::user()->id);
        $joinedPrograms = Program::getProgramsUserJoined(Auth::user()->id);
        $endProgram = Report::getEndProgramDataByDate(Auth::user()->id);
        $programsClubs = $this->getListProgramsAndClubsOfSupporter(Auth::user()->id);
        $menu = "mypage";
        return view('mypage.show', compact('endProgram', 'joinedPrograms', 'numberNewsNotifications', 'menu', 'programsClubs'));
    }

    public function close($id)
    {
        try {
            $club = ClubJoinUser::find($id);
            $club->program_end_checked_at = Carbon::now();
            $club->save();
        } catch(\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(['success' => false], 400);
        }
        return response()->json(['success' => true], 200);
    }

    public function ajaxDailyReport(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->done_report_type)) {
                $doneReportId = Program::getDoneReportByCurrentDate(Auth::user()->id, $request->program_id)->id;
                if (Program::isDoneReportUserToday(Auth::user()->id, $request->program_id)) {
                    DoneReportUser::deleteRecord(Auth::user()->id, $doneReportId);
                    return response()->json(['delete' => true]);
                }
                DoneReportUser::createData(Auth::user()->id, $doneReportId);
                return response()->json(['add' => true]);

            }
            return trans('message.error_occurred');
        }
    }
    
    public function getListProgramsAndClubsOfSupporter($userId) {
        $programsClubs = [];
        $supporter = User::find($userId)->supporter;
        if($supporter) {
            $supportedPrograms = $supporter->programs;
            if($supportedPrograms) {
                foreach ($supportedPrograms as $program) {
                    $clubs = Club::getClubNotFinishOfProgram($program->id);
                    $program->clubs = $clubs;
                    $programsClubs[] = $program;
                    unset($programsTmp);
                }
            }
        }
        return $programsClubs;
    }
}
