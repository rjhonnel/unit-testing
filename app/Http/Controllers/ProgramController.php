<?php

namespace App\Http\Controllers;

use Program as ProgramService;
use Illuminate\Http\Request;
use App\Models\Program;
use App\Models\ClubJoinUser;
use Supporter;
use App\Models\Supporter as SupporterModel;
use App\Models\DoneReport;
use Auth;
use Log;
use DB;

class ProgramController extends Controller
{
    const PER_PAGE_PROGRAM = 10;

    public function recommend()
    {
        $programs = ProgramService::getRecommendProgramList(Auth::user()->id);
        return view('programs.program_recommend', compact('programs'));
    }

    public function list()
    {
        $programs = Program::paginate(self::PER_PAGE_PROGRAM);
        $menu = "program";
        $joinedPrograms = Program::getProgramsUserJoined(Auth::user()->id);
        $supporters = Supporter::getSupporterList();
        return view('programs.list', compact('programs', 'menu', 'joinedPrograms', 'supporters'));
    }

    public function detail(Request $request, $programId)
    {
        $program = Program::find($programId);
        if (empty($program)) {
            return abort(404);
        }
        $isShow = $request->has('r') ? false : true;
        $isJoinedProgram = ClubJoinUser::isUserJoinedProgram(Auth::user()->id, $programId);
        $backUrl = isset($_SERVER['HTTP_REFERER'])
            ? $_SERVER['HTTP_REFERER'] : route('program.list');
        $profileOfSupporter = $program->supporter->user->profile;
        $menu = 'program';
        return view('programs.detail', compact(
                                        'program',
                                        'profileOfSupporter',
                                        'backUrl',
                                        'isShow',
                                        'isJoinedProgram',
                                        'menu'
                                    ));
    }

    public function updateUserJoinClub(Request $request)
    {
        if ($request->ajax()) {
            $program = Program::find($request->programId);
            if (empty($program)) {
                return trans('message.error_user_joined_club');
            }
            try {
                DB::beginTransaction();
                ClubJoinUser::createUserJoinClub(Auth::user()->id, $program);
                DoneReport::createDoneReportIfNotExist(Auth::user()->id, $program->id);
                DB::commit();
            } catch(Exception $e) {
                Log::error($e->getMessage());
                DB::rollback();
                return trans('message.error_user_joined_club');
            }
            return trans('message.success_user_joined_club');
        }
    }

    public function join(Request $request)
    {
        $programs = Program::whereIn('id', $request->input('program_id'))->get();
        try {
            DB::beginTransaction();
            // Join program
            foreach ($programs as $program) {
                ClubJoinUser::createUserJoinClub(Auth::user()->id, $program);
                DoneReport::createDoneReportIfNotExist(Auth::user()->id, $program->id);
            }
            DB::commit();
        } catch(\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            return redirect()->back()->with('error', trans('message.error_user_joined_club'));
        }
        return redirect()->route('mypage.index')->with('success', trans('message.success_user_joined_club'));
    }
}
