<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Models\DoneReportUser;
use App\Models\PostImage;
use App\Traits\UploadImage;
use Auth;
use DB;
use Log;

class PostController extends Controller
{
    use UploadImage;
    const FAIL_STATUS = 0;
    const SUCCESS_STATUS = 1;

    public function show($postId)
    {
        $post = Post::find($postId);
        if (empty($post)) {
            Log::warning('Not Found: ' . json_encode(['post_id' => $postId]));
            abort(404);
        }
        return view('posts.show', compact('post'));
    }

    public function delete($postId)
    {
        $res = Post::deleteRecord(Auth::user()->id, $postId);
        return response()->json(['status' => $res]);
    }

    public function report($doneReportId)
    {
        $res = DoneReportUser::createData(Auth::user()->id, $doneReportId);
        return response()->json([ 'status' => $res ]);
    }

    public function deleteReport($doneReportId)
    {
        $res = DoneReportUser::deleteRecord(Auth::user()->id, $doneReportId);
        return response()->json([ 'status' => $res ]);
    }

    public function create(Request $request)
    {
        if (!$request->has('club_id')) {
            return abort(404);
        }
        return view('posts.create', ['club_id' => $request->input('club_id')]);
    }

    public function store(PostRequest $request)
    {
        DB::beginTransaction();
        try {
            // Save to posts
            $text = $request->input('text') ? $request->input('text') : '';
            $post = Post::create([
                'club_id' => $request->input('club_id'),
                'user_id' => Auth::user()->id,
                'text' => $text
            ]);
            $images = $request->file('images');
            $no = 1;
            if ($request->hasFile('images')) {
                foreach ($images as $image) {
                    $imageFileName = $this->getFileNameByPost($image, $post->id, $no);
                    // Upload image to s3
                    $this->doUploadResizedImage($image, $imageFileName, config('post.image'));
                    // Save to post_images
                    PostImage::create([
                        'post_id' => $post->id,
                        'image_url' => $this->getImagePath($imageFileName, config('post.image'))
                    ]);
                    $no++;
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            return redirect()->back()->with('error', $e->getMessage());
        }
        // Back to post list
        return redirect()->route('communications.list', $request->input('club_id'));
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        if (empty($post)) {
            Log::warning('Not Found: ' . json_encode(['post_id' => $id]));
            return abort(404);
        }
        return view('posts.edit', compact('post'));
    }

    public function update(PostRequest $request, $id)
    {
        $post = Post::find($id);
        if (empty($post) || $post->user_id !== Auth::user()->id) {
            if (empty($post)) {
                Log::warning('Not Found: ' . json_encode(['post_id' => $id]));
            } else {
                Log::warning('Permission denied: ' . json_encode([
                    'post_id' => $id,
                    'user_id' => Auth::user()->id
                ]));
            }
            return abort(404);
        }
        DB::beginTransaction();
        try {
            // Update text
            $post = Post::find($id);
            $post->text = $request->input('text');
            $post->save();
            // Update images if is changed
            if ($request->input('is_file_change')) {
                $images = $request->file('images');
                $no = 1;
                if ($request->hasFile('images')) {
                    // Delete into post_images by post_id
                    PostImage::postId($id)->delete();
                    foreach ($images as $image) {
                        $imageFileName = $this->getFileNameByPost($image, $id, $no);
                        // Upload image to s3
                        $this->doUploadResizedImage($image, $imageFileName, config('post.image'));
                        // Save to post_images
                        PostImage::create([
                            'post_id' => $id,
                            'image_url' => $this->getImagePath($imageFileName, config('post.image'))
                        ]);
                        $no++;
                    }
                }
            }
            DB::commit();
        } catch(\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            return redirect()->back()->with('error', $e->getMessage());
        }
        return redirect()->route('communications.list', $request->input('club_id'));
    }

    private function getFileNameByPost($image, $postId, $imageNo)
    {
        $preImage = sprintf(
                '%s_%s_%s_%s',
                $postId,
                Auth::user()->id,
                date('YmdHis'),
                $imageNo
            );
        return $preImage . '.' . strtolower($image->getClientOriginalExtension());
    }
}
