<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\User;
use Auth;

class TopPageController extends Controller
{
    public function index () {
        $link = '/tutorial';
        if (Auth::user()->agreement_flag === 0) {
            $link = '';
        } elseif (Profile::existProfile(Auth::user()->id)) {
            $link = '/mypage';
        }
        return view('top_page.show', compact('link'));
    }

    public function ajaxTopPage (Request $request) {
        if ($request->ajax()) {
            $id = Auth::user()->id;
            User::find($id)->update([
                'agreement_flag' => 1
            ]);
            $data = '/tutorial';
            return $data;
        }
    }
}
