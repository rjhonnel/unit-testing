<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserProfileRequest;
use App\Models\Profile;
use App\Models\User;
use App\Models\Goal;
use App\Models\UserWeight;
use App\Traits\UploadImage;
use Carbon\Carbon;
use Auth;
use DB;
use Log;
use App\Services\ProfileService;

class UserController extends Controller
{
    use UploadImage;

    const STATUS_ERROR = 'error';
    const STATUS_SUCCESS = 'success';
    private $rules = [
        'nickname' => 'required|string',
        'height' => 'required|numeric',
        'goalweight' => 'required|numeric',
        'weight' => 'required|numeric',
        'plan' => 'required|numeric',
        'deadline' => 'required|date|after:now',
        'gender' => 'required|numeric',
        'birthday' => 'required|date|before:now',
        'plan' => 'required|numeric'
    ];
    private $p;

    public function __construct(ProfileService $profile)
    {
        $this->p = $profile;
    }

    public function weightGraph(Request $request)
    {
        $time = $request->has('time') ? $request->time : strtotime('now');
        $weights = UserWeight::getDataByMonth(Auth::user()->id, $time);
        $menu = 'log';
        return view('weight-reports.graph', compact('weights', 'time', 'menu'));
    }

    public function editProfile()
    {
        $currentWeight = UserWeight::getLastWeightReport(Auth::user()->id);
        return view('profiles.show')->with([
            'profile' => Auth::user()->profile,
            'goal' => Auth::user()->goal,
            'currentWeight' => !empty($currentWeight) ? $currentWeight->weight : null,
            'menu' => 'account'
        ]);
    }

    public function editField(Request $request)
    {
        $currentWeight = UserWeight::getLastWeightReport(Auth::user()->id);
        $data = [
            'profile' => Auth::user()->profile,
            'goal' => Auth::user()->goal,
            'field' => $request->f,
            'currentWeight' => !empty($currentWeight) ? $currentWeight->weight : null,
            'menu' => 'account'
        ];
        if (!empty($request->create) && $request->create == 'profile') {
            return view('profiles.create_profile')->with($data);
        }
        if (!empty($request->create) && $request->create == 'goal') {
            return view('profiles.create_goal')->with($data);
        }
        return view('profiles.edit_field')->with($data);
    }

    public function updateField(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);
        foreach ($data as $key => $value) {
            $this->validate($request, [
                $key => $this->rules[$key]
            ]);
        }
        $userId = Auth::user()->id;
        try {
            $this->p->handleUpdateData($userId, $data);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('profile.show')->with('error', $e->getMessage());
        }
        return redirect()->route('profile.show')->with('success', trans('message.update_success'));
    }

    public function updateAvatar(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'required|mimes:jpeg,jpg,png'
        ]);
        $file = $request->avatar;
        $filename = Auth::id() . strtotime('now') . $file->getClientOriginalName();
        try {
            $this->doUploadResizedImage($file, $filename, config('post.avatar'));
            $profile = Auth::user()->profile;
            $profile->photo_image_url = $this->getImagePath($filename, config('post.avatar'));
            $profile->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('profile.show')->with('error', $e->getMessage());
        }
        return redirect()->route('profile.show')->with('success', trans('message.update_success'));
    }
}
