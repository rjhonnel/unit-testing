<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use Auth;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = Notification::getListNotificationByUserId(Auth::user()->id);
        return view('notifications.show', compact('notifications'));
    }

    public function updateReadAt($id)
    {
        $res = Notification::updateReadAt($id);
        return response()->json(['status' => $res]);
    }
}
