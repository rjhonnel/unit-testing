<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserWeight;
use App\Models\DoneReportUser;
use App\Models\DoneReport;
use Auth;
use Log;

class WeightReportController extends Controller
{
    public function index($doneReportId)
    {
        $doneReport = DoneReport::find($doneReportId);
        if (empty($doneReport)) {
            Log::warning('Not Found: ' . json_encode(['done_report_id' => $doneReportId]));
            abort(404);
        }
        return view('weight-reports/show', [
            'doneReport' => $doneReport
        ]);
    }

    public function create(Request $request, $doneReportId)
    {
        $this->validate($request, [
            'weight' => 'required|numeric',
        ]);
        $doneReport = DoneReport::find($doneReportId);
        if (empty($doneReport)) {
            Log::warning('Not Found: ' . json_encode(['done_report_id' => $doneReportId]));
            abort(404);
        }
        if (UserWeight::createData(Auth::user()->id, $request->weight)) {
            DoneReportUser::createData(Auth::user()->id, $doneReportId);
            return redirect('communications/' . $doneReport->post->club_id)
                    ->with('success', trans('message.success'));
        }
        return redirect('communications/' . $doneReport->post->club_id)
                    ->with('error', trans('message.already_report'));
    }

    public function createWeightWithoutDoneReportUser(Request $request)
    {
        $this->validate($request, [
            'weight' => 'required|numeric',
            'date' => 'required|date'
        ]);
        if (UserWeight::createData(Auth::user()->id, $request->weight, $request->date)) {
            return redirect()->back()->with('success', trans('message.update_success'));
        }
        return redirect()->back()->with('error', trans('message.error'));
    }
}
