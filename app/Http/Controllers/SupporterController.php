<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Supporter;
use Auth;
use App\Models\LikeSupporter;
use App\Models\Supporter as SupporterModel;
use App\Models\Notification;

class SupporterController extends Controller
{
    public function index()
    {
        $supporters = Supporter::getSupporterList();
        $menu = "supporter";
        return view('supporter.index', compact('supporters', 'menu'));
    }

    public function detail($id)
    {
        // Get detail of supporter
        $supporter = Supporter::getSupporterDetailById($id);
        if (empty($supporter)) {
            return abort(404);
        }
        // Get supporter's program
        $programs = Supporter::getSupporterProgramList($id);
        // Check user liked supporter
        $likeData = LikeSupporter::supporterUserId($id, Auth::id())->first();
        $isLike = empty($likeData) ? false : true;
        $menu = "supporter";
        return view('supporter.detail', compact('supporter', 'programs', 'isLike', 'menu'));
    }

    public function like($supporterId)
    {
        $supporter = SupporterModel::find($supporterId);
        $userId = Auth::id();
        if (empty($supporterId) || empty($userId)) {
            return response()->json(
                [
                    'success' => false,
                    'message' => 'お気に入りが失敗しました!'
                ],
                400
            );
        }
        $likeData = LikeSupporter::supporterUserId($supporterId, $userId)->first();
        if (empty($likeData)) {
            $likeSupporter = LikeSupporter::create([
                    'supporter_id' => $supporterId,
                    'user_id' => $userId
                ]);
            Notification::createData(
                $supporter->user->id,
                Auth::user()->id,
                $supporter->id,
                Notification::TYPE_LIKE_SUPPORTER
            );
        }
        $cntLike = LikeSupporter::supporterId($supporterId)->count();
        return response()->json(
            [
                'success' => true,
                'num_like' => $cntLike
            ],
            200
        );
    }

    public function unlike($supporterId)
    {
        $userId = Auth::id();
        if (empty($supporterId) || empty($userId)) {
            return response()->json(
                [
                    'success' => false,
                    'message' => 'お気に入りが失敗しました!'
                ],
                400
            );
        }
        $likeData = LikeSupporter::supporterUserId($supporterId, $userId)->first();
        if (!empty($likeData)) {
            LikeSupporter::supporterUserId($supporterId, $userId)->delete();
        }
        $cntLike = LikeSupporter::supporterId($supporterId)->count();
        return response()->json(
            [
                'success' => true,
                'num_like' => $cntLike
            ],
            200
        );
    }
}
