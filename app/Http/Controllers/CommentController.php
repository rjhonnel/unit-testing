<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Notification;
use Auth;
use DB;
use Log;

class CommentController extends Controller
{
    const FAIL_STATUS = 0;
    const SUCCESS_STATUS = 1;
    const PER_PAGE = 10;

    public function show($postId)
    {
        $post = Post::find($postId);
        $comments = Comment::where('post_id', $postId)->paginate(self::PER_PAGE);
        if(!$post || !$post->club->isJoin(Auth::user()->id)) {
            if (!$post) {
                Log::warning('Not Found: ' . json_encode(['post_id' => $postId]));
            } else {
                Log::warning('Permission denied: ' . json_encode([
                    'post_id' => $postId,
                    'user_id' => Auth::user()->id
                ]));
            }
            abort(404);
        }
        return view('comments.show', compact('post', 'comments'));
    }

    public function create(Request $request, $postId)
    {
        $post = Post::find($postId);
        if (empty($post) || empty($request->content)) {
            return response()->json(['status' => self::FAIL_STATUS]);
        }
        $comment = null;
        try {
            DB::transaction(function() use ($post, $request, &$comment) {
                $comment = Comment::create([
                    'post_id' => $post->id,
                    'user_id' => Auth::user()->id,
                    'text' => $request->content
                ]);
                Notification::createData(
                    $post->user_id,
                    Auth::user()->id,
                    $comment->id,
                    Notification::TYPE_ADD_COMMENT
                );
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return view('comments.error_response', ['message' => $e->getMessage()]);
        }
        return view('comments.sub_comment', compact('comment'));
    }

    public function edit(Request $request, $commentId)
    {
        $comment = Comment::find($commentId);
        if (empty($comment) || empty($request->content)) {
            return response()->json(['status' => self::FAIL_STATUS]);
        }
        $comment->text = $request->content;
        $comment->edited_at = \Carbon\Carbon::now();
        $comment->save();
        return response()->json(['status' => self::SUCCESS_STATUS]);
    }

    public function delete($commentId)
    {
        try {
            $comment = Comment::findOrFail($commentId);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(['status' => self::FAIL_STATUS]);
        }
        $comment->delete();
        return response()->json(['status' => self::SUCCESS_STATUS]);
    }

}
