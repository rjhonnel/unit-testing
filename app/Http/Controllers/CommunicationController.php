<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Club;
use App\Models\ClubJoinUser;
use App\User;
use Auth;
use Log;

class CommunicationController extends Controller
{
    const PER_PAGE = 10;

    public function show($clubId)
    {
        $club = Club::find($clubId);
        if (empty($club) || (!$club->isJoin(Auth::user()->id) && !User::isOwnClub(Auth::user()->id, $clubId))) {
            if (empty($club)) {
                Log::warning('Not Found: ' . json_encode(['club_id' => $clubId]));
            } else {
                Log::warning('Permission denied: ' . json_encode([
                    'club_id' => $clubId,
                    'user_id' => Auth::user()->id
                ]));
            }
            abort(404);
        }
        $posts = Post::getPostByClubId($clubId)->paginate(self::PER_PAGE);
        $userWeight = ClubJoinUser::getUserWeightByCurrentDate($clubId);
        $userWeight = empty($userWeight) ? 0 : $userWeight;
        foreach ($posts as &$post) {
            if($post->doneReports->count() > 0) {
                $post->doneReportId = $post->doneReports->first()->id;
                $countDoneReportUsers = count($post->doneReports->first()->doneReportUsers);
                $post->countDoneReportUsers = $club->program->done_report_type==1 ? $countDoneReportUsers : $userWeight;
                $post->isDoneReport = ClubJoinUser::isUserDoneReport(Auth::user()->id, $post->id);
            }
        }
        return view("communication.show")->with([
            'posts' => $posts,
            'club' => $club,
            'joinUserCount' => count($club->joinUsers()),
        ]);
    }
}
