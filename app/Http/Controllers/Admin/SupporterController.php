<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Supporter;
use App\User;
use App\Models\Profile;
use App\Traits\UploadImage;

class SupporterController extends Controller
{
    use UploadImage;

    const PER_PAGE = 15;

    public function index()
    {
        $supporters = Supporter::paginate(self::PER_PAGE);
        return view ('admin.supporters.index', compact('supporters'));
    }

    public function create()
    {
        return view('admin.supporters.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nickname' => 'required|string',
            'email' => 'required|email|unique:users',
            'self_introduction' => 'required|string',
            'password' => 'required|string|min:6',
            'image' => 'required|mimes:jpg,jpeg,png,gif',
            'cover_image' => 'required|mimes:jpg,jpeg,png,gif'
        ]);
        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        Supporter::create(['user_id' => $user->id]);
        $profile = [
            'user_id' => $user->id,
            'nickname' => $request->nickname,
            'self_introduction' => $request->self_introduction,
            'height' => 100,
            'weight' => 100,
            'birthday' => date('Y-m-d')
        ];
        $filename = strtotime('now') . 'avatar' . $request->image->getClientOriginalName();
        $profile['photo_image_url'] = $this->uploadPhoto($request->image, $filename);
        $filename = strtotime('now') . 'cover' . $request->cover_image->getClientOriginalName();
        $profile['cover_image_url'] = $this->uploadPhoto($request->cover_image, $filename);
        Profile::create($profile);
        return redirect()->route('admin.supporter.list')->with('success', trans('supporter.message.update_success'));
    }

    public function show($id)
    {
        $supporter = Supporter::find($id);
        return view('admin.supporters.show', compact('supporter'));
    }

    public function edit($id)
    {
        $supporter = Supporter::find($id);
        return view('admin.supporters.edit', compact('supporter'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nickname' => 'required|string',
            'self_introduction' => 'required|string',
            'password' => 'nullable|string|min:6',
            'image' => 'nullable|mimes:jpg,png,jpeg,gif',
            'cover_image' => 'nullable|mimes:jpg,png,jpeg,gif'
        ]);
        // update user
        if (!empty($request->password)) {
            Supporter::find($id)->user->update(['password' => bcrypt($request->password)]);
        }
        $profile = [
            'nickname' => $request->nickname,
            'self_introduction' => $request->self_introduction
        ];
        // update avatar
        if (!empty($request->image)) {
            $filename = strtotime('now') . 'avatar' . $request->image->getClientOriginalName();
            $profile['photo_image_url'] = $this->uploadPhoto($request->image, $filename);
        }
        // update cover image
        if (!empty($request->cover_image)) {
            $filename = strtotime('now') . 'cover' . $request->image->getClientOriginalName();
            $profile['cover_image_url'] = $this->uploadPhoto($request->cover_image, $filename);
        }
        Supporter::find($id)->user->profile->update($profile);
        return redirect()->route('admin.supporter.list')
                ->with('success', trans('supporter.message.update_success'));
    }

    private function uploadPhoto($file, $filename)
    {
        try {
            $this->doUploadResizedImage($file, $filename, config('post.avatar'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return '';
        }
        return $this->getImagePath($filename, config('post.avatar'));
    }
}
