<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Club;

class ClubJoinUserController extends Controller
{
    const PER_PAGE = 15;
    const ACTION = 'club_join_user';

    public function index(Request $request)
    {
        $club = Club::find($request->club_id);
        if (empty($club)) {
            \Log::warning('Not Found: ' . json_encode(['club_id' => $request->club_id]));
            abort(404);
        }
        $users = Club::getUserJoinedClub($club);
        $action = self::ACTION;
        return view('admin.users.index', compact('users', 'action'));
    }
}
