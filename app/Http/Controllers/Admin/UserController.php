<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    const PER_PAGE = 15;

    public function index(Request $request)
    {
        if (!empty($request->search)) {
            $users = User::seachUserByIdOrNickname($request->search);
            $users->appends($request->all());
        } else {
            $users = User::with('profile', 'goal')->paginate(self::PER_PAGE);
        }
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6'
        ]);
        try {
            User::create([
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);
        } catch (\Exception $e) {
            return redirect()->route('admin.user.list')->with('error', $e->getMessage());
        }
        return redirect()->route('admin.user.list')->with('success', trans('user.message.create_success'));
    }

    public function show($id)
    {
        $user = User::with('profile', 'goal', 'clubs')->where('id', $id)->first();
        $clubs = $user->clubs;
        return view('admin.users.show', compact('user', 'clubs'));
    }
}
