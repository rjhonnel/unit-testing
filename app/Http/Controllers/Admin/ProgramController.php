<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Program;
use App\Models\Supporter;
use App\Traits\UploadImage;
use DataTables;
use Lang;
use Log;
use Auth;

class ProgramController extends Controller
{
    use UploadImage;
    private $rules = [
        'title' => 'required|string',
        'description' => 'required|string',
        'done_report_type' => 'required',
        'capacity' => 'required|integer',
        'program_days' => 'required|integer',
        'program_level' => 'required',
        'image_url' => 'required|image|mimes:jpeg,jpg,png|max:10024',
        'supporter_id' => 'required'
    ];

    public function index()
    {
        return view('admin.programs.index');
    }

    public function getPrograms()
    {
        return DataTables::of(Program::query())
            ->addColumn('action', function ($program) {
                return '<a href="'. route('admin.program.show', $program->id) .'" type="button" class="btn btn-success btn-xs" target="_blank">'. Lang::get('button.show') .'</a> <a href="'. route('admin.programs.edit', $program->id) .'" type="button" class="btn btn-warning btn-xs">'. Lang::get('button.edit') .'</a>';
            })
            ->editColumn('supporter_id', function ($program) {
                return  $program->supporter_id . ': ' . Supporter::getNameOfSupporter($program);
            })
            ->make(true);
    }


    public function show($id)
    {
        $program = Program::find($id);
        return view('admin.programs.show', compact('program'));
    }

    public function create()
    {
        $supporters = Supporter::getListOfSupporter();
        return view('admin.programs.add', compact('supporters'));
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $data = $request->all();
        $file = $request->image_url;
        $filename = Auth::guard('admin')->user()->id . '_' . strtotime('now') . $file->getClientOriginalName();
        try {
            $this->doUploadResizedImage($file, $filename, config('program.image'));
            $data['image_url'] = $this->getImagePath($filename, config('program.image'));
            Program::create($data);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('admin.program.create')->with('error', trans('message.create_error'));
        }
        return redirect()->route('admin.program.index')->with('success', trans('message.create_success'));
    }

    public function edit($id)
    {
        $program = Program::find($id);
        $supporters = Supporter::getListOfSupporter();
        return view('admin.programs.edit', compact('program', 'supporters'));
    }

    public function update(Request $request, $id)
    {
        unset($this->rules['image_url']);
        $this->validate($request, $this->rules);
        $data = $request->all();
        $file = $request->image_url;
        try {
            if (file_exists($file)) {
                $filename = Auth::guard('admin')->user()->id . '_' . strtotime('now') . $file->getClientOriginalName();
                $data['image_url'] = $this->getImagePath($filename, config('program.image'));
                $this->doUploadResizedImage($file, $filename, config('program.image'));
            }
            Program::find($id)->update($data);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect()->route('admin.programs.edit', $id)->with('error', trans('message.update_error'));
        }
        return redirect()->route('admin.program.index')->with('success', trans('message.update_success'));
    }
}
