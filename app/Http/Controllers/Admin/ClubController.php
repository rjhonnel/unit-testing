<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Club;

class ClubController extends Controller
{
    const PER_PAGE = 15;

    public function show(Request $request)
    {
        $clubs = Club::where('program_id', $request->program_id)
                     ->paginate(self::PER_PAGE)
                     ->appends(['program_id' => $request->program_id]);
        return view('admin.clubs.show', compact('clubs'));
    }

    public function detail($id)
    {
        $club = Club::find($id);
        return view('admin.clubs.detail', compact('club'));
    }
}
