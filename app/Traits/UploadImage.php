<?php

namespace App\Traits;

use AWS;
use Image;

trait UploadImage
{
    /**
     *
     * @param  [string] $image
     * @param  [string] $fileName
     * @param  [string] $mime
     * @param  [string] $folder
     * @return [void]
     * @throws [S3Exception]
     */
    public function uploadImageToS3($image, $fileName, $mime, $folder)
    {
        $s3 = AWS::createClient('s3');
        $s3->putObject([
                'Bucket'      => env('AWS_BUCKET_NAME'),
                'Key'         => "{$folder}/{$fileName}",
                'Body'        => (string)$image->encode(),
                'ContentType' => $mime,
                'ACL'         => 'public-read',
            ]);
    }

    public function getImagePath($filename, array $config)
    {
        return sprintf(
            $config['url'],
            env("AWS_BUCKET_NAME"),
            $config['path'],
            $filename
        );
    }

    private function doUploadResizedImage($file, $filename, array $config)
    {
        $imageObj = Image::make($file);
        $imageObj->resize(
                        $config['width'],
                        $config['height'],
                        function($constraint) {
                            $constraint->aspectRatio();
                        }
                    );
        // Upload image to s3
        $this->uploadImageToS3(
            $imageObj,
            $filename,
            $file->getMimeType(),
            $config['path']
        );
    }
}
