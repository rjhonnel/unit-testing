<?php
namespace App\Services;

use App\Models\Goal;
use App\Models\Profile;
use App\User;

class ProfileService
{
    public function handleUpdateData($userId, $data)
    {
        $user = User::find($userId);
        if (!empty($data['deadline']) || !empty($data['goalweight'])) {
            $this->updateGoal($userId, $data, $user->goal);
        } else {
            $this->updateProfile($userId, $data, $user->profile);
        }
    }

    private function updateGoal($userId, $data, $goal)
    {
        if (!empty($data['goalweight'])) {
            $data['weight'] = $data['goalweight'];
            unset($data['goalweight']);
        }
        if (empty($goal)) {
            $data['user_id'] = $userId;
            Goal::create($data);
        } else {
            Goal::userId($userId)->update($data);
        }
    }

    private function updateProfile($userId, $data, $profile)
    {
        if (empty($profile)) {
            $data['user_id'] = $userId;
            Profile::create($data);
        } else {
            Profile::userId($userId)->update($data);
        }
    }

}