<?php
namespace App\Services;

use DB;
use App\Models\Program;
use App\Models\LikeSupporter;

class Supporter
{
    public function getSupporterList($id = null)
    {
        $res = DB::table('supporters AS s')
            ->join('profiles AS p', 's.user_id', '=', 'p.user_id')
            ->leftJoin('like_supporters AS ls', function($join) {
                $join->on('s.id', '=', 'ls.supporter_id');
            })
            ->select(DB::raw('
                        s.id, p.nickname, p.photo_image_url,
                        p.self_introduction, p.cover_image_url,
                        count(ls.user_id) as cnt_liked
                    ')
            );
        if ($id) {
            return $res->where('s.id', '=', $id)->groupBy('s.id')->first();
        }
        return $res->groupBy('s.id')->get();
    }

    public function getSupporterDetailById($id)
    {
        return $this->getSupporterList($id);
    }

    public function getSupporterProgramList($id)
    {
        return Program::supporterId($id)->get();
    }
}
