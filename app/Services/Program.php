<?php
namespace App\Services;

use DB;
use App\Models\Program as ProgramModel;
use App\Models\RecommendProgram;

class Program
{
    const RECOMMEND_NUM = 3;

    public function getRecommendProgramList($userId)
    {
        $programs = RecommendProgram::userId($userId)->get();
        if ($programs->count() > 0) {
            return $programs;
        }
        // Make recommendation logic
        $ids = $this->makeRecommendData();
        foreach ($ids as $id) {
            RecommendProgram::create([
                'user_id' => $userId,
                'program_id' => $id
            ]);
        }
        return RecommendProgram::userId($userId)->get();
    }

    public function makeRecommendData()
    {
        $ids = [];
        $programs = ProgramModel::all();
        foreach ($programs as $program) {
            $ids[] = $program->id;
        }
        if (count($ids) <= self::RECOMMEND_NUM) {
            return $ids;
        }
        // Randomizes the order of the elements in array
        shuffle($ids);
        return array_slice($ids, 0, self::RECOMMEND_NUM);
    }
}
