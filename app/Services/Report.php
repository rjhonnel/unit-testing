<?php

namespace App\Services;

use App\Models\ClubJoinUser;
use Carbon\Carbon;
use DB;

class Report
{
    /**
     * Get end program's data
     *
     * @param [int] $userId
     * @param [string] $date Eg: 2017-11-17
     * @return [array]
     */
    public function getEndProgramDataByDate($userId, $date = null)
    {
        $today = Carbon::now()->format('Y-m-d');
        if ($date) {
            $today = $date;
        }
        $endPrograms = ClubJoinUser::userEndDate(
                    $userId, $today)->get();
        $programs = [];
        foreach ($endPrograms as $endProgram) {
            // Skip dialog if user is checked end program
            if ($endProgram->program_end_checked_at) {
                continue;
            }
            // Skip dialog if start or end date is null
            if (!$endProgram->start_date || !$endProgram->end_date) {
                continue;
            }
            return $this->convertEndProgramData($endProgram);
        }
        return [];
    }

    private function convertEndProgramData($endProgram)
    {
        $clubJoinUserId = $endProgram->id;
        $programName = $endProgram->club->program->title;
        $joinedDateArray = $this->getClubUserDoneReportDateArray(
                                        $endProgram->club_id,
                                        $endProgram->user_id
                                    );
        list($dayOfWeeks, $days, $joins) = $this->getUserJoinedPeriodArray(
                                            $endProgram->start_date,
                                            $endProgram->end_date,
                                            $joinedDateArray
                                        );
        return compact('clubJoinUserId', 'programName', 'dayOfWeeks', 'days', 'joins');
    }

    public function getUserJoinedPeriodArray($startDate, $endDate, $joinedDateArray)
    {
        $dayOfWeeks = $days = $joins = [];
        $startDateObj = new Carbon($startDate);
        $endDateObj = new Carbon($endDate);
        $endDateObj->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $dateRange = new \DatePeriod($startDateObj, $interval, $endDateObj);
        foreach ($dateRange as $date) {
            $dayOfWeeks[] = getDayOfWeek($date->dayOfWeek);
            $days[] = $date->format('m/d');
            $joins[] = in_array($date->format('Y-m-d'), $joinedDateArray, true) ? true : false;
        }
        return [$dayOfWeeks, $days, $joins];
    }

    public function getClubUserDoneReportDateArray($clubId, $userId)
    {
        $joinedDates = DB::table('done_reports AS dr')
            ->leftJoin('done_report_users AS dru', 'dr.id', '=', 'dru.done_report_id')
            ->join('posts AS p', 'dr.post_id', '=', 'p.id')
            ->select(DB::raw('dr.date'))
            ->where('p.club_id', '=', $clubId)
            ->where('dru.user_id', '=', $userId)
            ->get();
        $joinedDateArray = [];
        if ($joinedDates->count() > 0) {
            foreach ($joinedDates as $item) {
                $joinedDateArray[] = $item->date;
            }
        }
        return $joinedDateArray;
    }
}
