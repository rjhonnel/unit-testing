<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Token extends Model
{
    protected $table = 'tokens';
    protected $fillable = ['user_id', 'token', 'expire_date'];
    public $timestamps = FALSE;

    protected static function getTokenByUserId($userId)
    {
        return Token::select('token')
                    ->where('user_id', $userId)
                    ->first();
    }

    protected static function checkToken($token)
    {
        $check = Token::select('user_id', 'expire_date')->where('token', $token)->first();
        if ((!empty($check)) && (strtotime($check->expire_date) >= strtotime(Carbon::now())) ) {
            return $check->user_id;
        }
        return NULL;
    }
}
