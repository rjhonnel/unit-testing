<?php

if (!function_exists('h')) {
    /**
     * Convert special characters to HTML entities
     *
     * @param string $st
     * @param string $flag
     * @return string
     */
    function h($st, $flag = ENT_QUOTES)
    {
        return htmlspecialchars($st, $flag);
    }
}

if (!function_exists('getDayOfWeek')) {
    /**
     * Get day of week
     */
    function getDayOfWeek($day)
    {
        $week = ['日', '月', '火', '水', '木', '金', '土'];
        return isset($week[$day]) ? $week[$day] : null;
    }
}

if (!function_exists('showContentNotification')) {
    function showContentNotification($text)
    {
        if (strlen($text) > config('notification.text_lenght')) {
            echo htmlspecialchars(mb_substr($text, 0, config('notification.text_lenght')) . '...');
        } else {
            echo htmlspecialchars($text);
        }
    }
}

if (!function_exists('trimStr')) {
    function trimStr($text, $length) {
        if (strlen($text) > $length) {
            echo htmlspecialchars(mb_substr($text, 0, $length) . '...');
        } else {
            echo htmlspecialchars($text);
        }
    }
}

if (!function_exists('showStar')) {
    function showStar() {
        $numberStarColor = 1;
        for ($i = 1; $i <= 5; $i++) {
            echo '<option value="' . $numberStarColor . '">';
            for ($j = 1; $j <= 5; $j++) {
                if ($j <= $numberStarColor) {
                    echo '★';
                } else {
                    echo '✩';
                }
            }
            $numberStarColor = $numberStarColor + 1;
            echo '</option>';
        }
    }
}

if (!function_exists('getAgeFromBirthday')) {
    function getAgeFromBirthday($date)
    {
        return intval(date('Y', time() - strtotime($date))) - 1970;
    }
}

if (!function_exists('formatDateTime')) {
    function formatDateTime($datetime)
    {
        return \Carbon\Carbon::parse($datetime)->format('d/m/Y');
    }
}
#Validation
if (!function_exists('validateForm')) {
    function validateForm($errors, $field)
    {
        if($errors->has($field)) {
            echo '<i class="text-danger">'. $errors->first($field) .'</i>';
        } else {
            echo '';
        }
    }
}

