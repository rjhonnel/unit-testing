<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('css', function($filePath) {
            $path = base_path() . "/public" . $filePath;
            return $filePath . "?v=<?php echo \File::lastModified('$path') ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('supporter', function() {
            return new \App\Services\Supporter;
        });

        $this->app->bind('program', function() {
            return new \App\Services\Program;
        });

        $this->app->bind('report', function() {
            return new \App\Services\Report;
        });

        $this->app->bind('profile', function() {
            return new \App\Services\ProfileService;
        });
    }
}
