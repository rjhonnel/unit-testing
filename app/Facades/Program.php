<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see App\Services\Program
 */
class Program extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'program'; }
}
