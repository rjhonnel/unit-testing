TAG=$(git rev-parse --short HEAD)
NAME=registry.bikatsubu.jp:5000/bikatsubu-app

docker build -t $NAME:$TAG ./
docker push $NAME:$TAG 
docker tag $NAME:$TAG $NAME:latest
docker push $NAME:latest

