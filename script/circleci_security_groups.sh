#!/bin/bash

export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
export AWS_DEFAULT_REGION="ap-northeast-1"

MYPORT=$2
MYSECURITYGROUP=$3
MYIP=`curl -s ifconfig.me`

case $1 in
    "add" )
        echo "add: $MYIP $MYSECURITYGROUP"
        aws ec2 authorize-security-group-ingress --group-id $MYSECURITYGROUP --protocol tcp --port $MYPORT --cidr $MYIP/32
        ;;
    "remove" )
        echo "remove: $MYIP $MYSECURITYGROUP"
        aws ec2 revoke-security-group-ingress --group-id $MYSECURITYGROUP --protocol tcp --port $MYPORT --cidr $MYIP/32
        ;;
    * )
        echo "$0 {add|remove}"
        ;;
esac
