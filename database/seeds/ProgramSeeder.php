<?php

use Illuminate\Database\Seeder;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS=0');
      DB::table('programs')->truncate();
      DB::table('clubs')->truncate();

      // programs
      $p = new App\Models\Program;
      $p->supporter_id = 1;
      $p->title = "糖質制限ダイエット";
      $p->description = "低炭水化物ダイエットとは、肥満や糖尿病の治療を目的として炭水化物の摂取比率や摂取量を制限する食餌療法である。低糖質食、糖質制限食、ローカーボ・ダイエットとも呼ばれる。本質的には炭水化物で摂取していたエネルギーをタンパク質と脂質に置き換える食事法である。";
      $p->done_report_type = 1;
      $p->capacity = 50;
      $p->program_level = 4;
      $p->image_url = "/images/icon.jpg";
      $p->save();

      // clubs
      $c = new App\Models\Club;
      $c->program_id = $p->id;
      $c->save();
      DB::statement('SET FOREIGN_KEY_CHECKS=1');


      $tbl = [
          [ 'title' => '毎日体重測定', 'description' => 'レコーディングダイエットのきっかけを作る', 'done_report_type' => 2],
          [ 'title' => '毎朝ウォーキング', 'description' => '早起きして、朝の時間を有効に使うきっかけを作る', 'done_report_type' => 1],
          [ 'title' => '仕事終わりにランニング', 'description' => '仕事後の時間を有効に使うきっかけを作る', 'done_report_type' => 1],
          [ 'title' => '仕事終わりにトレーニング', 'description' => '仕事後の時間を有効に使うきっかけを作る', 'done_report_type' => 1],
          [ 'title' => '腹筋●●回', 'description' => '体の気になる部分を鍛える', 'done_report_type' => 1],
          [ 'title' => '腕立て●●回', 'description' => '体の気になる部分を鍛える', 'done_report_type' => 1],
          [ 'title' => '毎日寝る前にストレッチ', 'description' => '就寝前の時間を有効に使うきっかけを作る', 'done_report_type' => 1],
          [ 'title' => '週2回ジムへ行く！', 'description' => 'ジムでのトレーニングに行くきっかけを作る', 'done_report_type' => 1],
          [ 'title' => '週2回B-Monsterに行く！', 'description' => '特定プログラムを実施するジムに行くきっかけを作る', 'done_report_type' => 1],
          [ 'title' => '週末は自転車で遠出', 'description' => '週末に運動するきっかけを作る', 'done_report_type' => 1],
          [ 'title' => '隣駅まで歩いて通勤', 'description' => '通勤時間の中にトレーニングを入れることで、運動するきっかけを作る', 'done_report_type' => 1],
          [ 'title' => '糖質制限（朝だけ、昼食だけ、夜だけetc）', 'description' => '糖質を制限する食生活にすることで痩せさせる', 'done_report_type' => 1],
          [ 'title' => 'カロリー制限', 'description' => 'カロリー制限をする食生活にすることで痩せさせる', 'done_report_type' => 1],
          [ 'title' => 'お菓子厳禁！', 'description' => 'お菓子を我慢したことを共有し合うことで、我慢する気持ちを維持する', 'done_report_type' => 1],
          [ 'title' => '間食厳禁！', 'description' => '間食を我慢したことを共有し合うことで、我慢する気持ちを維持する', 'done_report_type' => 1],
          [ 'title' => '50回噛んで食べる', 'description' => '食事の際に良く噛むきっかけを作ることで食べ過ぎを抑制する', 'done_report_type' => 1],
          [ 'title' => 'お腹いっぱいまで食べない', 'description' => '食事の際に食べ過ぎないように意識させる', 'done_report_type' => 1],
          [ 'title' => '飲み会は週1回まで！', 'description' => '飲み会が多い、続いている人に', 'done_report_type' => 1],
          [ 'title' => '夕食はご飯の代わりにキャベツの千切り', 'description' => '糖質を制限する食生活にすることで痩せさせる', 'done_report_type' => 1],
          [ 'title' => '毎食食事記録', 'description' => 'レコーディングダイエットのきっかけを作る', 'done_report_type' => 1],
          [ 'title' => '週1回断食しよう', 'description' => '週1回体のリセットを行う', 'done_report_type' => 1],
          [ 'title' => '食べる時は最初はサラダ', 'description' => '', 'done_report_type' => 1],
          [ 'title' => 'RIZAPに行こう', 'description' => 'RIZAPに入会して2ヶ月間は週2回継続する', 'done_report_type' => 1],
          [ 'title' => '深夜ラーメン絶対食べない', 'description' => '深夜のラーメンを我慢することで', 'done_report_type' => 1],
          [ 'title' => '水を1日2リットル飲む', 'description' => '水を飲むことで代謝をあげる', 'done_report_type' => 1],
          [ 'title' => '朝起きてお白湯を飲む', 'description' => 'お白湯に飲むことで代謝をあげる', 'done_report_type' => 1],
          [ 'title' => '1日3食バランスよく食べる', 'description' => '不規則な生活の見直し', 'done_report_type' => 1],
          [ 'title' => 'まごはやさしい、の徹底', 'description' => '和食、健康的な食事を意識させる', 'done_report_type' => 1],
          [ 'title' => '夕食は◯時まで', 'description' => '夜に食べることで太るのを防ぐ', 'done_report_type' => 1],
          [ 'title' => '朝食置き換え', 'description' => '', 'done_report_type' => 1],
          [ 'title' => '昼食置き換え', 'description' => '', 'done_report_type' => 1],
          [ 'title' => '夕食置き換え', 'description' => '', 'done_report_type' => 1],
          [ 'title' => 'ご飯・パン・麺類抜き', 'description' => '様々な食材の糖質量を考えるのが大変、まずはご飯・パン・麺の3種類から制限', 'done_report_type' => 1],
          [ 'title' => 'ファスティング3日間', 'description' => '', 'done_report_type' => 1],
          [ 'title' => '休肝日をつくろう', 'description' => 'お酒を良くの人飲み過ぎを防ぐために', 'done_report_type' => 1],
          [ 'title' => 'アルコールの糖質制限', 'description' => '大量にお酒を飲む人へ、せめて糖質だけでも控えるために', 'done_report_type' => 1],
          [ 'title' => 'カフェインを摂らない', 'description' => '酵素の消費量を抑え代謝をあげるため', 'done_report_type' => 1],
          [ 'title' => '夜勤終わりに食べないで寝る', 'description' => '夜勤明けに食べてしまう方へ', 'done_report_type' => 1],
          [ 'title' => '腹8分を意識しよう', 'description' => '食べすぎを予防するために', 'done_report_type' => 1],
          [ 'title' => '箸置きダイエット', 'description' => '食べる速度が遅くなるので、お腹が一杯になりやすい', 'done_report_type' => 1],
          [ 'title' => '週に◯回以上半身浴', 'description' => '代謝をあげる', 'done_report_type' => 1],
          [ 'title' => '毎日姿勢を意識する', 'description' => '毎日姿勢を意識することで健康のためにも良いため', 'done_report_type' => 1],
          [ 'title' => '必ず階段を使う', 'description' => '毎日楽しく習慣にすることが出来る', 'done_report_type' => 1],
          [ 'title' => '1日お腹をひっこめる', 'description' => 'お腹太りが気になる人へ', 'done_report_type' => 1],
          [ 'title' => '就寝前脚マッサージ', 'description' => '脚やせをしたい人へ', 'done_report_type' => 1],
          [ 'title' => '就寝前ゴキブリ体操', 'description' => '脚やせをしたい人へ', 'done_report_type' => 1],
          [ 'title' => '壁腕立て伏せ1日50回以上', 'description' => '二の腕のシェイプアップをしたい人へ', 'done_report_type' => 1],
          [ 'title' => 'ウエストまわし運動1日5分以上', 'description' => 'ウエストを細くしたい人へ', 'done_report_type' => 1],
          [ 'title' => '毎日腸もみ', 'description' => 'お通じが気になる人へ', 'done_report_type' => 1],
          [ 'title' => '食後30分以上早歩き', 'description' => '血糖値の急な上昇を防ぐのでダイエットに効果的なため', 'done_report_type' => 1],
          [ 'title' => '腹筋30日間チャレンジ', 'description' => 'お腹痩せるを意識している人に', 'done_report_type' => 1],
          [ 'title' => 'プランク30日間チャレンジ', 'description' => '体幹を鍛えるために', 'done_report_type' => 1],
          [ 'title' => '寝る前10分ストレッチ', 'description' => 'どんなストレッチでも毎日の習慣にしてもらう', 'done_report_type' => 1],
          [ 'title' => '座る時は脚を閉じて座る', 'description' => '内股を細くした人へ', 'done_report_type' => 1],
          [ 'title' => '子供と夜ヨガ', 'description' => '夜寝る前にお子さんと一緒にヨガをする', 'done_report_type' => 1],
          [ 'title' => '子供と夜ダンス', 'description' => '夜寝る前にお子さんと一緒にダンスをする', 'done_report_type' => 1],
          [ 'title' => 'おかわり禁止', 'description' => 'おかわりが習慣になっている人（特にお米）', 'done_report_type' => 1],
          [ 'title' => '子供の残り物は食べない', 'description' => '子供の残り物を食べていて痩せない、という方が多いので…', 'done_report_type' => 1],
          [ 'title' => '締めのラーメンはなし', 'description' => '飲み会の多い人へ', 'done_report_type' => 1],
      ];

      for($i=0; $i<count($tbl); $i++) {
          App\Models\Program::create([
              'title' => $tbl[$i]['title'],
              'description' => $tbl[$i]['description'],
              'done_report_type' => $tbl[$i]['done_report_type'],
              'capacity' => 50,
              'program_days' => rand(1, 30),
              'program_level' => rand(1,5),
              'image_url' => '/images/icon.jpg',
              'supporter_id' => rand(1,2)
          ]);
      }

    }
}
