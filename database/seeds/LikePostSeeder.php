<?php

use Illuminate\Database\Seeder;

class LikePostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<20; $i++) {
            DB::table('like_posts')->insert([
                'post_id' => 1,
                'user_id' => $i+1 
            ]);
        }
    }
}
