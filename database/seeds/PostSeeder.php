<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    DB::statement('SET FOREIGN_KEY_CHECKS=0');
    DB::table('posts')->truncate();
    DB::table('comments')->truncate();
    DB::table('post_images')->truncate();

		for($j=0; $j<10; $j++) {
			$p = new App\Models\Post;
			$p->club_id = 1;
			$p->user_id = rand() % 3 + 1;
			$p->text = $j . ": This is test post. This is test post. This is test post. This is test post. This is test post. This is test post. This is test post. This is test post. This is test post. This is test post. ";
			$p->save();

      for($k=0; $k<rand() % 3; $k++) {
        $img = new App\Models\PostImage;
        $img->post_id = $p->id;
        $img->image_url = "images/nowprinting.jpg";
        $img->save();
      }

			for($i=0; $i<rand() % 20; $i++) {
				$c = new App\Models\Comment;
				$c->user_id = rand() % 3 + 1;
				$c->text = "this is test comment: " .$i;
				$p->comments()->save($c);
			}
		}
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}
