<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('users')->insert([
        'name' => 'admin',
        'email' => 'admin@example.com',
        'password' => bcrypt('secret'),
        'agreement_flag' => 1
    ]);
    DB::table('users')->insert([
        'name' => 'mika',
        'email' => 'mika@example.com',
        'password' => bcrypt('secret'),
        'agreement_flag' => 1
    ]);
    DB::table('users')->insert([
        'name' => 'taro',
        'email' => 'taro@example.com',
        'password' => bcrypt('secret'),
        'agreement_flag' => 1
    ]);
    DB::table('users')->insert([
        'name' => 'jiro',
        'email' => 'jiro@example.com',
        'password' => bcrypt('secret'),
    ]);
    DB::table('users')->insert([
        'name' => 'hikaru',
        'email' => 'hikaru@example.com',
        'password' => bcrypt('secret'),
    ]);

    DB::table('supporters')->insert([ 'user_id' => 2 ]);
    DB::table('supporters')->insert([ 'user_id' => 5 ]);

    $p = new App\Models\Profile;
    $p->user_id = 1;
    $p->nickname = 'admin';
    $p->birthday = date('1985/01/01');
    $p->weight = 60;
    $p->height = 170;
    $p->save();

    $p = new App\Models\Profile;
    $p->user_id = 2;
    $p->nickname = 'mika';
    $p->birthday = date('1995/12/31');
    $p->photo_image_url = '/images/woman.png';
    $p->weight = 40;
    $p->height = 155;
    $p->save();

    $p = new App\Models\Profile;
    $p->user_id = 3;
    $p->nickname = 'taro';
    $p->birthday = date('1991/05/15');
    $p->weight = 65;
    $p->height = 165;
    $p->save();

    $p = new App\Models\Profile;
    $p->user_id = 5;
    $p->nickname = 'hikaru';
    $p->birthday = date('1995/12/31');
    $p->photo_image_url = '/images/woman.png';
    $p->weight = 40;
    $p->height = 155;
    $p->save();


    $faker = Faker\Factory::create('ja_JP');
    for($i=0; $i<50; $i++) {
      DB::table('users')->insert([
          'name' => '',
          'email' => $i . "_" . $faker->email,
          'password' => bcrypt('111')
      ]);
      DB::table('profiles')->insert([
          'user_id' => $i+6,
          'nickname' => $faker->name,
          'birthday' => date('1995/1/1'),
          'weight' => rand(40, 80),
          'height' => rand(140, 190)
      ]);
    }
  }
}
