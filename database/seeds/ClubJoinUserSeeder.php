<?php

use Illuminate\Database\Seeder;

class ClubJoinUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('club_join_users')->insert([
            0 =>[
                    'club_id' => 1,
                    'user_id' => 1,
                    'start_date' => '2017-11-13',
                    'end_date' => '2017-11-17'
                ],
            1 =>[
                    'club_id' => 1,
                    'user_id' => 2,
                    'start_date' => '2017-04-13',
                    'end_date' => '2017-05-13'
                ],
            2=> [
                    'club_id' => 1,
                    'user_id' => 3,
                    'start_date' => '2017-06-13',
                    'end_date' => '2017-06-20'
                ],
            3 =>[
                    'club_id' => 1,
                    'user_id' => 4,
                    'start_date' => '2017-6-13',
                    'end_date' => '2017-07-31'
                ]
        ]);
    }
}
