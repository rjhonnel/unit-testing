<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Supporter;

class SupporterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'supporter',
            'email' => 'supporter@example.com',
            'password' => bcrypt('secret'),
            'agreement_flag' => 1
        ]);
        Supporter::create([
            'user_id' => $user->id
        ]);
    }
}
