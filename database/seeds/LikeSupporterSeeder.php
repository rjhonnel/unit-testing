<?php

use Illuminate\Database\Seeder;
use App\Models\LikeSupporter;

class LikeSupporterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LikeSupporter::create([
            'supporter_id' => 1,
            'user_id' => 1
        ]);
    }
}
