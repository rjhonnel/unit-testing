<?php

use Illuminate\Database\Seeder;

class LikeCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('like_comments')->insert([
            'comment_id' => 1,
            'user_id' => 2
        ]);
    }
}
