<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueLikePosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('like_posts', function (Blueprint $table) {
            $table->unique(['post_id', 'user_id'], 'post_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('like_posts', function (Blueprint $table) {
            $table->dropForeign('like_posts_post_id_foreign');
            $table->dropForeign('like_posts_user_id_foreign');
            $table->dropUnique('post_user');
        });
    }
}
