<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeDataWeight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE profiles MODIFY weight DECIMAL(5,1) NOT NULL;');
        DB::statement('ALTER TABLE goals MODIFY weight DECIMAL(5,1) NOT NULL;');
        DB::statement('ALTER TABLE user_weights MODIFY weight DECIMAL(5,1) NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE profiles MODIFY weight INTEGER NOT NULL;');
        DB::statement('ALTER TABLE goals MODIFY weight INTEGER NOT NULL;');
        DB::statement('ALTER TABLE user_weights MODIFY weight INTEGER NOT NULL;');
    }
}
