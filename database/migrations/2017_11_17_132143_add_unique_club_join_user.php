<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueClubJoinUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('club_join_users', function (Blueprint $table) {
            $table->unique(['club_id', 'user_id'], 'club_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('club_join_users', function (Blueprint $table) {
            $table->dropForeign('club_join_users_club_id_foreign');
            $table->dropForeign('club_join_users_user_id_foreign');
            $table->dropUnique('club_user');
        });
    }
}
