<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->unique();
            $table->string('nickname', 128);
            $table->tinyInteger('gender')->comment('1:male, 2:female')->nullable();
            $table->date('birthday');
            $table->integer('weight');
            $table->integer('height');
            $table->string('photo_image_url', 256)->nullable();
            $table->string('cover_image_url', 256)->nullable();
            $table->string('self_introduction', 256)->nullable();
            $table->tinyInteger('plan')->comment('1:lose to weight, 2:keep weight, 3:musle up')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
