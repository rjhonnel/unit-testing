<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueLikeComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('like_comments', function (Blueprint $table) {
            $table->unique(['comment_id', 'user_id'], 'comment_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('like_comments', function (Blueprint $table) {
            $table->dropForeign('like_comments_comment_id_foreign');
            $table->dropForeign('like_comments_user_id_foreign');
            $table->dropUnique('comment_user');
        });
    }
}
