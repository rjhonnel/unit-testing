<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supporter_id')->unsigned();
            $table->string('title', 256);
            $table->string('description', 256);
            $table->tinyInteger('done_report_type')->comment('1:just done, 2:input weight, 3:upload photo');
            $table->integer('capacity')->comment('max number of club join users');
            $table->integer('program_days')->nullable();
            $table->timestamps();

            $table->foreign('supporter_id')->references('id')->on('supporters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
