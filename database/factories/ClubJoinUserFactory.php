<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\ClubJoinUser::class, function (Faker $faker) {
    return [
        'club_id' => 1,
        'user_id' => 1,
        'start_date' => Carbon::today(),
        'end_date' => Carbon::tomorrow(),
        'program_end_checked_at' => null,
    ];
});
