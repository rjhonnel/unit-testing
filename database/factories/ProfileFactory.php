<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Profile::class, function (Faker $faker) {

    return [
        'nickname' => $faker->name,
        'birthday' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'self_introduction' => $faker->text(),
        'gender' => $faker->numberBetween($min=1, $max=2),
        'weight' => $faker->numberBetween($min=40, $max=80),
        'height' => $faker->numberBetween($min=145, $max=185)
    ];
});
