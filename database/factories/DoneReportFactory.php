<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\DoneReport::class, function (Faker $faker) {
    return [
        'post_id' => 1,
        'date' => \Carbon\Carbon::now()
    ];
});
