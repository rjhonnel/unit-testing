<?php

use Faker\Generator as Faker;

$factory->define(App\Models\UserWeight::class, function (Faker $faker) {

    return [
        'date' => date('Y-m-d'),
        'weight' => $faker->numberBetween($min=40, $max=80),
    ];
});
