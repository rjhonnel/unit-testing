<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\Program::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->text(200),
        'supporter_id' => 1,
        'done_report_type' => 1,
        'capacity' => 50,
        'program_days' => 7,
        'program_level' => rand(1,5),
        'image_url' => 'images/someimage.jpg',
    ];
});
