<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\DoneReportUser::class, function (Faker $faker) {
    return [
        'done_report_id' => 1,
        'user_id' => 1
    ];
});
