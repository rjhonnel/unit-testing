<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\Goal::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'weight' => $faker->numberBetween($min=40, $max=80),
        'deadline' => \Carbon\Carbon::tomorrow()
    ];
});
