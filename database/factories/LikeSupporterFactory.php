<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\LikeSupporter::class, function (Faker $faker) {
    return [
        'supporter_id' => 1,
        'user_id' => 1
    ];
});
