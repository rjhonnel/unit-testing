<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    return [
        'post_id' => 1,
        'user_id' => 1,
        'text' => $faker->text(200)
    ];
});
