<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Notification::class, function (Faker $faker) {

    return [
        'router_id' => $faker->numberBetween($min=1, $max=100),
        'notification_type' => $faker->numberBetween($min=1, $max=3)
    ];
});
