<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\RecommendProgram::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'program_id' => 1
    ];
});
