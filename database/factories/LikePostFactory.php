<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\LikePost::class, function (Faker $faker) {
    return [
        'post_id' => 1,
        'user_id' => 1
    ];
});
