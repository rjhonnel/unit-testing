<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\Club::class, function (Faker $faker) {
    return [
        'program_id' => 1
    ];
});
