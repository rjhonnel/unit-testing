<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\LikeComment::class, function (Faker $faker) {
    return [
        'comment_id' => 1,
        'user_id' => 1
    ];
});
