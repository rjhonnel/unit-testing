<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {

    return [
        'club_id' => 1,
        'user_id' => 1,
        'text' => $faker->text(200)
    ];
});
