@extends('layouts.app', ['back_url' => '/profile/edit/nickname', 'no_link' => 'yes'] )

@section('style')
  <style>
    .custom-content {border: 2px solid #ccc; min-width:320px; max-width: 500px;}
    .mr-t-10 {margin-top: 20px;margin-bottom: 20px;}
    .no-border {border:none;border-radius:0;}
  </style>
@endsection

@section('content')
  <div class="container custom-content">
    <div class="col-xs-12 mr-t-10">
      <legend class="text-center no-border">Edit nickname</legend>
      <form action="{{ route('profile.update.nickname') }}" method="POST" role="form">
        {{ csrf_field() }}
        <div class="form-group">
          <input type="text" class="form-control" name="nickname" value="{{ $nickname or old('nickname') }}">
        </div>
        @if ($errors->has('nickname'))
          <p class="text-danger">{{ $errors->first('nickname') }}</p>
        @endif
        <div class="col-xs-6 col-xs-offset-3">
          <button type="submit" class="btn btn-primary center-block btn-block no-border">Save</button>
        </div>
      </form>
    </div>
  </div>
@endsection