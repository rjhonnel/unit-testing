<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
#Admin login
Route::get('admin', 'Auth\LoginController@redirectAdminLogin');

Route::get('/admin/login', 'Auth\LoginController@showLoginAdminForm')->name('admin.login');
Route::post('/admin/login', 'Auth\LoginController@loginAdmin')->name('post.admin.login');
Route::post('/admin/logout', 'Auth\LoginController@logoutAdmin')->name('admin.logout');
Route::group(['prefix' => 'admin', 'middleware' => 'authAdmin'], function () {
  #Dashboard
  Route::get('/dashboard', 'Auth\LoginController@dashboard')->name('admin.dashboard');

  #Program
  Route::get('/programs', 'Admin\ProgramController@index')->name('admin.program.index');
  Route::get('/datatable/get-program', 'Admin\ProgramController@getPrograms')->name('datatable.programs');
  Route::get('/programs/{id}/show', 'Admin\ProgramController@show')->name('admin.program.show');
  Route::get('/programs/new', 'Admin\ProgramController@create')->name('admin.program.create');
  Route::post('/programs', 'Admin\ProgramController@store')->name('admin.programs.store');
  Route::get('/programs/{id}/edit', 'Admin\ProgramController@edit')->name('admin.programs.edit');
  Route::put('/programs/{id}', 'Admin\ProgramController@update')->name('admin.programs.update');

  #User
  Route::group(['prefix' => 'users', 'namespace' => 'Admin'], function() {
    Route::get('/', 'UserController@index')->name('admin.user.list');
    Route::get('new', 'UserController@create')->name('admin.user.create');
    Route::post('/', 'UserController@store')->name('admin.user.store');
    Route::get('{id}', 'UserController@show')->name('admin.user.show');
  });

  # Supporter
  Route::group(['prefix' => 'supporters', 'namespace' => 'Admin'], function() {
    Route::get('/', 'SupporterController@index')->name('admin.supporter.list');
    Route::get('new', 'SupporterController@create')->name('admin.supporter.create');
    Route::post('/', 'SupporterController@store')->name('admin.supporter.store');
    Route::get('{id}', 'SupporterController@show')->name('admin.supporter.show');
    Route::get('{id}/edit', 'SupporterController@edit')->name('admin.supporter.edit');
    Route::put('{id}', 'SupporterController@update')->name('admin.supporter.update');
  });

  #Club
  Route::get('/clubs', 'Admin\ClubController@show')->name('admin.club.show');
  Route::get('/clubs/{id}', 'Admin\ClubController@detail')->name('admin.club.detail');

  # Club join user
  Route::get('club_join_users', 'Admin\ClubJoinUserController@index')->name('admin.club_join_user.list');

});

#Authenticate
Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/login_from_email', 'Auth\LoginController@loginFromEmail');

#Publish post
Route::get('posts/{id}', 'PostController@show')->name('posts.show');

Route::group(['middleware' => 'auth'], function () {
  # Recommend Programs
  Route::group(['middleware' => 'userable'], function() {
      Route::get('recommend_programs', 'ProgramController@recommend')->name('recommend');
      Route::post('recommend_programs/join', 'ProgramController@join')->name('recommend_programs.join');
  });

  Route::group(['middleware' => 'checkAgreementFlag', 'prefix' => 'tutorial'], function() {
    Route::get('/', 'TutorialController@nickname')->name('tutorial');
    Route::post('nickname', 'TutorialController@postNickname')->name('tutorial.nickname.post');
    Route::get('height_weight', 'TutorialController@heightWeight')->name('tutorial.height_weight');
    Route::post('height_weight', 'TutorialController@postHeightWeight')->name('tutorial.height_weight.post');
    Route::get('gender', 'TutorialController@gender')->name('tutorial.gender');;
    Route::post('gender', 'TutorialController@postGender')->name('tutorial.gender.post');
    Route::get('birthday', 'TutorialController@birthday')->name('tutorial.birthday');
    Route::post('birthday', 'TutorialController@postBirthday')->name('tutorial.birthday.post');
    Route::get('plan', 'TutorialController@plan')->name('tutorial.plan');
    Route::post('plan', 'TutorialController@postPlan')->name('tutorial.plan.post');
    Route::get('goal', 'TutorialController@goal')->name('tutorial.goal');
    Route::post('goal', 'TutorialController@postGoal')->name('tutorial.goal.post');
    Route::get('confirm', 'TutorialController@confirm')->name('tutorial.confirm');
    Route::post('confirm', 'TutorialController@postData')->name('tutorial.confirm.post');
  });

  Route::post('user/profile', 'UserController@createProfile')->name('profile.create');
  Route::get('/profile', 'UserController@editProfile')->name('profile.show');
  Route::get('/profile/edit', 'UserController@editField')->name('profile.edit');
  Route::put('/profile', 'UserController@updateField')->name('profile.update');
  Route::put('/avatar', 'UserController@updateAvatar')->name('avatar.update');

  #Top
  Route::get('/', 'TopPageController@index')->name('toppage');
  Route::post('/ajax/agree_term', 'TopPageController@ajaxTopPage')->name('ajax.agree_term');

  #Terms of service
  Route::get('/terms_of_service', function () {
    return view('terms_of_service.show');
  });

  #My page
  Route::group(['middleware' => ['checkAgreementFlag', 'checkProfile']], function () {
    Route::get('/mypage', 'MyPageController@index')->name('mypage.index');
    Route::post('/ajax/daily_report', 'MyPageController@ajaxDailyReport')->name('ajax.daily_report');
    Route::post('/mypage/dialog/{id}/close', 'MyPageController@close')->name('mypage.close');
  });

  #Programs
  Route::get('/programs', 'ProgramController@list')->name('program.list');
  Route::get('/programs/{programId}{r?}', 'ProgramController@detail')->name('program.detail');
  Route::post('/ajax/update_user_join_club', 'ProgramController@updateUserJoinClub')->name('ajax.update_user_join_club');

  #Supporters
  Route::get('supporters', 'SupporterController@index')->name('supporters.list');
  Route::get('supporters/{id}', 'SupporterController@detail')->name('supporters.detail');
  Route::post('supporters/{id}/like', 'SupporterController@like')->name('supporters.like');
  Route::delete('supporters/{id}/like', 'SupporterController@unlike')->name('supporters.unlike');

  #Posts
  Route::post('posts/report/{id}', 'PostController@report');
  Route::delete('posts/report/{id}', 'PostController@deleteReport');
  Route::delete('posts/delete/{postId}', 'PostController@delete');
  Route::get('posts', 'PostController@create')->name('posts.create');
  Route::post('posts', 'PostController@store')->name('posts.store');
  Route::get('posts/edit/{id}', 'PostController@edit')->name('posts.edit');
  Route::put('posts/edit/{id}', 'PostController@update')->name('posts.update');

  #Comments
  Route::get('comments/{postId}', 'CommentController@show');
  Route::post('comments/create/{postId}', 'CommentController@create');
  Route::delete('comments/delete/{commentId}', 'CommentController@delete');
  Route::put('comments/edit/{commentId}', 'CommentController@edit');

  #Post Like
  Route::get('/post-likes/{postId}', 'LikeController@showUsersLikedPost');
  Route::post('like/posts/{postId}', 'LikeController@likePost');
  Route::delete('like/posts/{postId}', 'LikeController@deleteLikePost');

  #Comment Like
  Route::get('/comment-likes/{commentId}', 'LikeController@showUsersLikedConmment');
  Route::post('like/comments/{commentId}', 'LikeController@likeComment');
  Route::delete('like/comments/{commentId}', 'LikeController@deleteLikeComment');

  #Supporter Like
  Route::get('/supporter-likes/{supporterId}', 'LikeController@showUsersLikedSupporter');

  #User weigth
  Route::get('report-weight/{doneReportId}', 'WeightReportController@index');
  Route::post('report-weight/{doneReportId}', 'WeightReportController@create');
  Route::get('weight', 'UserController@weightGraph')->name('weight.index');
  Route::post('weight/update', 'WeightReportController@createWeightWithoutDoneReportUser');

  #Notifications
  Route::get('/notifications', 'NotificationController@index');
  Route::post('/notifications/read/{id}', 'NotificationController@updateReadAt');

  #Communications
  Route::get('communications/{clubId}', 'CommunicationController@show')->name('communications.list');
});
